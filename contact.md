---
title: Contact
---

I'm located in Zagreb, Croatia.

Email (PGP Key ID [1503F0AA][mit]):

* [marko@dimjasevic.net](mailto:marko@dimjasevic.net)

Online social network ([ActivityPub](https://en.wikipedia.org/wiki/ActivityPub)-based):

* Mastodon: [mdimjasevic@mamot.fr](https://mamot.fr/@mdimjasevic)

Real-time communication (the [Matrix protocol](https://en.wikipedia.org/wiki/Matrix_(protocol))):

* [**matrix**]: [\@mdimjasevic:matrix.org][matrixurl]

[mit]: https://pgp.mit.edu/pks/lookup?op=vindex&search=0x565EE9641503F0AA
[matrixurl]: https://matrix.to/#/@mdimjasevic:matrix.org
