# Building

To install the dependencies and build the project, run:

	cabal new-build

To compile the website from scratch, run:

	cabal new-run site rebuild

The compiled website will be located in the `_site` sub-directory.

To view it at `localhost:8080` in a web browser, run:

	cabal new-run site watch
