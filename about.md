---
title: About
---

I am a computer scientist. My research interests are in theorem
proving, software correctness, functional programming and type
theory. I am also a free software enthusiast and I have worked on and
participated in several projects and communities.

> [Resume](/resume.pdf) | [Contact](/contact/) | [Publications](/publications/) | [Service](#service) | [Github][gh] | [GitLab][gl]

## Projects and communities

* [Cardano][cardano-sl] --- [blockchain specification][cardano-specs]
* [Debian](https://www.debian.org/) --- [I had a goal][debm] of becoming a Debian developer:
    * [STP][stp] ([ITP #789055][stpitp]) --- Simple Theorem Prover
	* [KLEE][klee] ([ITP #576142][kleeitp]) --- LLVM-based symbolic virtual machine
    * [Java Pathfinder][jpf] ([ITP #805525][jpfitp]) --- Virtual machine for Java bytecode
* [Clover][clover] --- symbolic execution analysis of Debian packages with KLEE
* [maline][maline] --- Android malware detection
* [JDoop](https://github.com/psycopaths/jdoop) --- Automatic testing for Java
* [JDart](https://github.com/psycopaths/jdart) --- Dynamic symbolic execution for Java
* NASA --- [Runtime verification](http://dx.doi.org/10.1145/2771783.2771804) of [AutoResolver](http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20110008155.pdf)
* Google Summer of Code 2012 --- [Model Checking Android Services](https://gitlab.com/mdimjasevic/jpf-android-services)
* Google Summer of Code 2013 --- [Combining JDart and Randoop](https://github.com/psycopaths/jdoop)
* Google Summer of Code 2016 --- [Support for KLEE in Debile][dwiki]
* Artifact evaluation: [posts](/tag/artifact-evaluation/) and a [repository](https://gitlab.com/mdimjasevic/artifact-eval)
* [OpenStreetMap](https://www.openstreetmap.org/user/dim)


## <a name="service"></a>Research community service

* [International Workshop on Malware Analysis 2016][wma] Program Committee member
* [Programming Language Design and Implementation 2016][pldi] Artifact Evaluation Committee member
* [Computer Aided Verification 2015][cav] Artifact Evaluation Committee member

## Research done at

* [University of Utah](http://www.utah.edu/)
* [NASA Ames Research Center](https://www.nasa.gov/centers/ames/home/index.html)

[gh]: https://github.com/mdimjasevic
[gl]: https://gitlab.com/u/mdimjasevic
[debm]: https://mentors.debian.net/packages/uploader/marko%40dimjasevic.net
[stp]: http://mentors.debian.net/package/stp
[stpitp]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=789055
[klee]: https://mentors.debian.net/package/klee
[kleeitp]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=576142
[jpf]: https://gitlab.com/mdimjasevic/jpf-packaging
[jpfitp]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=805525
[clover]: https://github.com/soarlab/clover
[maline]: https://github.com/soarlab/maline
[cardano-sl]: https://github.com/input-output-hk/cardano-sl
[cardano-specs]: https://github.com/input-output-hk/cardano-ledger-specs
[dwiki]: https://wiki.debian.org/SummerOfCode2016/StudentApplications/MarkoDimjasevic
[wma]: https://web.archive.org/web/20160314192305/http://www.ares-conference.eu/conference/workshopsares2016/wma-2016/
[pldi]: https://web.archive.org/web/20160127042608/http://conf.researchr.org/committee/pldi-2016/pldi-2016-artifacts-evaluation-committee
[cav]: https://web.archive.org/web/20150814111727/http://i-cav.org/2015/evaluation/
