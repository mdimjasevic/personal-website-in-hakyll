---
author: Marko Dimjašević
comments: false
date: 2011-06-16 17:35:55+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/06/16/osnovana-udruga-slobodan-softver-hrvatska/
slug: osnovana-udruga-slobodan-softver-hrvatska
title: "Osnovana udruga Slobodan softver Hrvatska!"
wordpress_id: 357
---

U petak se na Matematičkom odsjeku Prirodoslovno-matematičkog
fakulteta u Zagrebu održala osnivačka skupština udruge Slobodan
softver Hrvatska. Ideja za takvom udrugom postoji već neko vrijeme i
sada je konačno realizirana.

Kako Lawrence Lessig kaže u uvodu zbirke eseja [Free Software, Free
Society](http://www.gnu.org/philosophy/fsfs/rms-essays.pdf),
programski kôd je tehnologija koja pokreće računala. Bez obzira je li
zapisan u softveru ili hardveru, to je niz naredbi koji definira
funkcionalnost digitalnih uređaja. Ti uređaji --- posebice osobna
računala --- sve više određuju i upravljaju našim životima pa u skladu
s time možemo reći da smo digitalno društvo. Udruga je nastala s
ciljem osvještavanja društva na svim razinama o odnosima moći i
kontroli kada se radi o tom nizu naredbi, tj. o softveru. Tko
kontrolira tu tehnologiju, a time i brojne aspekte života u digitalnom
društvu je pitanje oko kojeg kroz Udrugu želimo razviti
diskusiju. Lessig postavlja pitanja poput: "Kakvu kontrolu bismo
trebali imati nad tim kôdom? Kakvo znanje? Kakva bi sloboda trebala
postojati tako da odgovara kontroli koju omogućuje?" Sloboda kôda je
transparentni uvid u kontrolu koju programeri ugrađuju kao i pravo
svakoga da preuzme tu kontrolu i izmijeni ju prema svojim željama. To
je slobodan softver. Slobodan softver je kontrola koja je
transparentna i otvorena za promjene.

Želio bih da u Slobodnom softveru Hrvatska polazimo od pitanja u
kakvom društvu želimo živjeti. Smatram vrijednim i želim živjeti u
društvu čije norme promiču tehnološke arhitekture koje podupiru
vrijednosti jednakosti, učenja, dijeljenja, pomaganja, demokracije i
privatnosti.

Privremena
[web-stranica](http://wiki.fsfhr.mnalis.com/index.php/Main_Page)
udruge sadrži wiki na kojem se nalaze osnovne informacije o
udruzi. Ako na bilo koji način želite doprinijeti udruzi ---
volontiranjem, donacijom novca ili poslužitelja koji nam je potreban,
pružanjem prostora za rad, diskusijom ili na koji drugi način ---
javite nam se na [mailing
listu](http://lists.iugrina.com/cgi-bin/mailman/listinfo/fsf-hr).
