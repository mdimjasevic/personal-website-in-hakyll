---
author: Marko Dimjašević
comments: false
date: 2011-07-27 19:56:19+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/07/27/potpuno-slobodno-prijenosno-racunalo/
published: false
slug: potpuno-slobodno-prijenosno-racunalo
title: "Potpuno slobodno prijenosno računalo"
wordpress_id: 362
tags:
- free software
- fully free distro
- GNU/Linux
- laptop
- Shuttleworth
- slobodan softver
- Ubuntu
---

Nije tajna da mnoga današnja prijenosna računala neće u potpunosti
funkcionirati s isključivo slobodnim softverom. Razlog tome je što su
javno dostupne specifikacije hardvera prava rijetkost pa nije moguće
napisati slobodan softver koji bi njime upravljao. Većina današnjih
distribucija GNU/Linuxa nije u potpunosti slobodna pa sadrže nešto
vlasničkog softvera kako bi hardver za koji nije javno dostupna
specifikacija ipak funkcionirao. S druge strane postoje prijenosna
računala koja funkcioniraju s isključivo slobodnim softverom, barem
gledano na razini operacijskog sustava. Ako tražite prijenosno
računalo koje će (u potpunosti) funkcionirati sa slobodnim softverom,
stranica [h-node](http://www.h-node.com/) vam može biti od
pomoći. Pukom srećom imam takav netbook, iako nisam bio svjestan
problematike u trenutku nabave računala.

Danas sam naletio na
[članak](http://www.markshuttleworth.com/archives/131) na blogu Marka
Shuttlewortha, osnivača tvrtke Canonical Ltd. koja stoji iza
distribucije GNU/Linuxa zvane Ubuntu. Ubuntu nije u potpunosti
slobodna distribucija i dojma sam da sve više i više promiče vlasnički
softver. Službena stranica Ubuntua niti ne spominje slobodan softver;
spominje samo besplatan softver... barem je tako bilo kad sam zadnji
puta posjetio web-stranicu Ubuntua. Zbog toga sam se ugodno iznenadio
kada sam naišao na spomenuti članak Marka Shuttlewortha u kojem je
objavio inicijativu za proizvodnju prijenosnog računala koje će u
potpunosti funkcionirati sa slobodnim softverom, barem od razine
BIOS-a pa na više, a ako je moguće i na razini firmwarea. Nadam se da
će to potaknuti nastajanje sve više i više prijenosnih računala koja u
potpunosti funkcioniraju sa slobodnim softverom.

Drago mi je da Shuttleworth koristi svoj utjecaj kako bi nastalo (još
jedno) prijenosno računalo koje će poštovati slobodu računalnih
korisnika. Ta je sloboda iznimno bitna u današnjem digitalnom društvu.
