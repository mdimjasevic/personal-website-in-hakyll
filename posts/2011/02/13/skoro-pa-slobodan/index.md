---
author: Marko Dimjašević
comments: false
date: 2011-02-13 11:25:50+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/02/13/skoro-pa-slobodan/
slug: skoro-pa-slobodan
title: "Skoro pa slobodan"
wordpress_id: 284
tags:
- bios
- coreboot
- gnu
- linuxlibre
- parabola
- slobodan softver
---

![GNU](gnu-white.png "GNU")

Zadnjih pola godine koristio sam distribuciju Chakra. Instalirao sam
ju dok je bila u beta inačici i tu i tamo trebalo je nešto ručno
pokrpati kako bi dalje radila. Bio mi je gušt koristiti tu
distribuciju jer je pružala ono što sam tražio --- da ima KDE, da ima
[kontinuirano izdanje](http://en.wikipedia.org/wiki/Rolling_release) i
da je vrlo ažurna što se tiče novih paketa. Međutim, od prije kojih 2
-- 3 mjeseca ta inačica Chakre koju sam koristio više nije bila
podržana i moj je operacijski sustav ubrzo postao nered. Od tada
odgađam neizbježno, a to je instalacija novog sustava.


Do sada sam se od drugih distribucija okušao u Kubuntuu, Ubuntuu,
Fedori, Debianu, openSUSE-u, Archu i možda još kojoj. Nedavno sam
saznao da jezgra Linux zapravo i nije u potpunosti slobodan softver,
iako je licencirana pod GNU GPLv2. Mnogi taj propust pripisuju Linusu
Torvaldsu, benevolentnom diktatoru u projektu razvoja jezgre Linux. U
jezgru su se uvukli brojni driveri i firmware bilo u obliku
[blob-ova](http://en.wikipedia.org/wiki/Binary_blob), bilo pod
vlasničkom licencom ili čak oboje. Uvukli su se jer ih je odobrio
Torvalds. On odlučuje što će biti dio nove inačice jezgre, a što
neće. Kao reakcija na takav propust nastao je projekt
[Linux-libre](http://www.fsfla.org/svnwiki/selibre/linux-libre/),
inačica Linuxa koja ne sadrži blob-ove niti drugi vlasnički
softver. Vođeni, među ostalim, tom idejom, nastali su projekti potpuno
slobodnih distribucija GNU/Linuxa. Postoji [popis takvih
distribucija](http://www.gnu.org/distros/free-distros.html) koji je
sastavio Free Software Foundation.

Jučer sam instalirao potpuno slobodnu distribuciju. Zove se
[Parabola](http://parabolagnulinux.org/). Kao i Chakra, nastala je iz
distribucije Arch. Stoga nisam napravio veliku promjenu što se tiče
funkcioniranja distribucije, no napravljen je velik korak što se tiče
slobode --- sada koristim potpuno slobodan softver na svom netbooku!

No na računalu još uvijek imam nešto vlasničko, a to je
[BIOS](http://en.wikipedia.org/wiki/BIOS). Danas kada kupite računalo,
ono dođe s predinstaliranim BIOS-om koji je, nažalost, vlasnički. Imam
namjeru ukloniti i taj propust jer postoji [slobodan
BIOS](http://www.fsf.org/campaigns/free-bios.html). Jedan primjer
takvog BIOS-a je [coreboot](http://www.coreboot.org/). Ako uspijem
instalirati i slobodan BIOS, bit će to po prvi puta da ću koristiti
računalo slobodno od ograničenja i štete koje nameće i uzrokuje
vlasnički softver.
