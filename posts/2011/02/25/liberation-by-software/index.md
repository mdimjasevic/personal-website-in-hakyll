---
author: Marko Dimjašević
comments: false
date: 2011-02-25 20:04:25+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/02/25/liberation-by-software/
slug: liberation-by-software
title: "Liberation by software"
wordpress_id: 296
---

Ako do sada, dragi čitatelju, nisi shvatio o čemu ja to ovdje laprdam
kad pišem o slobodnom softveru, možda pomogne odličan članak [Ebena
Moglena](http://emoglen.law.columbia.edu/) naslova [Liberation by
software](http://www.guardian.co.uk/commentisfree/cifamerica/2011/feb/24/internet-freedomofinformation)
na Guardianu.

Pred kraj članka spominje "FreedomBoxove". FreedomBox je uređaj za
slobodnu komunikaciju kojeg razvija zajednica slobodnog softvera i
više o njemu možete pročitati na [web-stranici
fondacije](http://freedomboxfoundation.org/).
