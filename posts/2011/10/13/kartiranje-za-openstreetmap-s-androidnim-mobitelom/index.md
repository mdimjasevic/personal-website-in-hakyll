---
author: Marko Dimjašević
comments: false
date: 2011-10-13 10:52:43+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/10/13/kartiranje-za-openstreetmap-s-androidnim-mobitelom/
slug: kartiranje-za-openstreetmap-s-androidnim-mobitelom
title: "Kartiranje za OpenStreetMap s Androidnim mobitelom"
wordpress_id: 384
categories:
- OpenStreetMap
- volontiranje
tags:
- Android
- CyanogenMod
- GPS
- OpenStreetMap
- OsmAnd
- OSMTracker for Android
---

Ako kojim slučajem kartirate za
[OpenStreetMap](http://openstreetmap.org) i to radite uz Androidni
mobilni telefon, moguće je da koristite aplikaciju [OSMTracker for
Android](https://code.google.com/p/osmtracker-android/). Ta je
aplikacija vrlo praktična jer omogućava --- osim samog snimanja GPS
pozicije --- snimanje georeferenciranih fotografija i zvučnih snimki
kao i pridavanje raznih atributa trenutnoj poziciji kao što su
primarna cesta, telefonska govornica, bankomat, parkiralište, muzej i
dr. Tako snimljenu trasu moguće je izvesti u datoteku tipa
[GPX](https://secure.wikimedia.org/wikipedia/en/wiki/Gpx), a takva
datoteka prebačena na računalo zajedno s pripadnim fotografijama i
zvučnim snimkama je pogodna za obradu u programu kao što je
[JOSM](http://josm.openstreetmap.de).

![](screenshot-1318517665343.png){ height=500px }

Problem koji može uzrokovati OSMTracker for Android je nepredviđeno
gašenje telefona. Tako puni želje da ucrtate još neoznačenu stazu
unutar OpenStreetMapa na Psunju nakon npr. sat vremena hodanja uočite
da vam je telefon zapravo ugašen i čeka na unos PIN-a. Drugim
riječima, upravo ste ostali bez sat vremena zapisa o svojem kretanju,
a jedino što vam je ostalo je propuštena prilika da ucrtate tu stazu u
OpenStreetMap.

Nisam do sada uspio razaznati je li problem u samoj aplikaciji
OSMTracker for Android bilo koje dostupne inačice ili je problem vezan
uz vrstu i inačicu Androida kojeg koristite. Problem gašenja telefona
mi se događao unutar CyanogenModa inačica 7.0.3 i
7.1. [CyanogenMod](http://www.cyanogenmod.com) je varijanta
operacijskog sustava Android koja uključuje samo slobodan softver na
razini operacijskog sustava, no ne garantira i slobodni firmware.

Kako god, tokom jučerašnjeg bicikliranja uočio sam kako privremeno
doskočiti tom problemu --- uz OSMTracker for Android pokrenete i
[OsmAnd+](https://code.google.com/p/osmand/). OsmAnd+ uvijek treba
biti aktivna aplikacija, tj. "na vrhu" svih aplikacija (uključujući
situaciju dok je ekran ugašen), a OSMTracker for Android stavite u
pozadinu. OsmAnd+ ovdje ne koristim za kartiranje, već samo kao
popratnu aplikaciju koja održava telefon upaljenim :) _It works for
me_.
