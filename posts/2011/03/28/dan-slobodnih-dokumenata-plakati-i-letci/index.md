---
author: Marko Dimjašević
comments: false
date: 2011-03-28 20:51:35+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/03/28/dan-slobodnih-dokumenata-plakati-i-letci/
slug: dan-slobodnih-dokumenata-plakati-i-letci
title: "Dan slobodnih dokumenata — plakati i letci"
wordpress_id: 300
---

![](banner-180x150-dfd2011-hr.png)

Ove srijede je svjetski [Dan slobodnih
dokumenata](http://documentfreedom.org). Uskoro više o tom danu ovdje
na blogu, a za sada samo prevedeni plakati i letci za one koje bi
htjeli koristiti [materijale](artwork-dfd20111.zip) prevedene na
hrvatski.
