---
author: Marko Dimjašević
comments: false
date: 2011-03-31 23:05:54+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/03/31/zemlja-znanja/
slug: zemlja-znanja
title: "Zemlja znanja"
wordpress_id: 307
---

Trenutno radim kao nastavnik matematike u osnovnoj školi u
Križevcima. Zanimljivo je raditi u školi jer štošta saznaš i vidiš. Na
primjer, svaki dan pri dolasku u učionicu u kojoj držim nastavu vidim
vrata učionice koja ostavljaju dojam kao da se iza njih krije ruševna
štala. Učionica je takvih dimenzija da svi skupa jedva unutra
stanemo. Zavjesa u učionici koja gleda na zapad nema jer je,
pretpostavljam, šteta propustiti makar jednu zraku sunca za ovih
prelijepih proljetnih dana. Tako su đaci na satu matematike prepušteni
zalascima sunca jer se na ploču ionako ne vidi.

Danas je bilo učiteljsko vijeće i po već tko zna koji put čuo sam da
je država školi smanjila budžet. Bit će da je tome tako jer živimo u
zemlji znanja. Sigurno smo radi toga u školi trebali šparati na
grijanju za hladnijih dana. Ili da se izrazim rječnikom resornog
ministarstva --- racionalizirali smo potrošnju.

Država šalje jasnu poruku o tome koliko smatra obrazovanje bitnim. Ona
se iščitava, među ostalim, iz iznosa plaća prosvjetara i općenito
uvjeta rada. Kao da to već nije dovoljno pa država prosvjetarima kaže
i da se trebaju prekvalificirati za projektne menadžere. Naime, logika
je ova --- ako škola treba novac, a država ga za školstvo nema i ne
želi imati, neka prosvjetari smisle projekte i prijave ih na natječaje
koji izvlače novac iz pretpristupnih fondova Europske unije. Tako će
škole s najboljim projektnim menadžerima-prosvjetarima "uspjeti" i
proizvodit će đake koji će jednog dana biti konkurentniji na tržištu
rada. Da škola postoji zbog tržišta rada podsjetio me jedan izdavač
školskih udžbenika. U prijedlogu pripreme za nastavu matematike
za 5. razrede osnovne škole tako stoji da đake treba "upozoriti na
činjenicu da ih čeka cjeloživotno učenje (da bi opstali i bili
konkurentni na tržištu rada)". Da, radi se o djeci u 5. razredu i radi
se o državnoj školi.


> Ej, ako ti znaš di je ta pravda, reci meni jer vidim tu bi moglo bit
> para.

> Kad bi ja zna di je, bia bi svemirski diler i prodava bi tu tvoju
> pravdu na kile.

> Znam di je kvaka, nisam ja od juče. Svugdi ima luđaka koje želja za
> tim vuče.

> To je prilika za biznis, ne budi tele, to triba prodat svima koji to
> žele.

> > TBF u pjesmi Tobogan

		
