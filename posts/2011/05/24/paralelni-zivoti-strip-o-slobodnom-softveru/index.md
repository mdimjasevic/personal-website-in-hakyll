---
author: Marko Dimjašević
comments: false
date: 2011-05-24 16:28:05+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/05/24/paralelni-zivoti-strip-o-slobodnom-softveru/
slug: paralelni-zivoti-strip-o-slobodnom-softveru
title: "Paralelni životi — strip o slobodnom softveru"
wordpress_id: 338
---

_Prije tri tjedna pomogao sam Andreju Dundoviću napisati [vijest][lzs]
za portal "Linux Za Sve" pa ću taj tekst, uz neke izmjene i nadopune,
staviti i ovdje na blog._

U sklopu projekta "[Sembrando Libertad][sl-archived]" i kao dio knjige
"Slobodan softver za male ljude" nastao je kratki strip za djecu
Paralelni životi. Strip je priča o dva svijeta --- prvi prožet
vlasničkim, a drugi slobodnim softverom. Prvi svijet, barem u početku,
izgleda blještavo, no brzo se ispostavi da ima brojna ograničenja. Na
kraju djeca uz pomoć djece "iz svijeta" slobodnog softvera nauče da
postoji softver koji ih ne ograničava i koji donosi jedan novi
pozitivni skup vrijednosti koji pomaganje i suradnju drži jako
bitnima.


[![Strip o slobodnom softveru](strip.png "Strip o slobodnom softveru"){ width=75% }][pdf]


Strip je napravljen slobodnim softverom, promiče slobodan softver ---
konkretno, potpuno slobodnu distribuciju GNU/Linuxa
[Trisquel](https://trisquel.info/) i uredske alate
[LibreOffice](https://www.libreoffice.org/) --- te je ujedno dio
[slobodne kulture](http://freedomdefined.org/Definition). Preveo sam
ga na hrvatski te je, zahvaljujući zajednici slobodnog softvera,
također dostupan na engleskom, španjolskom, portugalskom, francuskom,
ruskom, njemačkom i islandskom jeziku.


Bilo bi lijepo u informatičkim učionicama osnovnih i srednjih škola
vidjeti ovaj strip umjesto plakata koji reklamira neslobodni
operacijski sustav uz poruku: "Dostupno za kupovinu".

Strip je prvotno bio dostupan samo u inačici koja je umjesto Trisquela
i LibreOfficea promicala Ubuntu i OpenOffice.org. Nakon toga je,
obzirom da se radi o slobodnoj kulturi, netko uzeo strip i napravio
inačicu koja promiče potpuno slobodan softver i softver koji je više u
duhu slobodnog softvera. Problem s Ubuntuom je što sadrži vlasnički
softver kao što ga i promiče, a OpenOffice.org promiče vlasnički
softver u obliku raznih proširenja.

Hvala [Andreju](https://identi.ca/adundovi) na komentarima tokom
prevođenja kao i Nataši Dimitrijević za lekturu. Ako nađete koju
grešku u prijevodu, koju pravopisnu, jezičnu ili bilo koju drugu
grešku, one, naravno, pripadaju meni. Strip je dostupan u dva formata:
[PDF][pdf] i [SVG][svg].

[sl-archived]: https://web.archive.org/web/20140722212022/http://www.sembrandolibertad.org.ar/libros/libro-1/
[lzs]: http://web.archive.org/web/20190530064348/http://www.linuxzasve.com/strip-o-slobodnom-softveru
[svg]: https://web.archive.org/web/20140722212022/http://wiki.gleducar.org.ar/images/b/b1/SL-vidas_paralelas-hr.svg
[pdf]: https://web.archive.org/web/20140722212022/http://wiki.gleducar.org.ar/images/d/db/SL-vidas_paralelas-hr.pdf
