---
author: Marko Dimjašević
comments: false
date: 2011-05-10 16:57:43+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/05/10/word-dokument-u-privitku/
slug: word-dokument-u-privitku
title: "Word dokument u privitku"
wordpress_id: 322
---

U zadnje vrijeme primio sam nekoliko e-mailova koji imaju Word
dokumente (.doc) u privitku. Ta je praksa, naravno, vrlo loša pa sam
odlučio sastaviti kratki tekst za ljude s kojima se dopisujem, a šalju
mi .doc privitke. Imao sam pripremljen tekst iste namjene i prije, no
mislio sam da ga treba ažurirati. Prvenstveno zbog toga što .doc
format više nije tajan (od 2008. kad je objavljena specifikacija) i
zato što postoji ne toliko loša podrška za .doc format u uredskim
alatima u slobodnom softveru (npr. LibreOffice Writer).

Postoji nekoliko dobrih tekstova koji razmatraju dotičnu problematiku
pa sam odlučio spojiti
[nekoliko](http://web.math.hr/~mdoko/no-word-attachments.hr.html)
[njih](https://fsfe.pad.foebud.org/en-nonfreedocs) u koliko-toliko
kratki tekst. Ukratko, sugovornika pozivam da mi dokumente šalje u
ODF-u, HTML-u ili PDF-u, namjerno izostavljajući _plain text_ (.txt
datoteke) jer mi "problematični" sugovornici ionako nikad ni ne šalju
.txt privitke. Slijedi tekst pa molim za komentare! Slobodno koristite
isti tekst za svoje sugovornike.


<blockquote>

Poslali ste mi privitak u vlasničkom formatu Microsoft Word. Takav
format datoteke sa svim tehničkim mogućnostima je moguće koristiti
samo u istoimenom softverskom alatu, prvenstveno zbog softverskih
patenata kojima Microsoft koči tržišno natjecanje. Ako želite da
pročitam dotični dokument, molim Vas da ga pošaljete u ODF, HTML ili
PDF formatu.

Slanje dokumenata u Word formatu je loše za Vas i za ostale. Ne možete
biti sigurni kako će izgledati dokument ako ga netko otvori pomoću
softvera koji nije Microsoft Word, a možda uopće neće raditi.

Slanje dokumenata u Word formatu je loše za Vas jer Word dokumenti
obično nose skrivene informacije o autoru. To radoznaloj strani
omogućava uvid u autorove aktivnosti (možda Vaše). Tekst koji mislite
da ste izbrisali možda je još uvijek prisutan i može dovesti do
neugodnih situacija. Pogledajte
http://news.bbc.co.uk/2/hi/technology/3154479.stm za više informacija.

No povrh svega, slanje Word dokumenata stavlja pritisak na druge da
koriste Microsoftov softver te im je tako uskraćen izbor. Tako
postajete podupirač Microsofta, osuđenog monopolista. Taj pritisak
glavna je prepreka za šire usvajanje slobodnog softvera. Hoćete li,
molim Vas, preispitati korištenje Word formata za komunikaciju s
drugim ljudima?

Dokument mi možete poslati i u Otvorenom formatu dokumenata (ODF) koji
je norma u Hrvatskoj i u svijetu (Hrvatski zavod za norme --- HRN
ISO/IEC 26300:2008; ISO --- ISO/IEC 26300:2006).

Lako je pretvoriti datoteku u HTML koristeći Word. Otvorite dokument,
kliknite na File (Datoteka), onda na Save As (Spremi kao) i u okviru
tipova datoteka odaberite HTML Document ili Web Page. Tada kliknite na
Save (Spremi). Nakon toga u e-mailu možete priložiti novi HTML
dokument umjesto Word dokumenta. Word se mijenja na nekonzistentan
način pa ako upravo dane upute ne odgovaraju onome što imate na
ekranu, probajte na slične načine.

Na svojem računalu možda imate program za pretvorbu u PDF
format. Odaberite Datoteka (File) => Ispis (Print). Pretražite listu
dostupnih pisača i odaberite PDF pretvornik. Kliknite na gumb Ispis i
--- ako je potrebno --- na zahtjev upišite naziv PDF datoteke.

</blockquote>
