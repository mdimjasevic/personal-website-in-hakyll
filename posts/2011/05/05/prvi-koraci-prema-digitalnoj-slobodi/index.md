---
author: Marko Dimjašević
comments: false
date: 2011-05-05 19:22:01+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/05/05/prvi-koraci-prema-digitalnoj-slobodi/
slug: prvi-koraci-prema-digitalnoj-slobodi
title: "Prvi koraci prema digitalnoj slobodi"
wordpress_id: 316
tags:
- slobodan softver
- teknologija narodu
---

**DOPUNA:** Članak je objavljen na portalu
[križevci.info](http://www.krizevci.info/kolumne/111-teknologija-narodu/8772-prvi-koraci-prema-digitalnoj-slobodi)
u kolumni Teknologija narodu.

Osobna računala postala su dio inventara više-manje svakog
kućanstva. Obzirom da je takvo računalo vlasništvo pojedinca, normalno
bi bilo pretpostaviti da vlasnik može s tim računalom napraviti što
god želi, odnosno da vlasnik ima kontrolu nad računalom. Današnja je
situacija zapravo dijametralno suprotna zahvaljujući potrebi za
softverom i neupućenosti korisnika računala. Naime, za rad računala je
potreban softver. Tu spadaju operacijski sustav i razne korisničke
aplikacije. Ako korisnik na računalu ima instalirane Prozore ili
Dizajnersku nepoznanicu, on nema kontrolu nad vlastitim računalom jer
su oba proizvoda --- redmondski i kupertinski --- zapravo vlasnički
softver, a takav korisniku oduzima kontrolu nad računalom. Trenutak u
kojem se korisnik odriče kontrole nad vlastitim računalom je klik na
gumb "I Agree" pri instalaciji ili prvom pokretanju softvera iza kojeg
stoji pristanak na prepuštanje kontrole nad računalom nekoj trećoj
strani, odnosno proizvođaču softvera. Stranica [Najsramotnije
EULA-e](http://www.eulahallofshame.com/) donosi brojne besmislice koje
se kriju u pravnim bespućima licencnih uvjeta za krajnje korisnike
(eng. End User License Agreement) na koje korisnik pristaje
sudbonosnim "I Agree", a vrijedi izdvojiti nekoliko njih:
	
* ne smijete koristiti Microsoftov softver FrontPage kako bi rekli
  nešto [protiv
  Microsofta](http://www.eulahallofshame.com/microsoft/frontpage.html),
* ako imate operacijski sustav Microsoft Vista, pristali ste na to da
  Microsoft prilikom aktivacije sustava prikupi vaše "osobne
  informacije" i "obrađuje" ih na način i na mjestu gdje se Microsoftu
  svidi,
* ako koristite koju Googleovu uslugu (npr. Gmail), pristajete na
  [stalnu promjenu uvjeta
  korištenja](http://www.eulahallofshame.com/google.html) bez da vas
  Google o obavijesti kad se dogodi promjena,
* Yahoo u svojim uvjetima korištenja za razne usluge smatra da bi
  njihovi uvjeti korištenja, [potencijalno
  protuzakoniti](http://www.eulahallofshame.com/yahoo-tos.html),
  trebali biti iznad državnih zakona.


Da bi korisnik imao kontrolu nad vlastitim računalom, trebao bi
koristiti softver koji je transparentan, koji nema što skrivati od
korisnika, koji ne nameće dodatna ograničenja (država je već nametnula
ograničenja raznim zakonima), kojeg je moguće prilagođavati i koji je
nematerijalno dobro za zajednicu. Takav je slobodan softver.

Četiri slobode
[definiraju](http://fsfe.org/about/basics/freesoftware.hr.html)
slobodan softver:
	
* sloboda pokretanja programa u bilo koju svrhu,
* sloboda proučavanja načina na koji program radi i prilagođavanja
  istog svojim potrebama,
* sloboda redistribucije kopija kako bi se pomoglo bližnjemu i
* sloboda poboljšavanja programa i objavljivanja vaših poboljšanja
  javnosti, u korist cijele zajednice.


Te četiri softverske slobode u zadnjih 25 i više godina stvorile su
zajednicu milijuna entuzijasta i zaposlenika brojnih tvrtki koji su
odlučili međusobno dijeliti i pomagati si. Rezultate svoga rada nisu
zadržali samo za sebe, već su ih nesebično podijelili sa
svima. Stvorili su kulturnu baštinu, nevjerojatno bogatstvo znanja
svima dostupno, ali i tehnologiju koja našu svakodnevicu čini
boljom. Tako postoji slobodan softver za obradu teksta, slika,
surfanje webom, čitanje e-pošte, reprodukciju multimedijalnog
sadržaja, upravljanje infrastrukturom Interneta, [upravljanje
burzama](http://www.fsf.org/working-together/profiles/new-york-stock-exchange),
za [digitalnu
enciklopediju](http://www.fsf.org/working-together/profiles/wikipedia),
nalazi primjenu u [nacionalnoj
sigurnosti](http://www.fsf.org/working-together/profiles/department-of-defense),
u predviđanju vremenskih uvjeta na super-računalima, u
[mobitelima](https://secure.wikimedia.org/wikipedia/en/wiki/Android_%28operating_system%29)...

Pitanja slobode, kontrole i moći --- kada se radi o softveru --- u
početku su mi bila preapstraktna. U svijet slobodnog softvera ušao sam
koristeći konkretne tehnologije pa će čitatelj možda odlučiti započeti
na isti način. Sa slobodnim softverom --- izuzev korištenja Firefoxa
--- započeo sam drastičnim rezovima u smislu softvera kojeg koristim
za svaki oblik rada na računalu. Bez ikakvog predznanja o toj
slobodnoj i za mene novoj tehnologiji, izbrisao sam instalaciju
Prozora i drugog vlasničkog softvera i umjesto toga instalirao sam
operacijski sustav [GNU/Linux](http://www.gnu.org/). Na taj način bio
sam prisiljen brzo naučiti kako se snaći u novom okruženju. Čitatelju
ne preporučujem tako veliki skok, iako se tako nauči zaista puno u
vrlo kratkom periodu. Postepena zamjena vlasničkog softvera slobodnim
softverom čini se manje zahtjevnom za prosječnog korisnika računala,
no ako je netko odvažan ili se smatra informatički pismenim i voljan
je odmah u potpunosti preuzeti kontrolu nad vlastitim računalom, [rado
ću pomoći](/about/) u prvim, ali i kasnijim koracima.

Zakoračiti prema digitalnoj slobodi čitatelj može tako da na računalo
instalira neke popularne aplikacije unutar operacijskog sustava kojeg
trenutno koristi. Primjeri su:

* [Firefox](https://www.mozilla.com/hr/firefox/) za surfanje webom,
* [Thunderbird](https://www.mozillamessaging.com/en-US/thunderbird/)
  za e-poštu,
* [LibreOffice](http://www.libreoffice.org/) za uređivanje teksta,
  prezentacija i proračunskih tablica,
* [VLC](http://www.videolan.org/vlc/) za multimedijalni sadržaj i
* na web-stranici
  [PDFreaders.org](http://pdfreaders.org/index.hr.html) možete pronaći
  programe za pregledavanje PDF dokumenata.

Zainteresiranom čitatelju preporučujem pronalaženje zamjene u
slobodnom softveru za svaku aplikaciju vlasničkog softvera. Jedan
repozitorij aplikacija slobodnog softvera kategoriziran po
funkcionalnosti moguće je pronaći na web-stranicama [Free Software
Foundationa](http://directory.fsf.org/).
