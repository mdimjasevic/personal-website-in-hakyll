---
author: Marko Dimjašević
comments: false
date: 2011-05-21 10:18:31+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/05/21/parabola-je-i-sluzbeno-slobodna/
slug: parabola-je-i-sluzbeno-slobodna
title: "Parabola je i \"službeno\" slobodna"
wordpress_id: 328
---

Jučer je Free Software Foundation, organizacija za promicanje
slobodnog softvera,
[uvrstio](http://www.fsf.org/blogs/licensing/parabola-gnu-linux-joins-the-fsf-list-of-free-distributions)
[Parabolu](http://parabolagnulinux.org/) u svoj
[popis](http://www.gnu.org/distros/free-distros.html) potpuno
slobodnih distribucija GNU/Linuxa. Parabola je i prije uvrštavanja u
taj popis bila slobodna, no kad dotični Free Software Foundation kaže
da je nešto u softveru slobodno, to ima dodatnu težinu. Ta objava me
veseli jer sam nekoliko mjeseci [korisnik
Parabole](/2011/02/13/skoro-pa-slobodan/).

I dok ću svakome preporučiti da se "prebaci" na GNU/Linux i općenito
slobodan softver, neću svakome preporučiti da počne s
Parabolom. Naime, sam prelazak s vlasničkog operacijskog sustava kao
što su Windows ili OS X na GNU/Linux je velika promjena sama po sebi
pa ne treba pretjerivati i očekivati da će se višegodišnji korisnik
miša i grafičkog okruženja odmah snaći u
[distribuciji](https://secure.wikimedia.org/wikipedia/en/wiki/GNU/Linux_distribution)
koja uopće ne dolazi s grafičkim sučeljem. Ako niste znali, kad po
prvi puta ubacite CD s Parabolom i pokrenete "live" inačicu, zaslon
računala ispunjen je sivim monotonim znakovima na crnoj pozadini i
izgleda ovako:

> (Ups, ova slika je nestala s weba u međuvremenu!)
<!-- http://img520.imageshack.us/img520/9903/dsc00092rx3.jpg -->

Na vama je da primite tipkovnicu i počnete računalu zadavati naredbe!
Mnogim korisnicima je izostanak grafičkog sučelja vrlo odbojan, no
računalni geekovi će vam potvrditi da se mnoge stvari mogu obaviti
brže i jednostavnije kroz to tzv. komandno-linijsko sučelje nego u
grafičkom sučelju!

Priznajem potrebu za grafičkim sučeljem i sam bih teško obavljao
svakodnevne aktivnosti na računalu bez grafičkog sučelja pa sam nakon
instalacije Parabole instalirao grafičko sučelje
[KDE](http://kde.org/). Mnogim će korisnicima izgled KDE-a biti oku
ugodniji od komandno-linijskog sučelja:

[![](46-w09.png){ width=95% }](46-w09.png)

Sada imam operacijski sustav po mjeri. Imam instaliran samo slobodan
softver, uvijek najnovije inačice, lak za održavanje, relativno brz,
oku ugodan i (dijelom) preveden na hrvatski.
