---
author: Marko Dimjašević
comments: false
date: 2011-05-03 16:23:52+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/05/03/softver-kao-javno-dobro/
slug: softver-kao-javno-dobro
title: "Softver kao javno dobro"
wordpress_id: 310
---

Chris Puttick, student ekonomije, iznio je odličnu analizu zašto je
softver javno dobro. Dva su kriterija da bi neko dobro bilo javno
dobro --- kriterij nekonkurentnosti i kriterij
neisključivosti. Ukratko, moje korištenje softvera ne isključuje
nikoga drugoga u istovremenom korištenju istog softvera ([kriterij
nekonkurentnosti](https://secure.wikimedia.org/wikipedia/en/wiki/Rivalry_%28economics%29))
jer svatko može imati svoju identičnu digitalnu kopiju, tj. primjerak
dotičnog softvera. Iako se brojne tvrtke-proizvođači trude onemogućiti
korištenje softvera kojeg su napisali bez da ga korisnik prvo plati,
korisnici uvijek nađu načina da ga koriste bez plaćanja. To je tako
jer je napraviti novu (digitalnu) kopiju softvera iznimno lako, a
sastoji se "copy-pastea", tj. jednostavne radnje kopiranja i
lijepljenja ili dijeljenja softvera putem BitTorrenta ili na neki
drugi jednostavan način. Drugim riječima, softver zadovoljava i
[kriterij
neisključivosti](https://secure.wikimedia.org/wikipedia/en/wiki/Non-excludable_good),
tj. softver je javno dobro.

S pravom se možemo pitati zašto onda uopće postoji vlasnički softver,
a to je onaj koji, među ostalim, pokušava biti oskudan i
isključiv. Odgovor je jednostavan --- proizvođači vlasničkog softvera
žele zgrnuti basnoslovne iznose uvođenjem umjetnih ograničenja potpuno
nesvojstvenih javnom dobru kao što je softver. Žele proizvesti samo
jednom, a prodati potencijalno beskonačno mnogo puta. Uvode oskudnost
i isključivost tamo gdje one ne postoje. I s ovog stajališta, jedino
logično stanje softvera je da bude slobodan softver.

Cijelu analizu možete pročitati na blogu dotičnog [Chrisa
Putticka](http://putt1ck.blogspot.com/2011/05/public-good-of-software.html).
