---
author: Marko Dimjašević
comments: true
date: 2011-05-28 09:43:22+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/05/28/o-informaticko-drustvenom-aspektu-naseg-obrazovnog-sustava/
slug: o-informaticko-drustvenom-aspektu-naseg-obrazovnog-sustava
title: "O informatičko-društvenom aspektu našeg obrazovnog sustava"
wordpress_id: 349
tags:
- BSA
- javno dobro
- Microsoft
- obrazovanje
- slobodan softver
- teknologija narodu
---

**DOPUNA:** Članak je objavljen na portalu
[križevci.info](http://www.krizevci.info/kolumne/111-teknologija-narodu/8868-o-informatiko-drutvenom-aspektu-naeg-obrazovnog-sustava)
u kolumni Teknologija narodu.

Informacijske, odnosno digitalne tehnologije dio su naše
svakodnevice. Prisutne su u obliku osobnih računala, mobilnih
telefona, dizala, nadzornih sustava, zrakoplovnih upravljačkih
sustava, sve češće ugrađuju se u automobile, dio su informatiziranog
zdravstvenog sustava itd. Način na koji doživljavamo i razumijemo
svijet oko sebe te kako se ponašamo pod sve je većom kontrolom
tehnologije, ali i onih koji kontroliraju tehnologiju. Stoga je
uvođenje predmeta informatike u škole, odnosno obrazovni sustav sasvim
opravdan i očekivan potez. Potrebno je dijelom obrazovanja učiniti i
usvajanje znanja o toj novoj sveprisutnoj tehnologiji kako bi noviji
naraštaji ovladali tom tehnologijom.

Unatoč evidentnoj potrebi, predmet informatike u osnovnim je školama
tek izborni predmet. Ipak se u svakoj školi nalazi informatička
učionica i u svakoj od njih nalazi se određeni broj starijih ili
novijih računala. Računalo bez softvera nije previše
upotrebljivo. Zato svako računalo treba operacijski sustav i
korisničke aplikacije. U našim je školama pravilo da je operacijski
sustav jedan od Microsoft Windowsa, iako ga ne propisuje nastavni
program Ministarstva znanosti, obrazovanja i športa. Tome nije tako
samo u školama, već i na fakultetima. Kad je dotični Microsoft
uspostavio monopol nad tržištem operacijskih sustava, iskoristio je
priliku pa je uspostavio monopol i na tržištu uredskih alata. Ako se
na računalu nalazi Windows, u pravilu će se nalaziti i Microsoft
Office. Dakle, djeca i mladi kroz cijeli obrazovni sustav stasaju uz
proizvode jedne korporacije.

Razmišljanje tipa: "Ili Microsoftovi proizvodi ili neupotrebljivo
računalo?" je lažna dilema. Microsoftov operacijski sustav i uredski
alati nisu jedini "na tržištu"; dapače, postoji mnoštvo drugih,
posebice onih koji su slobodan softver.

Ta naoko bezazlena sveprisutnost Windowsa u obrazovnom sustavu ima
dalekosežne posljedice na sustav vrijednosti u društvu, monokulturu u
informacijsko-tehnološkom sektoru, kriminal, gospodarstvo i
suverenitet države, a indirektno i na razvijanje kritičkog
razmišljanja.

Državne škole i fakulteti, dakle, uče nove naraštaje korištenju
vlasničkog proizvoda multinacionalne korporacije Microsoft. Drugim
riječima, mi kao građani ove države, umjesto da smo se opredijelili za
znanje o novim tehnologijama i usvajanje načina na koji rade te
digitalne tehnologije, opredijelili smo se za oblikovanje mladih u
konzumente proizvoda točno određene korporacije. Da apsurd bude veći,
tok novčane nagrade za širenje tržišta dotične korporacije na
cjelokupnu populaciju ne ide od Microsofta prema državi, već od
države, tj. nas državnih poreznika prema Microsoftu.

Iz odgojno-obrazovnih institucija "izlaze" mladi sposobni za
korištenje jednog sustava koji po mnogo pitanja uopće nije
interoperabilan s drugim računalnim sustavima. Stvaramo društvo ovisno
o proizvodima jedne tehnološke korporacije. Kada društvo stvara puke
konzumente, što valja očekivati po pitanju radničkih zahtjeva o
informatičkoj tehnologiji na radnom mjestu bilo u privatnom sektoru
bilo u državnim službama? Društvo zna i želi koristiti samo proizvode
točno određene korporacije. Kako i ne bi kad u državnoj osnovnoj školi
u informatičkoj učionici --- u kojoj se informatički oblikuju djeca
starosti 11 do 15 godina --- stoji plakat sa zadanim zaslonom Windowsa
XP i na njemu piše: "Dostupno za kupovinu!" Nije li zabrinjavajuće što
su škole postale marketinški prostor softverske buržoazije pri čemu su
ciljana skupina tog marketinga upravo djeca? Ne bi li bilo bolje da
škole izvjese [strip][strip] koji promiče pomaganje i suradnju?

Ruku na srce, rijetki imaju dovoljno novca i još rjeđi su spremni
platiti licencu za bilo koji operacijski sustav Windows (usput rečeno,
Microsoft ne prodaje operacijski sustav, već licencu za korištenje
istog). Obzirom da Microsoft kroz tržište neće plasirati primjerak
Windowsa bez da mu za isti platite i obzirom da cijena za hrvatske
prilike nije prihvatljiva, kompaktni disk s Windowsima već nekako nađe
put do korisnikovog računala bez posredstva Microsofta. Drugim
riječima, postoji izvjestan broj nelegalnih primjeraka koji se
udomaćio na računalima diljem Lijepe naše. Dapače, usudio bih se reći
da je broj nelegalnih primjeraka dotičnog softvera kudikamo veći od
broja legalnih primjeraka istog tog softvera. Dovoljno je osvrnuti se
oko sebe i pogledati tko uopće od prijatelja, poznanika ili susjeda na
svom računalu ima legalan primjerak Windowsa.

Microsoft uz pomoć države usmjerava studente orijentirane na
računarstvo i informatiku na ovisnost o Microsoftovim vlasničkim
proizvodima. Tako država financira studije ili dijelove studijskih
programa za obuku studenata u korištenju Microsoftovih vlasničkih
tehnologija. Primjera radi, studenti jednog od najpoznatijih i
najuglednijih domaćih fakulteta po pitanju računarstva --- zagrebački
Fakultet elektrotehnike i računarstva --- besplatno s Interneta mogu
preuzeti operacijski sustav iz serije Microsoft Windows i razvojne
alate Microsoft Visual Studio. Kroz nekoliko kolegija potiče ili
diktira korištenje dotičnih vlasničkih tehnologija. Fakultet time
očito potiče mono-kulturu prisutnu u informacijsko-tehnološkom
sektoru. Radi li time Fakultet uslugu studentima ili nekome drugom?

Kroz program naziva Microsoft Student Partners tvrtka Microsoft traži
od studenata da budu njihova produžena ruka i da sudjeluju u
utvrđivanju Microsoftove dominacije u brojnim aspektima informacijske
tehnologije. Tako studenti postaju besplatna radna snaga koja svjesno
ili nesvjesno sudjeluje u stvaranju i održavanju međunarodnog
imperija, inače osuđenog monopolista i u EU-u i u SAD-u. Ako je suditi
po [članku na
Wikipediji](https://secure.wikimedia.org/wikipedia/en/wiki/Microsoft_Student_Partners#Purpose),
cilj je tog programa podići svijest među studentskom populacijom o
Microsoftovim tehnologijama. Kad smo kod svijesti i kritičkog gledanja
na dotične tehnologije, iskreno se nadam da je Microsoft vjeran tome
cilju pa da studentima ukazuje na punu problematiku vlasničkog
softvera općenito i konkretno ukazuje na cijeli spektar problema koji
proizlaze iz svojstva vlasničkog softvera kakvi jesu Microsoftovi
proizvodi. S druge pak strane itekako sam svjestan da do toga nikada
neće doći i da će Microsoft prešutjeti najbitniji dio priče oko svojih
vlasničkih tehnologija. Naime, bilo bi protivno korporativnim
interesima reći da Microsoft zapravo uvodi oskudnost tamo gdje ona ne
postoji jer, ekonomski gledano, [softver je javno
dobro](http://putt1ck.blogspot.com/2011/05/public-good-of-software.html),
da uvodi nekompatibilnosti u računalne sustave, da korisnike ostavlja
na milosti i nemilosti istog tog Microsofta, da kao i duhanske tvrtke
u nekim prilikama besplatno dijeli svoje proizvode kako bi stvorio
ovisnost o istima i time povećao svoj udio na tržištu, da od tih
studenata "partnera" očekuje da na blogovima, news-grupama, forumima,
portalima i mailing listama brane Microsoftove interese bez obzira
bili oni moralni ili ne. Tko se toga neće držati, čeka ga sankcija u
obliku izbacivanja iz "partnerskog" programa.

Jeste li ikad pomislili da bi mnoga djeca i mladi mogli biti
kriminalci? Neautorizirano umnažanje Microsoft Windowsa i Microsoft
Officea je protuzakonito. Opet, s druge strane, spržiti CD je
nevjerojatno lako i jeftino i upravo presnimavanjem Windowsa ili
Officea svatko od nas može pomoći prijatelju da obavi njemu bitnu
radnju na računalu. Djeca i mladi uglavnom ne mare o nezakonitosti
umnažanja tog softvera pa naprave kopiju i za svoje prijatelje. Je li
problem u zakonu koji zabranjuje takvo umnažanje ili u tome što je
dotičan softver vlasnički? Kako god, trebamo se zapitati kakva je
budućnost društva čiji mladi odrastaju s mišlju da su kriminalci.

Microsoft ima itekakve koristi od toga što su se građani koji su
"primorani" na nelicencirane inačice operacijskog sustava i uredskih
alata --- bilo zbog toga što država od njih zahtijeva da koriste
upravo Microsoftove proizvode kao što to radi FINA, bilo zbog
nekompatibilnosti dominantnih Microsoftovih proizvoda sa drugim
računalnim sustavima --- odlučili protuzakonito nabaviti upravo
Microsoftove proizvode. Korporacija želi dominaciju na tržištu i ako
je tzv. piratstvo jedini način da to postigne u nerazvijenim zemljama
kao što je Hrvatska, neće imati ništa protiv. Dapače, sam Bill Gates,
osnivač Microsofta, [izjavio
je](http://news.cnet.com/2100-1023-212942.html): "Dok god će [ljudi]
krasti, želimo da kradu od nas. Na neki će način postati ovisni i tada
ćemo odgonetnuti kako to naplatiti u narednom desetljeću." Ono što je
Microsoftu daleko bitnije od toga da naplati softver je baza
korisnika. U tom je svjetlu krajnje licemjerno kada Microsoft kroz
[BSA](http://www.bsa.org/), kartel softverskih korporacija čiji je
član,
[primijećuje](http://www.bsa.org/country/News%20and%20Events/News%20Archives/global/05062011-idc-globalpiracystudy.aspx)
kako Hrvatska, gledano u okviru Europske Unije kojoj službena politika
teži, ima 7. najvišu stopu piratstva te kako će nastaviti "s
aktivnostima usmjerenim na promicanje i zaštitu prava intelektualnog
vlasništva". Drugim riječima, Microsoft je istovremeno za i protiv
piratstva --- on je za kada želi dominantan položaj "na tržištu", a
protiv je kada treba igrati ulogu uzorne tvrtke koju svi žele u svojoj
državi. Da nije piratstva u Hrvatskoj, Microsoft ne bi mogao ni
sanjati o tolikoj bazi korisnika koju sad ima. Bio bi ostavljen na
margini softverskog tržišta jer si građani s ovakvim plaćama i
standardom jednostavno ne mogu svakih nekoliko godina priuštiti trošak
od par tisuća kuna za nabavku operacijskog sustava i uredskih alata.

Pitanje koje mislim da si svi moramo postaviti je: "U kakvom društvu
želimo živjeti?" Jasna predodžba o sustavu vrijednosti koji u društvu
želimo njegovati upućuje nas na prepoznavanje trenutnog stanja u
društvu i potiče na razmišljanje o tome što valja mijenjati kako bismo
postali društvo kakvo priželjkujemo. Želimo li živjeti u društvu u
kojem su dijeljenje, pomaganje, suradnja i transparentnost procesa
temeljne vrijednosti? Smatram da je vrijedno i potrebno težiti takvom
društvu. Trenutno stanje društva gdje smo svi slijepi na zbivanja u
užem ili širem okruženju, gdje je izvjestan manjak ili bolje rečeno
nedostatak propitivanja onog što nam se servira u svakom smislu te
riječi, gdje vlada rezignacija i podilaženje nametnutim idejama i
okruženju nije stanje slobodnog demokratskog društva niti vodi ka
istom. Prije bi se moglo reći da je takvo društvo podložno najširim
manipulacijama.

Spomenuta sveprisutnost Windowsa pospješitelj je te otuđenosti jer je
taj operacijski sustav vlasnički softver. Kao što je već spomenuto,
softver je javno dobro, no razvijatelji vlasničkog softvera ne žele da
javnost zna za tu činjenicu. Kada bi javnost saznala, postalo bi jasno
kao dan da su poslovni modeli oko vlasničkog softvera umjetni i bez
temelja u ekonomiji. Dodatno, vlasnički softver nosi socio-psihološke
posljedice za korisnike jer zabranjuje pomaganje susjedu kao i široj
zajednici. Pojedincu uskraćuje mogućnost zadovoljenja želje za znanjem
koje bi mogao steći proučavanjem načina rada softvera. Nekritičkim
usvajanjem Windowsa kao modusa operandi društva kroz nastavu
informatike i nastavu općenito etablirali smo sustav vrijednosti koji
kaže da su umjetna oskudnost, sebičnost, otuđenost i nekritičnost ne
samo prihvatljive, već i poželjne vrijednosti.

[strip]: https://web.archive.org/web/20140722212022/http://wiki.gleducar.org.ar/images/d/db/SL-vidas_paralelas-hr.pdf
