---
author: Marko Dimjašević
comments: false
date: 2011-09-19 21:56:29+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/09/19/dan-slobodnog-softvera-2011-u-krizevcima/
slug: dan-slobodnog-softvera-2011-u-krizevcima
title: "Dan slobodnog softvera 2011. u Križevcima"
wordpress_id: 371
categories:
- volontiranje
tags:
- Dan slobodnog softvera
- Križevci
- Mozilla Hrvatska
- SFD
- slobodan softver
- Slobodan softver Hrvatska
---

Ove je subote bio svjetski Dan slobodnog softvera i na [popisu od
preko 400
gradova](http://softwarefreedomday.org/map/index.php?year=2011) u
kojima se obilježio taj dan po prvi puta su se našli Križevci
zahvaljujući novim entitetima na domaćoj softverskoj sceni ---
[Slobodan softver Hrvatska](http://www.slobodansoftver.hr/) (SSH) i
[Mozilla Hrvatska](http://mozilla-hr.org/). Bilo bi super da je SSH
organizirao ovo događanje u još kojem gradu, no nismo se uspjeli
organizirati. Zbivanje je bilo na otvorenom na središnjem
Strossmayerovom trgu i privuklo je stotinjak građana čime smo mi
organizatori zadovoljni! Dodatnu čar organiziranju takvog čega dalo je
poklapanje Dana slobodnog softvera u ovoj 2011. godini --- inače svake
treće subote u rujnu --- s dvadesetom godišnjicom oslobođenja vojarne
u Križevcima i s izviđačkim aktivnostima oko šatora podignutog 10-ak
metara od paviljona gdje smo mi promicali slobodan softver.


[![](img_20110917_110257.jpg){ width=100% }](img_20110917_110257.jpg)


Sama provedba ovog obilježavanja vidjela je tek jednog člana SSH-a
(mene), no spas je bio u zajednici Mozilla Hrvatska koja je brojila 5
organizatora sa svoje strane. Kromatsko sivilo koje su pružali letci
SSH-a nastalo nedostatkom novčanih sredstava za tisak u boji razbili
su promotivni materijali koje je Mozilla isporučila zajednici
korisnika u Hrvatskoj. Zato se na godinu treba dovoljno rano prijaviti
na [webu Dana slobodnog softvera](http://softwarefreedomday.org/) kako
bi Software Freedom International, međunarodni koordinator ovog dana,
isporučio promotivne materijale. Možda su nedostajali veći natpisi
koji bi privukli slučajnog prolaznika, no stvar je spasila Mozillianka
Ivona koja je obukla role i širom trga veselo promicala Firefox i
općenito slobodan softver; definitivno jedan od boljih poteza samog
događanja!


Od kako znam za slobodan softver i taj termin na hrvatskome jeziku,
bio sam sretan što on nema višeznačnosti kao što ima u engleskom
jeziku gdje _free_ u _free software_ može značiti, među ostalim,
slobodan, ali i besplatan. Stoga sve do ove subote nisam smatrao
potrebnim odmah na početku davanja objašnjenja što je slobodan softver
naglasiti da slobodan softver nije pitanje cijene, već
slobode. Richard Stallman, začetnik pokreta slobodnog softvera, često
naglašava tu misao. Iskustvo od subotnjeg Dana slobodnog softvera kaže
da jezična barijera nije jedina te da ipak treba na samom početku
zainteresiranom sugovorniku naglasiti upravo to --- slobodan softver
nije pitanje cijene, već slobode. Kroz razgovor sa sugrađanima
primijetio sam da je poimanje slobodnog softvera istovjetno poimanju
besplatnog softvera. I dok se slobodan softver često može nabaviti
besplatno, to nije uvijek slučaj i novčana cijena je tek marginalno
pitanje. Slobodan softver je prvenstveno pitanje slobode, kontrole nad
vlastitim računalom i odnosa moći u današnjem digitalnom društvu.


Zahvaljujući obilježavanju ovog dana više Križevčana ima informaciju,
barem u obimu u kojem se to može prenijeti u minutu razgovora na
gradskom trgu. Dugoročno se nadam da će svi Križevčani biti
informirani o slobodnom softveru i da će znati što on znači za njihovu
svakodnevicu te da će zbog toga moći donijeti informiranu odluku o
softveru koji će upravljati njihovim računalima, kakva god ta odluka
bila.


Drago mi je da postoji Mozilla Hrvatska i da smo zajedno organizirali
ovo zbivanje. To ujedno znači da Mozilliani sigurno imaju još jedan
način na koji mogu gledati na Mozillin softver kojeg do ovog zbivanja
možda nisu bili svjesni, a to je misao da Mozillin softver korisnicima
pruža slobodu u njihovoj komunikaciji putem Interneta.


[![](img_20110917_113106.jpg){ width=100% }](img_20110917_113106.jpg)

Danas je na portalu križevci.info osvanula
[vijest](http://www.krizevci.info/vijesti/77-drustvo/9188-krievci-na-dan-nacionalni-centar-slobodnog-softvera)
koja u naslovu ističe kako su Križevci na jedan dan bili "nacionalni
centar slobodnog softvera". Pretpostavljam da je odabran taj naslov
zbog činjenice da su Dan slobodnog softvera organizirala dva entiteta
koja u nazivu imaju "Hrvatska" čime se sugerira njihov teritorij
djelovanja, a odlučili su se "pojaviti" upravo u Križevcima čime su ga
na neki način smjestili u svoj centar, u svoj fokus. Dapače,
organizirali su taj dan samo u Križevcima. Bez namjere da svome gradu
oduzmem tu titulu, valja napomenuti kako se poklopilo da su "čelnici"
organizatora --- Nikola Matosović ispred Mozille Hrvatska i moja
malenkost ispred Slobodnog softvera Hrvatska --- upravo Križevčani pa
ne čudi da se taj dan u toj "kombinaciji" obilježio upravo u
Križevcima. Mene osobno bi veselilo kada bismo kao SSH taj ponos koji
se iščitava iz naslova vijesti uspjeli preusmjeriti tako da se ubuduće
odnosi na "H" u "SSH" i da nije ograničen na samo jedan dan u
godini. To postignuće vjerojatno bi značilo da je SSH postao suvišan i
da u SSH-u možemo reći: "Vrijeme je da se prebacimo u [Free Software
Foundation Europe](http://fsfe.org/)."
