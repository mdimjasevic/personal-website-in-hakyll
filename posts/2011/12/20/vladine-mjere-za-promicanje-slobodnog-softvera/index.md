---
author: Marko Dimjašević
comments: false
date: 2011-12-20 17:28:31+00:00
excerpt: "Uskoro će na vlast u Hrvatskoj doći nova vlada. Vjerujem da je\
  \ to prilika za analizu loših poteza prethodnih vlada. Dio koji je meni posebno\
  \ zanimljiv, a posredno --- siguran sam --- i ostalima je pitanje slobode građana. Živimo\
  \ u digitalnom društvu koje je prožeto izračunima i računalnim operecijama koje\
  \ za nas obavljaju razna računala, npr. osobna računala i tzv. pametni telefoni.\
  \ Naša sloboda ovisi o tome tko ima kontrolu nad tim operacijama, odnosno nad našim\
  \ računalima."
layout: post
link: https://dimjasevic.net/marko/2011/12/20/vladine-mjere-za-promicanje-slobodnog-softvera/
slug: vladine-mjere-za-promicanje-slobodnog-softvera
title: "Vladine mjere za promicanje slobodnog softvera"
wordpress_id: 393
tags:
- fs
- Kukuriku
- neovisnost
- slobodan softver
---

Uskoro će na vlast u Hrvatskoj doći nova vlada. Vjerujem da je to
prilika za analizu loših poteza prethodnih vlada. Dio koji je meni
posebno zanimljiv, a posredno --- siguran sam --- i ostalima je
pitanje slobode građana. Živimo u digitalnom društvu koje je prožeto
izračunima i računalnim operecijama koje za nas obavljaju razna
računala, npr. osobna računala i tzv. pametni telefoni. Naša sloboda
ovisi o tome tko ima kontrolu nad tim operacijama, odnosno nad našim
računalima.

Richard Stallman daje
[osvrt](https://www.gnu.org/philosophy/government-free-software.html)
što vlada može i mora učiniti kako bi osigurala slobodu građana kada
se radi o softveru, odnosno računarstvu. Koliko je novoj vladi u
Hrvatskoj stalo do slobode građana? Je li svjesna da neovisnost
države, kada se u obzir uzme trenutno stanje po pitanju softvera,
stupanjem na vlast neće biti u njenim rukama?
