---
author: Marko Dimjašević
comments: false
date: 2011-01-19 21:40:01+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/01/19/teknologija-narodu/
slug: teknologija-narodu
title: "Teknologija narodu"
wordpress_id: 270
tags:
- free software
- slobodan softver
- teknologija narodu
---

_U Križevcima djeluje udruga [P.O.I.N.T.](http://www.udruga-point.hr/)
koja ima gradski portal [križevci.info](http://www.krizevci.info) gdje
se nalazi, među ostalim, kolumna [Teknologija
narodu](http://www.krizevci.info/kolumne/111-teknologija-narodu). Na
Hrvojev prijedlog (predsjednik udruge) naći će se i koji moj tekst o
slobodnom softveru u toj kolumni. Prvi bi tekst trebao osvanuti za
koji dan pod naslovom _Uvod u svijet slobodnog softvera_ i sadržaja
kao u nastavku._

Informacijska tehnologija postala je sastavni dio
svakodnevice. Okružuje nas u obliku osobnih računala, mobitela,
Interneta i brojnih pripadnih tehnologija kao što su web, e-mail,
torrenti i dr., kao i u obliku raznolikog softvera na mobilnim
uređajima i osobnim računalima. Nije tajna da šire hrvatsko pučanstvo
kod kuće na osobnim računalima pretežito koristi softver bez legalno
stečene licence. To je u nas normala --- koristi se sve od
operacijskog sustava do specijaliziranog softvera za obradu slika,
teksta, prezentacija, borbu protiv računalnih virusa i dr. I to ne
čudi --- tko si može svakih nekoliko godina priuštiti trošak od
nekoliko tisuća kuna za sav taj softver? Nerijetko se čovjek nađe u
situaciji da treba pomoći svom susjedu kad nešto zašteka na računalu
--- možda treba instalirati najnovije izdanje Prozora, možda treba
instalirati softver za obradu teksta. Susjed često nema svoju kopiju,
odnosno licencu za taj softver pa vas zamoli da mu "spržite" CD s
dotičnim softverom. Međutim, zakon vam brani umnažanje takvog
softvera. Vi ste tada u dilemi --- da li poštivati zakon i time odbiti
pomoći susjedu ili prekršiti zakon i pomoći svom susjedu? Koji god
odabir napravite, određena psihičko-društvena šteta je
neizbježna. Takvu dilemu uzrokuje vlasnički softver, a to je onaj koji
ne poštuje vaša prava da izvršavate program za koju god svrhu želite,
da ga proučavate i mijenjate, da ga nepromijenjenog dijelite sa svojim
susjedom i da izmijenjene verzije podijelite sa svima tako da šire
pučanstvo ima koristi od vaših izmjena. No ta dilema --- poštivati
zakon ili pomoći susjedu --- ne potraje dugo i učinite ono što
smatrate manjim zlom --- prekršit ćete zakon i pomoći svom
susjedu. Društvo nas je odgojilo tako da smatramo da je ta dilema
normalna. Činjenica je da je to lažna dilema.

Postoje programeri koji poštuju vaša spomenuta prava oko softvera. Oni
razvijaju slobodan softver i takav softver poštuje slobodu korisnika
računala. Odabrati slobodan ili vlasnički softver je pitanje kontrole
nad vlastitim računalom --- treba li računalo primati naredbe od vas
(što je slučaj kod slobodnog softvera) ili od nekog trećeg,
npr. prekooceanske korporacije (što je slučaj kod vlasničkog
softvera)? To je pitanje suverenosti. Kod slobodnog softvera nema
spomenute dileme između poštivanja zakona i pomaganja susjedu jer ste
slobodni pomoći svojem susjedu presnimavanjem slobodnog softvera i pri
tome poštivati zakon. Razlika je u licenci --- kod vlasničkog softvera
licenca je napisana tako da vam oduzme prava i zabranjuje vam pomoći
susjedu, a kod slobodnog softvera licenca je napisana tako da poštuje
vaša prava i potiče vas na dijeljenje sa susjedom. U tom smislu može
se reći da je tema slobodnog softvera pun pogodak za kolumnu naziva
Teknologija narodu.

Sada se već pitate zašto do sada niste čuli za slobodan softver. Moj
odgovor je --- ne znam! Možda koristite web preglednik imena Mozilla
Firefox. To je slobodan softver. Slobodan softver postoji od kada
postoji i računalo. Postoji desetak autoru poznatih operacijskih
sustava slobodnog softvera kao i pregršt slobodnog softvera za
svakodnevnu uporabu kod kuće, za poslovna okruženja, za djecu
predškolskog i školskog uzrasta kao i za studente i neprofitne
organizacije. Tko u Križevcima svakodnevno koristi samo slobodan
softver? Koliko mi je poznato, svega šačica pojedinaca.

Slobodan softver, osim svoje očigledne tehnološke dimenzije, ima i
infrastrukturnu, filozofsku, političku, ekonomsku, pravnu, kulturnu,
aktivističku, obrazovnu i društvenu dimenziju. Ako vas zanima više o
slobodnom softveru, mogu preporučiti nekoliko poveznica:
	
* [Free Software Foundation](https://fsf.org/) (FSF), prva i
  najpoznatija organizacija u svijetu koja promiče slobodan softver,
* [Free Software Foundation Europe](https://fsfe.org) ([na
  hrvatskom](https://fsfe.org/index.hr.html)), sestrinska organizacija
  FSF-a locirana u Europi,
* [Free Software Daily](http://www.fsdaily.com/), web-stranica koja
  prenosi vijesti o slobodnom softveru.


Richard Stallman, osnivač Free Software Foundationa, napisao je na ovu
temu brojne eseje. Dio njih nalazi se u zbirci [Free Software, Free
Society](http://shop.fsf.org/product/free-software-free-society-2/).

[Identi.ca](https://identi.ca/), servis za mikro-blogove, okuplja,
među ostalim, brojne ljubitelje i aktiviste za slobodan
softver. Prateći njihove mikro-blogove moguće je naučiti mnogo toga o
slobodnom softveru. Identi.ca nudi i tematske grupe u koje
zainteresirani šalju poučne i zanimljive mikro-blogove kroz koje se
često razvije diskusija. Grupe o slobodnom softveru koje prati [moja
malenkost](https://identi.ca/mdim) i koje bi mogle biti zanimljive
čitateljstvu su (bez nekog posebnog redoslijeda navođenja):
	
* [Free Software](http://identi.ca/group/fs),
* [Electronic Frontier Foundation](http://identi.ca/group/eff),
* [Free Software & Culture Group](http://identi.ca/group/free),
* [GNU's Not Unix](http://identi.ca/group/gnu),
* [Free Software Foundation](http://identi.ca/group/fsf),
* [Free Software Foundation Europe](http://identi.ca/group/fsfe) i
* [autonomo.us](http://identi.ca/group/autonomous).
