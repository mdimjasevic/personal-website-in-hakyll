---
author: Marko Dimjašević
comments: false
date: 2011-01-30 13:47:21+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2011/01/30/kultursok-u-2011/
slug: kultursok-u-2011
title: "Kulturšok u 2011."
wordpress_id: 278
---

Ovo je treća godina za redom da ću izvoditi
[kulturšok](http://www.c-shock.org/2011/) u svojim Križevcima. Na
repertoaru imam dvije predstave, baš kao i [prošle
godine](/2010/03/13/kultursok-u-krizevcima/). Prva je 22. ožujka
u obliku tribine naslova "Autorsko krivo --- copyleft kao mehanizam
kulturnog zaokreta". Kratki opis koji će se pojaviti u knjižici
Kulturšoka 2011 glasi:


> Značajni dio kulture, onoga što određuje svakog čovjeka, u proteklom
> je stoljeću gotovo neopozivo preselio iz domene javnog dobra u
> domenu privatnog vlasništva. Društvo je kroz zakonske odredbe to
> odabralo kao temeljnu vrijednost. Je li moguće iskoristiti iste te
> zakonske odredbe za postizanje obrnutog efekta, odnosno za ponovno
> stvaranje zajedničke dijeljene kulture?


Tjedan dana kasnije, 30. ožujka, obilježit ću [Dan slobodnih
dokumenata](http://documentfreedom.org). Sloboda u ovom kontekstu
označava slobodu čitanja i pisanja u digitalnom svijetu i komplement
je slobodnom softveru. Knjižica će za ovaj događaj zabilježiti:

> Naša komunikacija sve se više seli u digitalni svijet. U digitalnom
> društvu nam [otvoreni
> standardi](http://fsfe.org/projects/os/os.hr.html) i otvoreni
> formati dokumenata pružaju slobodu čitanja i pisanja. Dan slobodnih
> dokumenata je svjetski dan proslave otvorenih standarda i otvorenih
> formata dokumenata i isticanja njihove važnosti.


Bit će mi zadovoljstvo ako se želiš uključiti na bilo koji način u te
dvije predstave! Možeš me naći na [Identi.ci](http://identi.ca/mdim)
ili me kontaktirati [e-mailom](/about/).
