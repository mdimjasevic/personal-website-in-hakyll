---
author: Marko Dimjašević
comments: false
date: 2016-04-15 05:39:17+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/04/15/im-into-scala/
slug: im-into-scala
title: "I'm Into Scala"
wordpress_id: 35270
categories:
- free software
tags:
- functional programming
- JVM languages
- NASA internship
- programming languages
- Scala
---

![Scala](scala-logo.png)

As of last week I've finally gave the [Scala][scala] programming
language a try, as in I've actually [started coding in Scala][dpkg]. I
don't want this to be a yet-another way of writing plain old
imperative, hence next to incomprehensible and unmaintainable
code. Therefore, lately I've been putting a lot of energy into
thinking how to [program functionally][fp]. You can say I'm finally
delivering on [my earlier promise][dys] to [code in Scala][jvm]. In
other words, seeing lots of bad/imperative code during [an
internship][nasa] can do you good, i.e. it can make you want to
program in a better way.


<!-- more -->


If you wonder why Scala, and not some other language, here is a quick
summary: 1) I've had exposure to other JVM languages (Java and
AspectJ) and I've spent quite some time learning the runtime, 2) it is
functional, 3) it is object-oriented, 4) it works great across
multiple domains such as scripting, web applications, and "regular"
applications, 5) its type system is static, inferred, structural, and
strong, 6) it is free software, and 7) it is truly cross-platform,
i.e. it works on pretty much any operating system. For a longer
comparison of programming languages and somehow artificially evaluated
criteria one can use in choosing a language, [read a blog
post][why].


As Scala's designer Martin Odersky has put it in his Coursera course
on Scala and functional programming, it would be great if, when
programming, we can break away from the low-level modus operandi of
computers where we execute instructions one after another and change
the state. Functional programming in Scala is definitely the way to go
to write better code.


[scala]: https://en.wikipedia.org/wiki/Scala_%28programming_language%29
[dpkg]: https://gitlab.com/mdimjasevic/dpkg-process
[fp]: https://en.wikipedia.org/wiki/Functional_programming
[dys]: /2014/12/20/working-on-imperative-programming-code-aint-fun/
[jvm]: /2014/10/28/going-with-jvm-languages/
[nasa]: /2014/08/31/internship-in-nasa/
[why]: https://web.archive.org/web/20160305022256/http://blog.srinivasan.biz/software/if-you-have-to-learn-just-one-programming-language
