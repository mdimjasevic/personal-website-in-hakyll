---
author: Marko Dimjašević
comments: false
date: 2016-04-12 00:58:54+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/04/12/conference-decision-chart/
slug: conference-decision-chart
title: "Conference Decision Chart"
wordpress_id: 35268
categories:
- ephemera
- science
post_format:
- Status
tags:
- PhD comics
---

The PhD Comics has a nice chart helping you figure out if you should
[attend a conference](http://www.phdcomics.com/comics.php?f=1871).
