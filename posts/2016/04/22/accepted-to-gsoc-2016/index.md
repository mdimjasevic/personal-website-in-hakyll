---
author: Marko Dimjašević
comments: false
date: 2016-04-22 01:42:09+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/04/22/accepted-to-gsoc-2016/
slug: accepted-to-gsoc-2016
title: "Accepted to GSoC 2016!"
wordpress_id: 35285
categories:
- ephemera
- free software
post_format:
- Status
tags:
- Clover
- Debian
- Debile
- Google Summer of Code
- KLEE
---

I got accepted to the Google Summer of Code 2016 program! The project
is on [integrating KLEE into
Debile](https://wiki.debian.org/SummerOfCode2016/StudentApplications/MarkoDimjasevic). Woot!
