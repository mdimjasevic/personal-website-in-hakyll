---
author: Marko Dimjašević
comments: false
date: 2016-04-16 06:19:39+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/04/16/scripting-in-scala/
slug: scripting-in-scala
title: "Scripting in Scala"
wordpress_id: 35278
categories:
- ephemera
post_format:
- Status
tags:
- Scala
- scripting
---

If you are interested in how to use Scala as a scripting language and
how that works behind the scenes, there's a [nice blog
post](https://web.archive.org/web/20150924132001/http://halyph.com/blog/2015/02/02/scripting-in-scala/)
about it.
