---
author: Marko Dimjašević
comments: false
date: 2016-05-25 17:08:26+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/05/25/on-pc-of-the-jpf-2016-workshop/
slug: on-pc-of-the-jpf-2016-workshop
title: "On the PC of the JPF Workshop 2016"
wordpress_id: 35301
categories:
- ephemera
- science
post_format:
- Status
tags:
- Android
- CFP
- Java
- Java PathFinder
- JPF
- software analysis
---

I'll be serving on the program committee of the [Java Pathfinder
Workshop 2016](http://jpf.byu.edu/jpf-workshop-2016). If you are
working on analysis for Java and Android, consider submitting your
work! See the workshop's [call for
papers](http://jpf.byu.edu/jpf-workshop-2016/cfp.html).
