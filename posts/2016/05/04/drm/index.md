---
author: Marko Dimjašević
comments: false
date: 2016-05-04 04:05:17+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/05/04/drm/
slug: drm
title: "DRM Video on e-books"
wordpress_id: 35293
categories:
- ephemera
- free software
post_format:
- Status
tags:
- April
- DRM
- e-books
---

If you're not familiar with DRM (Digital Restrictions Management),
watch this informative [3-minute video by
April](https://www.april.org/en/drm-locked-e-book-always-downgrade-printed-book-video)
with an e-books example. DRM is also incompatible with free software
as you do not have control over things that DRM restricts.
