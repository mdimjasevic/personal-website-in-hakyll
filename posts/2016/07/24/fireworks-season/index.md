---
author: Marko Dimjašević
comments: false
date: 2016-07-24 03:39:06+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/07/24/fireworks-season/
slug: fireworks-season
title: "Fireworks Season"
wordpress_id: 35454
categories:
- ephemera
post_format:
- Status
tags:
- fireworks
- Salt Lake City
- Utah
---

The Fireworks Season in Salt Lake City starts one week before the
Independence Day (~June 27) and runs all the way through the Pioneer
Day (July 25). Fireworks every evening. I bet fireworks shop owners
love the country the most these days.
