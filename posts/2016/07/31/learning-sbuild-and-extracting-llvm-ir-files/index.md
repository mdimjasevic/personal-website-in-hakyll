---
author: Marko Dimjašević
comments: false
date: 2016-07-31 05:40:40+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/07/31/learning-sbuild-and-extracting-llvm-ir-files/
slug: learning-sbuild-and-extracting-llvm-ir-files
title: "Learning sbuild and Extracting LLVM IR Files"
wordpress_id: 35466
categories:
- free software
tags:
- Debian
- Debile
- Google Summer of Code
- KLEE
- LLVM IR
- sbuild
- schroot
---

Google Summer of Code 2016 is closely coming to an end --- we're
stepping into August, and coding for GSoC ends on August 23. With that
in mind, I've been working on bringing my GSoC project to an end,
though there are still things to do.


<!-- more -->


A decent portion of this week went into fiddling with sbuild. To my
surprise, I figured out that my last week's patch for sbuild that adds
a command line option for reusing an existing schroot session didn't
work in a clean environment, so I had to rework that patch. Now it's
working and I tested it on a system with no prior installation of
sbuild and schroot. It was nice to see a modified sbuild in action,
i.e. being able to use sbuild with the new command line option. The
updated patch [got merged in Debile
upstream](https://github.com/opencollab/debile/pull/47).


Learning how to use sbuild by hand can take some time. What I learned
is that there is a tool `mk-sbuild`, which does heavy lifting for you
of setting up an environment. It was written for Ubuntu and it found
its way into Debian in the `ubuntu-dev-tools` binary package.


Finally, I brought the KLEE plugin for Debile one step closer to being
done. I wrote a function that extracts LLVM bitcode/IR from a Debian
source package. However, I haven't tested it yet as I did it under an
assumption that once sbuild is done building a source package, that it
leaves resulting binary packages in `/var/lib/sbuild` in the chroot,
and no one in Debian IRC channels could confirm this for me. The
directory path sounds more like it's on the host side, and not in the
chroot. Therefore, I'll have to test this properly. With the new
command line option for reusing an existing schroot session, this does
matter to me as once sbuild is done with building a source package, I
can go and play with the same chroot, e.g. extract LLVM bitcode that
will eventually be fed to KLEE for analysis.


Everything is on the [klee
branch](https://github.com/mdimjasevic/debile/tree/8cf54f84efcbc0e6c0a3c2b2341c41857e1b8514)
of my Debile fork.
