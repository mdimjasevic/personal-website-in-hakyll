---
author: Marko Dimjašević
comments: false
date: 2016-07-18 18:59:53+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/07/18/academic-rejection-letter/
slug: academic-rejection-letter
title: "Academic Rejection Letter"
wordpress_id: 35451
categories:
- ephemera
- science
post_format:
- Status
tags:
- PhD comics
---

This is what it looks like when your academic paper gets rejected for
publication: [PhD Comics](http://www.phdcomics.com/comics.php?f=1888).
