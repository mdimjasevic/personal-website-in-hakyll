---
author: Marko Dimjašević
comments: false
date: 2016-07-18 06:57:15+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/07/18/setting-up-sbuild-environment-for-klee/
slug: setting-up-sbuild-environment-for-klee
title: "Setting up sbuild Environment for KLEE"
wordpress_id: 35434
categories:
- free software
tags:
- Debile
- Google Summer of Code
- KLEE
- Python
- sbuild
- schroot
---

[Debile](http://debile.debian.net/) uses sbuild for building
packages. As mentioned before, my setup for analyzing Debian source
packages with KLEE assumed pbuilder. Therefore, i've been moving to
sbuild for symbolic execution via KLEE on Debian packages.


<!-- more -->


One of obstacles I had this week had to do with using schroot in
Python, namely in Debile, which is written in Python. Paul
Tagliamonte, a Debian developer, who contributed to Debile, also
created a [Python library
schroot](https://pypi.python.org/pypi/schroot) (which is packaged in
[Debian](https://packages.debian.org/stretch/python-schroot)), for
working with [schroot](https://wiki.debian.org/Schroot). I looked at
implementations of Debile runners of other analysis tools already
supported in Debile that used schroot. It seemed pretty
straightforward and I thought it would be very easy to do the same for
KLEE. However, it wasn't.


I wanted to set up an schroot session by using the usual command line
piping, passed as a command to be executed within the session. I also
wanted to change directory followed by a command. The implementation
of executing commands in a Python schroot (the run function) is such
that it boils down to using Python subprocess's Popen with the shell
argument set to false, which means no piping and no and-ing commands
(the `&&` operator). It took me some time to track this down.


To make my life easier, I created a helper function to run a sequence
of commands and break out of the sequence as soon as a command from
the sequence return a non-zero value:

```python

def chroot_cmd_sequence(chroot, cmds):
    out_ = ""
    err_ = ""
    for cmd in cmds:
        if cmd[0] == 'root':
            out, err, ret = chroot.run(cmd[1], user='root')
        else:
            out, err, ret = chroot.run(cmd[1])
        out_ += out
        err_ += err
        if ret:
            return out_, err_, ret

    return out_, err_, 0
```

The function turned out to be very handy.

A next obstacle that I thought wouldn't be there was with using for
expressions to build lists. A problem I ran into is building a list of
strings representing a command to be run and its arguments using for
expressions. I wanted to have something like:

```python
	x for x in ['a', 'b', 'c']
```

but that didn't work when part of another list. So I reverted to plain
old for loops and modifying a big list in-place, which is not nice.


I got all of that working so what I have now is: a proper installation
of LLVM, WLLVM, and KLEE in an schroot session. The next step will be
to build a provided Debian source package and run KLEE on it.


The current state of the implementation is available in my fork of
Debile in the [_klee_
branch](https://github.com/mdimjasevic/debile/tree/13a2a04a255f25e6c7346fadcada5632c0c40ec9).
