---
author: Marko Dimjašević
comments: false
date: 2016-07-25 06:16:07+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/07/25/modifying-sbuild/
slug: modifying-sbuild
title: "Modifying sbuild"
wordpress_id: 35456
categories:
- free software
tags:
- debhelper
- Debian
- Debile
- GSoC
- KLEE
- sbuild
- schroot
---

This GSoC week of working on a Debian project was all about sbuild. I
made progress with setting up an sbuild session for KLEE and I also
reworked a patch for sbuild to support a command-line option not
present in the upstream version of sbuild.


<!-- more -->


With the previous week I managed to install all KLEE dependencies in
an sbuild session within Debile. However, having tools and libraries
in place is not enough; I also need to tweak quite a few settings in
the environment to make KLEE work on a Debian package.


One thing that I needed to tweak are environment variables. For
example, [WLLVM](https://github.com/travitch/whole-program-llvm)
expects several environment variables set. A problem is in the Python
schroot library by default not remembering the environment from
previously executed commands. Luckily, the library has a way of using
the host's environment in the container so I configured the variables
on the host side and simply shared them with the sbuild container.


Another configuration that needed to be done is to
`dh_auto_configure`, a tool that is part of the debhelper command
sequencer, which is used by virtually every Debian source package. In
earlier explorations I realized that some packages fail to configure
when the `WLLVM_CONFIGURE_ONLY` variable is not set, i.e. when WLLVM
compiler wrappers are used instead of GCC during package
configuration. To work around this, I created a wrapper
`/usr/local/bin/dh_auto_configure` for the original
`dh_auto_configure` as such:

```bash
    #!/bin/bash
    dpkg-buildflags --export
    WLLVM_CONFIGURE_ONLY=1 /usr/bin/dh_auto_configure "$@"
```

With the `PATH` environment variable having `/usr/local/bin` appearing
before `/usr/bin`, the wrapper calls the original `dh_auto_configure`
with the `WLLVM_CONFIGURE_ONLY` variable set and the configuration
proceeds without any troubles. This was a tricky hack as I needed to
pass `WLLVM_CONFIGURE_ONLY` only to `dh_auto_configure`, and not to
other package build commands.


With all above, this should finish setting up the environment needed
to run KLEE on a Debian package. However, there will be custom scripts
to call after a package is built in order to call KLEE on appropriate
LLVM bitcode files.


I also reorganized the code I have in Debile so far as it was getting
hard to read.


Finally, I re-based [Léo Cavaillé's earlier
patch](http://leo.cavaille.net/public/gsoc/sbuild-patches/0001-Added-support-for-schroot-sessions.patch)
to a current version of sbuild, i.e. sbuild 0.69.0. His original patch
is for 0.64.1. The patch adds a new command-line option to sbuild so
it can reuse an existing schroot session. This is important for me as
I need to use an schroot session I set up for KLEE with all its
customizations, including environment variables. As the older sbuild
version is not available in Debian Sid anymore, I had to update Léo's
patch so it works against the latest release of sbuild. At first I was
reluctant to do this because sbuild is written in Perl, and Perl looks
like a very scary programming language to me. Hey, I don't even know
how to write a hello world program in Perl! Nevertheless, in the
#debile IRC channel over at OFTC, other Debile developers suggested it
would be better to update the patch than to try to use Debian's
Snapshot Archive to install sbuild 0.64.1, which is what I was trying
to do initially. Clément Schreiner, my co-mentor on this project,
asked me to write the patch in accordance with the [DEP3
format](http://dep.debian.net/deps/dep3/); hopefully, we can get this
patch finally merged upstream or at least in the sbuild Debian source
package.


On a related note, I fixed a Dockerfile for a Debile slave that was
installing the upstream version of sbuild that doesn't have the
command-line option. Now it pulls a couple of binary packages with the
patch applied and currently I host both of the packages on my
server. That and the patch are waiting in a [pull
request](https://github.com/opencollab/debile/pull/47) to the upstream
Debile repository.


New KLEE-specific changes are available in the [klee
branch](https://github.com/mdimjasevic/debile/tree/3b309a5638c0618281023d9eb97ef639b9638aa1)
of my Debile fork.
