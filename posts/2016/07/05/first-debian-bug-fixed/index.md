---
author: Marko Dimjašević
comments: false
date: 2016-07-05 16:24:55+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/07/05/first-debian-bug-fixed/
slug: first-debian-bug-fixed
title: "First Debian Bug Fixed!"
wordpress_id: 35422
categories:
- ephemera
- free software
post_format:
- Status
tags:
- Debian
- dynamic symbolic execution
- Google Summer of Code
- hostname
- KLEE
---

My first bug in a Debian package that I found with KLEE got confirmed
and fixed, bug report
[#829010](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=829010#15)!
