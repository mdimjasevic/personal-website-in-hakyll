---
author: Marko Dimjašević
comments: false
date: 2016-08-17 00:46:39+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/08/17/phd-candidate/
slug: phd-candidate
title: "PhD Candidate"
wordpress_id: 35497
categories:
- ephemera
- University of Utah
post_format:
- Status
---

Today I passed my oral qualifier. In other words, I'm a PhD candidate,
i.e. I'm getting close to graduation!
