---
author: Marko Dimjašević
comments: false
date: 2016-08-02 01:56:59+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/08/02/davdroid/
slug: davdroid
title: "DAVdroid"
wordpress_id: 35469
categories:
- ephemera
- free software
post_format:
- Status
tags:
- DAV
- DAVdroid
- ownCloud
---

If you use DAVdroid on your phone (and have your address books and
calendars in ownCloud), don't rush with installing DAVdroid 1.2. I
just had to uninstall it and install version 1.1.1.2 to have it usable
on my phone again. The old lesson learned: don't install .0 releases.
