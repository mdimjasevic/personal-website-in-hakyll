---
author: Marko Dimjašević
comments: false
date: 2016-08-14 21:58:47+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/08/14/summary-of-my-google-summer-of-code-2016-project/
slug: summary-of-my-google-summer-of-code-2016-project
title: "Summary of My Google Summer of Code 2016 Project"
wordpress_id: 35475
tags:
- Debian
- Debile
- dynamic symbolic execution
- Google Summer of Code
- hostname
- KLEE
- sbuild
- schroot
- WLLVM
---

We are in the last days of the Google Summer of Code 2016. This post
summarizes what I've done with regard to [my project on supporting
KLEE in Debile][dwiki] and where to find code and other I wrote and
did during the summer.

[![Clover](/2016/05/27/putting-klee-to-test/clover-swirl.png)](https://github.com/soarlab/clover)


<!-- more -->


There are several code bases I contributed to. Some of the
contributions have been merged upstream, and some not.


## Debile

Debile is a Debian package analysis infrastructure. This is the
umbrella project I work on during the Google Summer of Code.

There are a few commits I made that got merged upstream:

> [https://github.com/opencollab/debile/commits/master?author=mdimjasevic](https://github.com/opencollab/debile/commits/master?author=mdimjasevic)

One fix I made is related to Debile failing to build. Then there were
a few documentation changes. The latest change is what I consider as a
contribution to the sbuild project, which I write more about in a
separate section.


The main contribution with Debile that I made is in writing a plugin
for KLEE, which is in a separate code base:

> [https://github.com/mdimjasevic/debile/commits/klee?author=mdimjasevic](https://github.com/mdimjasevic/debile/commits/klee?author=mdimjasevic)

This is not merged upstream yet as it still needs testing.


What remains to be done with it is to test the plugin properly and fix
any remaining bugs. I believe it will not work the way it is. I have
quite a few modifications to the analysis environment, including
environment variables that need to be passed to sbuild, but at the
moment sbuild doesn't inherit the variables from the host. I'm looking
for ways to work around this. Note I got KLEE analyzing Debian
packages with pbuilder, which is another Debian package build
tool. That is why I was able to [run KLEE on over 500 programs from
Debian][interp].


## sbuild

sbuild is a Debian package build tool. It uses a chroot environment to
build a source packge in isolation of the host system. By default, it
uses a clean schroot session to build a package. Several existing
Debile plugins, including the new one for KLEE, rely on setting up an
schroot session first and then reusing it when building and analyzing
a package. However, the upstream version of sbuild does not support
reusing an existing schroot session. Therefore, a few years ago Léo
Cavaillé [extended
sbuild](http://leo.cavaille.net/public/gsoc/sbuild-patches/0001-Added-support-for-schroot-sessions.patch)
to support a command line option for building a package in an existing
session. However, the extension was a patch that never got applied
upstream and stayed in Debile's code base only.


In the meantime, sbuild moved on to newer versions and Léo's patch
became outdated, i.e. misaligned with the current version of
sbuild. To use the feature Léo added, I had to re-base his patch
against the latest version of sbuild, which was 0.69.0 when I did
it. Ironically, there was a yet another release of sbuild in the
meantime, namely sbuild 0.70.0. If we are unlucky enough, another
patch re-basing will be needed.


The patch is in Debile's repository:

> [debile/contrib/debian/support-for-schroot-sessions.patch](https://github.com/opencollab/debile/blob/62c068ab011d95fa069df512cbdb6ac699a157c7/contrib/debian/support-for-schroot-sessions.patch)


Hopefully after GSoC we will get to submit the updated patch upstream
and have it merged.


## WLLVM

WLLVM or Whole program LLVM is a tool that facilitates compiling a
program or a library written in C/C++ to the LLVM intermediate
representation (IR). This has been an important tool in the tool chain
as KLEE works on a program's LLVM IR representation, and not on its
source or binary code.


There was an unfortunate situation where a fork of WLLVM diverged from
its upstream. I'd like to believe I managed to bring them back as
developers from both sides started communicating and working together.


My contributions to WLLVM have been merged upstream:

> [https://github.com/travitch/whole-program-llvm/commits/master?author=mdimjasevic](https://github.com/travitch/whole-program-llvm/commits/master?author=mdimjasevic)


## KLEE

[KLEE](http://klee.github.io/) is a dynamic symbolic execution (or
concolic execution) tool for C programs. My efforts to create a Debian
package for KLEE have led to this GSoC project. I've been in touch a
lot with its developers and I made a number of contributions in terms
of [issues reported][klee-issues] (including bugs) and code that got
merged upstream:

> [https://github.com/klee/klee/commits/master?author=mdimjasevic](https://github.com/klee/klee/commits/master?author=mdimjasevic)


I haven't pushed contributions specific to the GSoC project upstream
yet, but they are in my fork of KLEE:

> [https://github.com/mdimjasevic/klee/commits/firehose?author=mdimjasevic](https://github.com/mdimjasevic/klee/commits/firehose?author=mdimjasevic)


I implemented support for the Firehose XML output format in KLEE by
adding a command line option for it. What is missing, though, is
providing concrete input values that lead to an error in a program
analyzed. This is usually done with the `klee-replay` tool, but what
would need to be done is to re-factor that part of code so it can be
used elsewhere. Once I finish that, I will make a pull request
upstream.


## Bug found, confirmed, and fixed

Thanks to this work, I was able to find a bug in the
[hostname](https://packages.debian.org/jessie/hostname) package. With
KLEE I produced a test case that led hostname to read from
uninitialized memory. The bug was confirmed for hostname versions 3.15
and 3.17, and a bug fix for it resulted in the hostname 3.18
release. The bug report I made is in the [Debian Bug Tracking
System](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=829010).


## Blog posts written during the summer

For every week of the Google Summer of Code, I would write a blog post
reporting my progress. Sometimes I would also write short notes on
what was on my mind. Here is a complete list sorted chronologically
from oldest to newest:

* [Accepted to GSoC 2016!](/2016/04/22/accepted-to-gsoc-2016/)
* [Putting KLEE to Test](/2016/05/27/putting-klee-to-test/)
* [KLEE: It Ain’t Gonna Do Much Without Libraries](/2016/06/03/klee-it-aint-gonna-do-much-without-libraries/)
* [2016 and C++](/2016/06/07/2016-and-c/)
* [Firehosing KLEE](/2016/06/11/firehosing-klee/)
* [Writing Tests and LLVM-interpreting Hundreds of Programs](/2016/06/17/writing-tests-and-llvm-interpreting-hundreds-of-programs/)
* [Unit Testing Interleaved with Development](/2016/06/19/unit-testing-interleaved-with-development/)
* [C++ Taking Toll](/2016/06/25/c-taking-toll/)
* [Unit Testing, cgroups, and Confirming Bugs](/2016/06/25/unit-testing-cgroups-and-confirming-bugs/)
* [Almost There with Firehosing KLEE, First Debian Bug Reported](/2016/07/03/almost-there-with-firehosing-klee-first-debian-bug-reported/)
* [First Debian Bug Fixed!](/2016/07/05/first-debian-bug-fixed/)
* [Started integrating KLEE into Debile](/2016/07/10/started-integrating-klee-into-debile/)
* [Setting up sbuild Environment for KLEE](/2016/07/18/setting-up-sbuild-environment-for-klee/)
* [Modifying sbuild](/2016/07/25/modifying-sbuild/)
* [Learning sbuild and Extracting LLVM IR Files](/2016/07/31/learning-sbuild-and-extracting-llvm-ir-files/)
* [First Debile Plugin for KLEE Done!](/2016/08/08/first-debile-plugin-for-klee-done/)


## How is this related to my research project

This GSoC is highly related to my research project at the University
of Utah called [Clover](https://github.com/soarlab/clover). The
project aims to analyze real-world programs with dynamic symbolic
execution or to be more precise, with the KLEE tool in particular. We
want to have big experiments in the project and we decided to do it on
Debian-packaged programs. That is why I decided to contribute to the
Debile software analysis infrastructure. We still have to do research
on how to do certain things in symbolic execution, but thanks to the
GSoC we have made a lot of progress.


It is true that I don't have a working implementation out of the GSoC
project, but I've learned a lot, wrote code for 4 different
established software projects plus my research project, and identified
remaining problems to be solved. This was all very useful for my PhD
thesis proposal that I had on August 17, 2016[^date].


I plan to keep on working on Debile, to finish the KLEE plugin for it,
and to extend it with things I will discover in my research project. I
really hope this will have a non-marginal impact on software in Debian
that is written in C.


[dwiki]: https://wiki.debian.org/SummerOfCode2016/StudentApplications/MarkoDimjasevic
[interp]: /2016/06/17/writing-tests-and-llvm-interpreting-hundreds-of-programs/
[klee-issues]: https://github.com/klee/klee/issues?utf8=%E2%9C%93&q=is%3Aissue%20mdimjasevic
[^date]: I started writing this post on August 14, hence the date in
    the URL, but I finished writing and published it only after the
    thesis proposal.
