---
author: Marko Dimjašević
comments: false
date: 2016-02-22 06:17:00+00:00
excerpt: "\n\t\t\t\t\t\t"
layout: post
link: https://dimjasevic.net/marko/2016/02/22/org-mode-to-markdown/
slug: org-mode-to-markdown
title: "Org-mode to Markdown"
wordpress_id: 35241
categories:
- ephemera
- free software
post_format:
- Status
tags:
- Emacs
- maline
- Markdown
- Org-mode
---

While it didn't work before, now with Org-mode 8.2.10, markdown-mode
2.0, and markdown-toc 0.0.7, exporting a table of contents finally
generates working links for Github! For example, see the readme of my
[maline](https://github.com/soarlab/maline) tool. Emacs is a neat
editor!
