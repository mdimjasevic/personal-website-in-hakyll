---
author: Marko Dimjašević
comments: true
date: 2016-02-11 07:34:14+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/11/gnu-enscript/
slug: gnu-enscript
title: "GNU Enscript"
wordpress_id: 35172
categories:
- ephemera
- free software
post_format:
- Status
tags:
- gnu
- html
- syntax highlighting
---

The tool of the day is [GNU
Enscript](https://www.gnu.org/software/enscript/). I might use it in
my blog writing instead of a not-so-functional WordPress syntax
highlighting plugin.
