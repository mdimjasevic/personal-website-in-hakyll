---
author: Marko Dimjašević
comments: false
date: 2016-02-02 05:43:10+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/02/who-controls-the-code-in-self-driving-cars/
slug: who-controls-the-code-in-self-driving-cars
title: "Who Controls the Code in Self-driving Cars?"
wordpress_id: 35148
categories:
- ephemera
- free software
post_format:
- Status
tags:
- back door
- ethics
- self-driving cars
---

Cory Doctorow writes about self-driving cars and asks [who controls
the code][guard] in self-driving cars.

[guard]: http://www.theguardian.com/technology/2015/dec/23/the-problem-with-self-driving-cars-who-controls-the-code
