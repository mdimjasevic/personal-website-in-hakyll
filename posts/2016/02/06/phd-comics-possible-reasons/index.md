---
author: Marko Dimjašević
comments: false
date: 2016-02-06 19:58:55+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/06/phd-comics-possible-reasons/
slug: phd-comics-possible-reasons
title: "PhD comics: Possible Reasons"
wordpress_id: 35157
categories:
- ephemera
- science
post_format:
- Status
tags:
- academia
- e-mail
- PhD comics
---

[Possible reasons](http://www.phdcomics.com/comics.php?f=1851) why
someone hasn't answered your e-mail by PhD comics is how I've been
feeling lately! :-)
