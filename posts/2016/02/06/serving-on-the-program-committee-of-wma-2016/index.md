---
author: Marko Dimjašević
comments: true
date: 2016-02-06 23:52:45+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/06/serving-on-the-program-committee-of-wma-2016/
slug: serving-on-the-program-committee-of-wma-2016
title: "Serving on the Program Committee of WMA 2016"
wordpress_id: 35160
categories:
- science
tags:
- malware
- malware detection
- program committee
- WMA 2016
---

[![Virus, Courtesy of NIAID](virus.jpg "Virus, Courtesy of NIAID"){ width=100% }](https://www.flickr.com/photos/niaid/14440817981/)

I am happy to announce i will be serving on the program committee of
the first incarnation of the International Workshop on Malware
Analysis (WMA)! In other words, I will be reviewing papers submitted
to the workshop. WMA is a workshop focusing on malicious software and
its analysis and detection. For a more detailed list of topics of
interest, see the [workshop's website][ws]. The workshop will be
collocated with the [International Conference on Availability,
Reliability and Security][ares] (ARES), which will take place August
31 -- September 2, 2016 in Salzburg, Austria.


<!-- more -->


Me and the rest of the WMA PC would be happy to see your paper
submission on malware to [WMA
2016](http://www.ares-conference.eu/conference/workshopsares2016/wma-2016/)!
The submission deadline is April 4, 2016.


[ws]: https://web.archive.org/web/20160314192305/http://www.ares-conference.eu/conference/workshopsares2016/wma-2016/
[ares]: https://web.archive.org/web/20160207021507/http://www.ares-conference.eu/conference/
