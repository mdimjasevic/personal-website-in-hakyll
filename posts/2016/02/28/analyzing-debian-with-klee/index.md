---
author: Marko Dimjašević
comments: false
date: 2016-02-28 20:27:44+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/28/analyzing-debian-with-klee/
slug: analyzing-debian-with-klee
title: "Analyzing Debian with KLEE"
wordpress_id: 35244
categories:
- ephemera
- free software
- science
post_format:
- Status
tags:
- Clover
- Debian package
- dynamic symbolic execution
- KLEE
---

![Clover logo](clover-swirl-small.png "Clover")

Just started with the [Clover](https://github.com/soarlab/clover)
project!  The project is about analyzing Debian with
[KLEE](http://klee.github.io/), a symbolic execution engine for
C/C++. The plan is to analyze the whole Debian archive.
