---
author: Marko Dimjašević
comments: false
date: 2016-02-04 17:49:53+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/04/how-winning-leads-to-cheating/
slug: how-winning-leads-to-cheating
title: "How Winning Leads to Cheating"
wordpress_id: 35152
categories:
- ephemera
- science
post_format:
- Status
---

Scientific American has an article on [how competition affects
cheating][amer]. Wanting to beat others is not necessarily a good
thing.

[amer]: https://web.archive.org/web/20160204174204/http://www.scientificamerican.com/article/how-winning-leads-to-cheating/
