---
author: Marko Dimjašević
comments: false
date: 2016-02-19 03:23:00+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/19/when-a-draft-says-hello-to-a-phd-student/
slug: when-a-draft-says-hello-to-a-phd-student
title: "When a Draft Says Hello to a PhD Student"
wordpress_id: 35239
categories:
- ephemera
- science
post_format:
- Status
tags:
- Adele
- Hello
- PhD comics
- writing
---

PhD comics has a couple of great comics on writing papers, accompanied
with a reference to a trending soul song: [In your
dreams](http://www.phdcomics.com/comics.php?f=1856) and a follow-up
[Hello (Academic
Version)](http://www.phdcomics.com/comics.php?f=1857).
