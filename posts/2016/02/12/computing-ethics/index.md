---
author: Marko Dimjašević
comments: false
date: 2016-02-12 05:05:36+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/12/computing-ethics/
slug: computing-ethics
title: "Computing Ethics"
wordpress_id: 35174
categories:
- ephemera
- free software
- software engineering
post_format:
- Status
tags:
- ethics
---

A nice essay on [computing
ethics](http://randomwalker.info/publications/sw_engg_ethics.pdf): why
software engineering courses should include ethics coverage (DOI:
[10.1145/2566966](http://dx.doi.org/10.1145/2566966)). Free software
anyone?
