---
author: Marko Dimjašević
comments: false
date: 2016-02-09 17:34:07+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/09/security-and-privacy-double-talk/
slug: security-and-privacy-double-talk
title: "Security and Privacy Double-talk"
wordpress_id: 35167
categories:
- ephemera
- University of Utah
post_format:
- Status
tags:
- privacy
- security
---

That time when a security and privacy researcher advertises storing
your data in Google Drive (because you get 2 GB for free today).
