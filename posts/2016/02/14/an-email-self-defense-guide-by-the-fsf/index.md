---
author: Marko Dimjašević
comments: false
date: 2016-02-14 05:31:53+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/14/an-email-self-defense-guide-by-the-fsf/
slug: an-email-self-defense-guide-by-the-fsf
title: "An Email Self-Defense Guide by the FSF"
wordpress_id: 35176
categories:
- free software
tags:
- '1984'
- Debian
- dystopian society
- Edward Snowden
- encryption
- Facebook
- FSF
- GCHQ
- gnu
- GnuPG
- Google
- GPG
- Microsoft
- NSA
- OpenPGP
- Orwell
- PGP
- privacy
- security
---

[![GnuPG by the Free Software Foundation](gnupg.png "GnuPG by the Free Software Foundation"){ width=239px }](gnupg.png)


Recently [I wrote about encryption](/2016/01/24/lets-encrypt/)
and in particular about free SSL certificates. SSL certificates are
useful for serving web pages over the HTTPS protocol, secure and
private email transfer from your email server to your email client
program, secure and private usage of [your own cloud
service](https://owncloud.org/), and other. But there is more to
security and privacy online.


<!-- more -->


If you are one of those who have been turning a blind eye to numerous
security and privacy issues on the Internet with the rise of companies
moving our online communications towards centralized silos (the
Googles, the Facebooks, the Apples, and the Microsofts of the world),
ever since 2013 Edward Snowden has been making it harder and harder to
keep on doing that. In case you still don't know what I'm talking
about, it's the [global surveillance
programs.](https://en.wikipedia.org/wiki/Edward_Snowden#Revelations)
Good morning, we live in a dystopian society. It's Nineteen
Eighty-Four.


[![Surveillance, Courtesy of the Free Software Foundation](surveillance-enabled.png "Surveillance, Courtesy of the Free Software Foundation"){ width=90% }](surveillance-enabled.png)


This post is about protecting your email. However, one should note it
doesn't make much sense to protect your email in the way mentioned
below if you use proprietary software on your computing devices
(desktops, laptops, tablets, smartphones, or whatever). Proprietary
software such as Microsoft Windows and Apple OS X and iOS is not
controlled by you, but by respective companies, hence the software
works against you. Android devices are not much better --- almost all
of them also have proprietary software installed and in terms of
hardware a vast majority of the devices are defective by design
(i.e. suitable for surveillance). Adding email protection on top of
proprietary software is futile and pointless as the owners of the
proprietary software --- which is not you, you are a mere licensee
that agreed to a nonnegotiable end user license agreement --- can
easily override the protection. Therefore, first I suggest you wipe
away proprietary software and install [free
software](https://en.wikipedia.org/wiki/Free_software); installing a
free GNU/Linux distribution like [Debian](https://www.debian.org/)
would be a great start.


Email messages are sent in plain text by default and they traverse
multiple computers while being sent and received. An email provider
might have an SSL certificate used for email transfer, but that
protects the messages only on their way from the provider to their
users and vice versa; the rest still goes and is in plain text. For
example, having an HTTPS connection to your Gmail inbox doesn't do you
much good when in the end it's Google, the NSA, and their buddies at
the GCHQ reading your messages in plain text as if they were
postcards. Even though I don't use Gmail, [Google has most of my email
because it has all of yours][mako]. This is obviously
unacceptable. Everyone has a right to communicate freely and securely
in private; if that is endangered, the whole society suffers and falls
under a totalitarian regime.


The solution is to use an open standard that scrambles email messages
for those that have no business in reading your mail, and at the same
time makes them clear for intended recipients. The standard is called
[OpenPGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy#OpenPGP),
which is short for Pretty Good Privacy. It is interchangeably used
with terms such as PGP, GNU Privacy Guard, GnuPG, and GPG. For this to
work, both communicating parties have to use the standard. It all
boils down to having a cryptographic keypair, a pair of codes where
one code is public and the other is secret and known only to
you. However, you don't have to have a degree in cryptography to use
it.


I've been using GPG for several years now. Just recently my first
keypair expired and I created a [new one][key] (I set this one to
never expire). For example, I run the server hosting this web site and
when I need to communicate something administrative about the server
to my brother, him and me use GPG to encrypt our email
communication. That way no one can tell the content of our
communication but the two of us. You don't need to be a server admin
or Edward Snowden to need to communicate privately and securely. Note
that ["I've got nothing to hide"][hide] is a very dangerous political
statement as it legitimizes unauthorized power over us. Snowden also
said,

<blockquote>Arguing that you don't care about the right to privacy
because you have nothing to hide is no different than saying you don't
care about free speech because you have nothing to say.</blockquote>


It is a basic human right not being watched over by someone, so read
on to learn how to take control over your email communication.


[![GnuPG Robot, by the Free Software Foundation](gnupg-robot.png "GnuPG Robot, by the Free Software Foundation"){ width=80% }](gnupg-robot.png)


As mentioned before, for secure and private email communication you
need free software and free software only. Free software puts you in
charge, not someone else remotely controlling your computing
devices. The well known implementation of OpenPGP is the [GNU Privacy
Guard](https://gnupg.org/) (abbreviated as GnuPG or GPG), a tool by
the GNU Project. It may be daunting to set it up and start using it on
your own. Therefore, the Free Software Foundation (FSF), which is the
organization behind the GNU Project, has prepared [Email
Self-Defense](https://emailselfdefense.fsf.org/), a guide to fighting
surveillance with GnuPG encryption. The guide comes with an
[infographic](https://emailselfdefense.fsf.org/en/infographic.html)
(pieces of it are heavily used in this post) giving you a quick
introduction to the problem and how to go about it. What is nice about
the guide is that you don't need a friend that already uses OpenPGP in
order to set it up on your computer or device; in testing your setup
you can communicate via email with Edward, a computer program by the
FSF that knows how to use encryption.


I don't use the email client that is recommended in the
guide. Instead, I use [Evolution][evo], a personal information manager
for GNOME (that's a nice benefit of relying on open standards and free
software --- you can choose any implementation you like of a given
open standard). If you also use other than the IceDove email client,
note that in step 3.A of the guide I downloaded my public key from a
key server and attached it to an email to Edward the computer
program. Finally, I also signed Edward's public key with my private
key, thereby participating in the web of trust.


What I also did is to change the signature I put at the end of each
email. For example, when sending school and research related mail, I
have this signature:

    
    Kind regards,
    Marko Dimjašević <marko@cs.utah.edu> .   University of Utah
    https://dimjasevic.net/marko         . PGP key ID: 1503F0AA
    Learn email self-defense!  https://emailselfdefense.fsf.org


That way I inform others about the guide and my public PGP
key. Furthermore, all my email messages are automatically digitally
signed. I also encrypt messages with those whose public keys I have.


I would encourage you to set up GnuPG. If you do so, get [my public
key][key] and [let's get in touch](/contact/)!


[![Surveillance disabled, Courtesy of the Free Software Foundation](surveillance-disabled.png "Surveillance disabled, Courtesy of the Free Software Foundation"){ width=90% }](surveillance-disabled.png)

[mako]: https://mako.cc/copyrighteous/google-has-most-of-my-email-because-it-has-all-of-yours
[key]: https://pgp.mit.edu/pks/lookup?op=vindex&search=0x565EE9641503F0AA
[hide]: https://en.wikipedia.org/wiki/Nothing_to_hide_argument
[evo]: https://en.wikipedia.org/wiki/Evolution_%28software%29
