---
author: Marko Dimjašević
comments: false
date: 2016-02-14 01:52:30+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/14/welcome-to-the-pirate-bay-of-science/
slug: welcome-to-the-pirate-bay-of-science
title: "Welcome to the Pirate Bay of Science"
wordpress_id: 35181
categories:
- ephemera
- science
post_format:
- Status
tags:
- academic publishing
- copyright
- Elsevier
- paywall
- Sci-Hub
- The Pirate Bay
---

Welcome to the [Pirate Bay of science][pb]!

[pb]: http://www.sciencealert.com/this-woman-has-illegally-uploaded-millions-of-journal-articles-in-an-attempt-to-open-up-science
