---
author: Marko Dimjašević
comments: false
date: 2016-02-17 19:13:20+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/02/17/hand-signing-a-digital-document/
slug: hand-signing-a-digital-document
title: "Hand-signing a Digital Document"
wordpress_id: 35224
categories:
- ephemera
- free software
post_format:
- Status
tags:
- GIMP
- Xournal
---

If you need to hand-sign a document to be sent over e.g. email,
consider using
[GIMP](http://www.wikihow.com/Make-a-Transparent-Image-Using-Gimp) to
scan and process your signature once and then use
[Xournal](http://xournal.sourceforge.net/manual.html#tools) to insert
the signature into the document.
