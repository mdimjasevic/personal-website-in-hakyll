---
author: Marko Dimjašević
comments: false
date: 2016-09-28 23:43:01+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/09/28/you-dont-have-to-be-stupid-to-work-here-but-it-helps/
slug: you-dont-have-to-be-stupid-to-work-here-but-it-helps
title: "You don’t have to be stupid to work here, but it helps"
wordpress_id: 35508
categories:
- ephemera
post_format:
- Status
tags:
- aeon
- economics
- mindlessness
- organisations
- social psychology
- work
---

An essay by André Spicer on research showing how being stupid is key
in doing well in a company or an organization:
[https://aeon.co/essays/you-don-t-have-to-be-stupid-to-work-here-but-it-helps][aeon].

[aeon]: https://aeon.co/essays/you-don-t-have-to-be-stupid-to-work-here-but-it-helps
