---
author: Marko Dimjašević
comments: false
date: 2016-10-05 06:20:59+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/10/05/america-is-obsessed-with-happiness-and-its-making-us-miserable/
slug: america-is-obsessed-with-happiness-and-its-making-us-miserable
title: "America is obsessed with happiness — and it’s making us miserable"
wordpress_id: 35512
categories:
- ephemera
post_format:
- Status
tags:
- American Dream
- British humour
- happiness
- USA
---

A cynical Brit writes about the [Great American Search for
Happiness](http://www.vox.com/first-person/2016/10/4/13093380/happiness-america-ruth-whippman). I
love Brits for their sense of humour!
