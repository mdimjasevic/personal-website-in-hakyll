---
author: Marko Dimjašević
comments: false
date: 2016-03-17 00:44:29+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/03/17/apple-a-for-profit-company/
slug: apple-a-for-profit-company
title: "Apple — A For-profit Company"
wordpress_id: 35257
categories:
- ephemera
post_format:
- Status
tags:
- Apple
- privacy
- profit
- technology
---

If you've subscribed to the recent hype that Apple is for privacy,
[think again][mag]. The truth is they're simply taking care of their
bottom line in the post-Snowden era.

[mag]: https://www.jacobinmag.com/2016/03/apple-fbi-spying-privacy-sweatshops/
