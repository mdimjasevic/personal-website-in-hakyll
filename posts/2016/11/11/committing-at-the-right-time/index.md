---
author: Marko Dimjašević
comments: true
date: 2016-11-11 18:16:39+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/11/11/committing-at-the-right-time/
slug: committing-at-the-right-time
title: "Committing at the Right Time"
wordpress_id: 35521
categories:
- ephemera
post_format:
- Status
tags:
- Git
- JDoop
- software development
- version control
---

Mind the time stamp in my commit to JDoop:

    commit ff8041f748267be7182e07d21be7cb81b2dcb7db
    Author: Marko Dimjašević <marko@cs.utah.edu>
    Date:   Fri Nov 11 11:11:11 2016 -0700
    
        Fixed incorrect error messages
