---
author: Marko Dimjašević
comments: false
date: 2016-06-25 20:28:21+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/06/25/c-taking-toll/
slug: c-taking-toll
title: "C++ Taking Toll"
wordpress_id: 35380
categories:
- ephemera
post_format:
- Status
tags:
- C++
- Google Summer of Code
- KLEE
- object slicing
---

I've already learned more C++ in extending KLEE for GSoC 2016 than I
was comfortable with, and this has been taking its toll. For example,
you cannot have a vector of abstract classes due to [object
slicing](https://en.wikipedia.org/wiki/Object_slicing). Sigh.
