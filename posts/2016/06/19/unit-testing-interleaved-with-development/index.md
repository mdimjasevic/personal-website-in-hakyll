---
author: Marko Dimjašević
comments: false
date: 2016-06-19 04:55:03+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/06/19/unit-testing-interleaved-with-development/
slug: unit-testing-interleaved-with-development
title: "Unit Testing Interleaved with Development"
wordpress_id: 35374
categories:
- ephemera
- free software
post_format:
- Status
tags:
- Firehose
- Google Summer of Code
- KLEE
---

Unit testing is so nice to have as you develop your implementation. If
you get puzzled by a simple test not passing, you can spot it easily
in the implementation of the unit. I'm happy to be writing unit tests
for Firehose support in KLEE.
