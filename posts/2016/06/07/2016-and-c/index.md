---
author: Marko Dimjašević
comments: false
date: 2016-06-07 01:43:20+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/06/07/2016-and-c/
slug: 2016-and-c
title: "2016 and C++"
wordpress_id: 35330
categories:
- ephemera
post_format:
- Aside
tags:
- C++
- Google Summer of Code
- KLEE
---

It's 2016 and I'm learning C++. It feels weird.
