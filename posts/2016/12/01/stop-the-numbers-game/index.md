---
author: Marko Dimjašević
comments: false
date: 2016-12-01 03:23:00+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/12/01/stop-the-numbers-game/
slug: stop-the-numbers-game
title: "Stop the Numbers Game"
wordpress_id: 35526
categories:
- ephemera
- science
post_format:
- Status
tags:
- academic publishing
---

It was 2007 when David Parnas wrote this viewpoint, but almost 10
years later, things haven't changed in (computer) science --- [Stop
the Numbers
Game](http://ce.sharif.edu/~ghodsi/PaP/stop_the_number_game.pdf).
