---
author: Marko Dimjašević
comments: false
date: 2016-01-21 07:34:31+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/01/21/on-the-artifact-evaluation-committee-for-pldi-2016/
slug: on-the-artifact-evaluation-committee-for-pldi-2016
title: "On the Artifact Evaluation Committee for PLDI 2016!"
wordpress_id: 35093
categories:
- reproducible research
- science
tags:
- artifact evaluation
- PLDI
- research artifacts
---

[![Tools by j](tools.jpg "Tools by j"){ width=100% }](https://www.flickr.com/photos/zzpza/3269784239/)


I've been writing about [reproducible
research](/tag/reproducible-research/) for some time now as it is a
very interesting topic to me. It is the very core of research, hence
putting "reproducible" in front of "research" should be the same as
just saying "research", but unfortunately [the situation is
different](/2015/04/03/doing-research-without-doing-research/). Doing
reproducible research is even optional these days. Therefore, I am
limited to participating in the rare optional activities.


<!-- more -->


This year I will be on the [Artifact Evaluation Committee][aec] of the
[Programming Language Design and Implementation
conference][pldi]. Equipped with the
[experience](/2015/05/23/experience-with-artifact-evaluation/)
from being on the same committee of the Computer Aided Verification
conference last year, I am hoping to do well in evaluating research
artifacts of accepted papers to PLDI 2016! I learned quite a few
things last year when it comes to artifact evaluation. It was an
enjoyable service to the community. Let's see what PLDI will bring!


[aec]: http://conf.researchr.org/committee/pldi-2016/pldi-2016-artifacts-evaluation-committee
[pldi]: https://web.archive.org/web/20160114001640/http://conf.researchr.org/home/pldi-2016
