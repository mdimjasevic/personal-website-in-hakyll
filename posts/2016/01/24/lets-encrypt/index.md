---
author: Marko Dimjašević
comments: false
date: 2016-01-24 01:47:54+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/01/24/lets-encrypt/
slug: lets-encrypt
title: "Let's Encrypt"
wordpress_id: 35090
categories:
- free software
tags:
- certificate authority
- encryption
- Let's Encrypt
- security
- SSL
---

[![System Lock by Yuri Samoilov](security-lock.jpg "System Lock by Yuri Samoilov"){ width=100% }](https://www.flickr.com/photos/yusamoilov/13334048894/)


Last year the Electronic Frontier Foundation started an amazing
project called [Let's Encrypt](https://letsencrypt.org). Basically,
it's about providing everyone with a free SSL certificate. Why is that
important? Well, so far if you wanted an SSL certificate that is
recognized by web browsers, operating systems and other platforms, you
would have to buy one, which is pricy for an individual. Yet,
individuals host their web sites, email servers, social networks,
etc., all of which could use an SSL certificate. Most of these folks,
including me, would so far simply create a self-signed certificate for
free, which was better than no certificate, but users of your services
in such a case would get all kinds of security warnings, hence
annoyance.


<!-- more -->


Let's Encrypt is a certificate authority in a hierarchy of recognized
certificate authorities and it provides SSL certificates for free to
everyone. It is even automated to a great extent: one downloads their
free software tool (the whole project is still in beta), runs a
command or two, and that's it!


Just recently I applied for a certificate for my domains with Let's
Encrypt, got it, and now this website, my email server, and my own
cloud are all served through encrypted connections via the valid
certificate! Additionally, I configured my web server to redirect all
http traffic to https. I really like this and am very happy with
it. If you have a server and a domain, but you don't have an SSL
certificate yet, I strongly recommend checking out [Let's
Encrypt](https://letsencrypt.org/).
