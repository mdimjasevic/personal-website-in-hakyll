---
author: Marko Dimjašević
comments: false
date: 2016-01-26 05:52:40+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2016/01/26/added-the-about-page/
slug: added-the-about-page
title: "Added the About page"
wordpress_id: 35127
categories:
- ephemera
post_format:
- Status
---

I just added the [About](/about/) page because I thought it might be
useful to have a digest with lots of things about me.
