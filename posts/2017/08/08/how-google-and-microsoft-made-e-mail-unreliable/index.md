---
author: Marko Dimjašević
comments: false
date: 2017-08-08 16:17:58+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/08/08/how-google-and-microsoft-made-e-mail-unreliable/
slug: how-google-and-microsoft-made-e-mail-unreliable
title: "How Google and Microsoft made E-mail Unreliable"
wordpress_id: 35559
categories:
- ephemera
post_format:
- Status
tags:
- email
- Facebook
- federated services
- Google
- Microsoft
---

A post on [How Google and Microsoft made E-mail
Unreliable](http://penguindreams.org/blog/how-google-and-microsoft-made-email-unreliable/).
