---
author: Marko Dimjašević
comments: false
date: 2017-11-14 20:28:34+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/11/14/types-and-proofs/
slug: types-and-proofs
title: "Types and Proofs"
wordpress_id: 35641
categories:
- software engineering
tags:
- automatic testing
- Curry-Howard correspondence
- proofs
- software
- software corectness
- testing
- type systems
- types
---

[![Courtesy of Ella's Dad](shape-sorter-by-ellas-dad-flickr.jpg "Courtesy of Ella's Dad"){ width=100% }](https://www.flickr.com/photos/ellasdad/425813314/)

I found a powerful
[analogy](https://twitter.com/runarorama/status/929754554111004672)
between software correctness and dishes:

<blockquote>Testing is washing the dishes. Automatic tests are a
dishwasher. Types are proof that your dishes are not even
dirty.</blockquote>


<!-- more -->


In a discussion thread that the analogy opened, there was a
[clarification](https://twitter.com/StephenPiment/status/929767335933571072)
stating that you cannot prove other than what is stated in a theorem:

<blockquote>Type-checking proves that your dishes do not have
particular sorts of food stuck to them. They may still be
dirty.</blockquote>


That is, if your type does not have a particular property, then the
property cannot be proven by a type system. For example, if you use an
integer type to describe the result of a function computing the
factorial, then the type obviously does not state that the factorial
is a non-negative integer. Choosing a less precise type (or even no
type at all!) over a more precise type is a missed opportunity to have
one more proof in your program.


If this dish talk got you confused, read up on [the Curry-Howard
correspondence](https://en.wikipedia.org/wiki/Curry%E2%80%93Howard_correspondence).
