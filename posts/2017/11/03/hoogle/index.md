---
author: Marko Dimjašević
comments: false
date: 2017-11-03 20:54:29+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/11/03/hoogle/
slug: hoogle
title: "Hoogle"
wordpress_id: 35594
categories:
- ephemera
post_format:
- Status
tags:
- documentation
- GHCi
- Haskell
- Hoogle
- type search
---

[Hoogle](https://wiki.haskell.org/Hoogle) is a Haskell API search
engine that integrates nicely with GHCi, Emacs and Firefox! You can
also search by approximate type signature.
