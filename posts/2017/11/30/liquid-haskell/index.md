---
author: Marko Dimjašević
comments: false
date: 2017-11-30 07:13:24+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/11/30/liquid-haskell/
slug: liquid-haskell
title: "Liquid Haskell"
wordpress_id: 35661
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- dependent types
- refinement types
- Haskell
- Idris
- lambda cube
- Liquid Haskell
- logic
- software correctness
- totality
- type systems
---

[Liquid Haskell](https://ucsd-progsys.github.io/liquidhaskell-blog/)
is an extension to Haskell to provide via logic what
[Idris](http://idris-lang.org/) has in its type system (via dependent
types): a way to provide more precise properties to programs,
including totality.
