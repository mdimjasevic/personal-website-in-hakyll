---
author: Marko Dimjašević
comments: false
date: 2017-11-07 06:37:38+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/11/07/programming-language-grouping/
slug: programming-language-grouping
title: "Programming Language Grouping"
wordpress_id: 35597
categories:
- ephemera
- software engineering
post_format:
- Status
tags:
- programming languages
---

Someone
[said](https://twitter.com/jamesmacaulay/status/927672960218939392):

> I doubt there's any more useful way to group programming languages
> than by the kind of bullshit their users are happy to put up with.
