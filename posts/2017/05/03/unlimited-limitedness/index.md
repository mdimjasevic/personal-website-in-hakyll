---
author: Marko Dimjašević
comments: false
date: 2017-05-03 00:30:20+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/05/03/unlimited-limitedness/
slug: unlimited-limitedness
title: "Unlimited Limitedness"
wordpress_id: 35544
categories:
- ephemera
post_format:
- Status
---

Just saw this slogan on an SLC bike share van:

> $75 = UNLIMITED 1-hour rides for a year!

Gotta love this kind of unlimited limitedness.
