---
author: Marko Dimjašević
comments: false
date: 2017-12-03 06:57:04+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/12/03/haskell-for-dummies/
slug: haskell-for-dummies
title: "Haskell for Dummies"
wordpress_id: 35664
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- Haskell
- static typing
---

Haskell for Dummies:
[https://www.snoyman.com/blog/2016/11/haskell-for-dummies](https://www.snoyman.com/blog/2016/11/haskell-for-dummies).
