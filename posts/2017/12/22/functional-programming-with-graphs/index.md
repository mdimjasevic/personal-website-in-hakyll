---
author: Marko Dimjašević
comments: false
date: 2017-12-22 21:41:09+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/12/22/functional-programming-with-graphs/
slug: functional-programming-with-graphs
title: "Functional Programming with Graphs"
wordpress_id: 35673
categories:
- functional programming
tags:
- algorithms
- data structures
- graph theory
- Haskell
---

[![A graph by futtetennista](graph.png "A graph by futtetennista"){ width=70% }](https://futtetennismo.me/posts/algorithms-and-data-structures/2017-12-08-functional-graphs.html)


Back in elementary and high school days I used to attend algorithmic
problem solving competitions where one would have to implement in code
solutions to given problems. At the time I knew nothing about
programming paradigms and I used a paradigm that everyone else was
using: imperative programming. Recently I've been wondering how to
implement data structures and algorithms in a pure functional language
such as Haskell. For example, how to do graph theory in functional
programming?

I am still going through it, but there is a [nice
overview](https://futtetennismo.me/posts/algorithms-and-data-structures/2017-12-08-functional-graphs.html)
of graph theory data structures and algorithms for pure functional
programming, (implemented in Haskell) such as adjacency lists, depth-
and breath-first search, minimum spanning tree and shortest path
(Dijkstra). The referenced implementations are suboptimal when it
comes to running times. However, there is an optimized implementation
in the [Functional Graph
Library](https://www.stackage.org/lts-9.14/package/fgl-5.5.3.1).
