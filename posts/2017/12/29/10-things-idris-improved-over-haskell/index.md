---
author: Marko Dimjašević
comments: false
date: 2017-12-29 21:24:19+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/12/29/10-things-idris-improved-over-haskell/
slug: 10-things-idris-improved-over-haskell
title: "10 Things Idris Improved over Haskell"
wordpress_id: 35686
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- Haskll
- Idris
---

Read a list of [10 things Idris improved over
Haskell](https://deque.blog/2017/06/14/10-things-idris-improved-over-haskell/). For
example, Idris has overloading without type classes, record fields
that are namespaced, a records update and access syntax and Functor
and Monad got fixed. Dependent types are not even considered as
they're out of Haskell's league!
