---
author: Marko Dimjašević
comments: false
date: 2017-12-14 07:42:07+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/12/14/former-facebook-exec-says-social-media-is-ripping-apart-society/
slug: former-facebook-exec-says-social-media-is-ripping-apart-society
title: "Former Facebook Exec Says Social Media is Ripping Apart Society"
wordpress_id: 35668
categories:
- ephemera
post_format:
- Status
tags:
- Facebook
---

Former Facebook exec says social media is [ripping apart
society][fb]. We're all surprised by this.

[fb]: https://www.theverge.com/2017/12/11/16761016/former-facebook-exec-ripping-apart-society
