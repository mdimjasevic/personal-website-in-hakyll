---
author: Marko Dimjašević
comments: false
date: 2017-12-14 15:09:16+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/12/14/dijkstra-on-abstraction/
slug: dijkstra-on-abstraction
title: "Dijkstra on Abstraction"
wordpress_id: 35670
categories:
- ephemera
post_format:
- Status
tags:
- Abstraction
- functional programming
---

> "The purpose of abstraction is not to be vague, but to create a new
> semantic level in which one can be absolutely precise."

> > ~ Edsger W. Dijkstra
