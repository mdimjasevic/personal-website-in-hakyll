---
author: Marko Dimjašević
comments: false
date: 2017-12-30 17:35:07+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2017/12/30/free-monads-in-idris/
slug: free-monads-in-idris
title: "Free Monads in Idris"
wordpress_id: 35688
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- abstract syntax tree
- effectful stream processing
- free monad
- Idris
- IdrisPipes
- interpreter
- monad
---

Free monads in Idris with an introduction and then composable and
effectful stream processing, at [deque.blog][deque]. Still trying to
wrap my head around it.

[deque]: https://deque.blog/2017/11/13/free-monads-from-basics-up-to-implementing-composable-and-effectful-stream-processing/
