---
author: Marko Dimjašević
comments: true
date: 2013-09-02 04:03:12+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2013/09/02/presentation-of-jpf-doop-at-fmcad-2013/
slug: presentation-of-jpf-doop-at-fmcad-2013
title: "Presentation of JPF-Doop at FMCAD 2013"
wordpress_id: 21820
tags:
- FMCAD
- Java PathFinder
- jpf-doop
- Randoop
- software testing
- software verification
- student forum
---

I am happy to say that I have my first document written as a PhD
student and accepted by reviewers. It is a 2-page research summary
titled "Automatic Testing of Software Libraries". The venue where I
will present it is the [FMCAD
2013](http://www.cs.utexas.edu/users/hunt/FMCAD/FMCAD13/index.shtml)
[Student
Forum](http://www.cs.utexas.edu/users/hunt/FMCAD/FMCAD13/student-forum.shtml),
taking place from the 20th to the 23rd of October this year in
Portland, Oregon.


The summary is about research and a tool that we've been working
on. The tool is JPF-Doop, and it is an integration of
[JPF](http://babelfish.arc.nasa.gov/trac/jpf/wiki)-JDart and
[Randoop](https://code.google.com/p/randoop/) into a single
tool. JPF-Doop is a tool for testing of Java libraries. JPF-Doop
comprises feedback-directed random testing from Randoop and concolic
execution from JPF-JDart. So far we have seen decent improvements in
terms of code coverage compared to the baseline Randoop. Hopefully
there will be a release of the tool very soon. I will also demo it at
the FMCAD conference to anyone interested.


At the Student Forum I will give a short talk and have a poster. If
you will be at this year's FMCAD or at any of the collocated events
--- [MEMOCODE](http://www.memocode-conference.com/),
[DIFTS](http://www.cmpe.boun.edu.tr/difts13/),
and[HWMCC'13](http://fmv.jku.at/hwmcc13) --- and you are interested in
software testing and symbolic execution, it would be nice to have a
chat.
