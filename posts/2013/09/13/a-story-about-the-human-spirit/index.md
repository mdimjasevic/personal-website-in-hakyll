---
author: Marko Dimjašević
comments: false
date: 2013-09-13 16:14:24+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2013/09/13/a-story-about-the-human-spirit/
slug: a-story-about-the-human-spirit
title: "A story about the human spirit"
wordpress_id: 22841
tags:
- TED talk
---

I've never written a blog post about a TED talk before, but a moving
story told by Ron McCallum is a remarkable talk. It is a story about
the human spirit. I don't need to write any further about it, just go
and watch the talk at the [TED
website](http://www.ted.com/talks/ron_mccallum_how_technology_allowed_me_to_read.html)
or at [YouTube](https://www.youtube.com/watch?v=pY6ul_P70HY).
