---
author: Marko Dimjašević
comments: true
date: 2013-09-16 05:46:49+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2013/09/16/embedding-bibliographic-metadata-into-a-wordpress-site/
slug: embedding-bibliographic-metadata-into-a-wordpress-site
title: "Embedding bibliographic metadata into a WordPress site"
wordpress_id: 23187
tags:
- bibliography
- COinS
- reference management
- ScholarPress Coins
- scientific writing
- WordPress
- Zotero
---

[![Photo by Dan4th Nicholas](citation_needed1.jpg "Photo by Dan4th Nicholas"){ width=100% } ](citation_needed1.jpg)

If you have a blog and you want to embed bibliographic metadata into
it, you can use ContextObjects in Spans or
[COinS](https://en.wikipedia.org/wiki/COinS) for short. In this way
you make it easier for others interested in your research that you
write about on your blog to have references to your writing. Reference
management software such as
[Zotero](https://en.wikipedia.org/wiki/Zotero) will be able to
retrieve the metadata.


If you're wondering how to embed the metadata with ease to a WordPress
site, there's a WordPress plugin that does the job - [ScholarPress
Coins](http://wordpress.org/plugins/scholarpress-coins/).
