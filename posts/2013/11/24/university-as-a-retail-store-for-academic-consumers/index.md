---
author: Marko Dimjašević
comments: true
date: 2013-11-24 01:28:11+00:00
excerpt: "t"
layout: post
link: https://dimjasevic.net/marko/2013/11/24/university-as-a-retail-store-for-academic-consumers/
slug: university-as-a-retail-store-for-academic-consumers
title: "University as a Retail Store for Academic Consumers"
wordpress_id: 29828
categories:
- free software
- University of Utah
tags:
- Apple
- free software
- Microsoft
- NSA
- surveillance
- Thanksgiving
---

I finished my undergrad at the University of Zagreb in Croatia. It is
great to be in academia in Croatia because it hasn't become a
production line (students being the main product) and market, at least
not to the degree the University of Utah has, where I am a student
these days. However, it won't be long until universities in Croatia
move more aggressively in the same direction.


[![Photo by Nina Hale](production-line.jpg "Photo by Nina Hale"){ width=100% } ](production-line.jpg)


<!-- more -->

I've been studying at the University of Utah since fall 2012. From the
very first moments I started here at the university, I've noticed a
disturbing mind set present throughout the university --- in the USA
everything is for sale, and the university simply follows the national
trend.


Just like [last year][iUni], security and privacy of everyone at the
university is [for][att2] [sale][att1]. The irony is that one does not
pay to obtain security and privacy, but to surrender them to
corporations and the USA government. Believe it or not, people do it
in sheer numbers.


Ever since I started writing this blog, I have been warning about
proprietary software and in general about software that the user does
not have control of. Proprietary software takes freedom away from its
user, and transfers the freedom and control to someone else. If up to
a few months ago you thought these are just empty words, revealings of
what the USA's National Spying Agency (NSA) has been doing should
clarify that they are not empty, because proprietary software is the
key for surveillance. The scale at which the surveillance has been
going on in the USA confirms the opposite of what the USA government
(and the mainstream media) has been claiming: the USA is not a free
and democratic society, but a totalitarian regime.


Proprietary software on your computer takes away the freedom from you,
and these days turns you into a yet another surveillance target. With
proprietary software, the users have no way of making sure it does not
go against them because its inner workings are hidden from them. It
has been shown in countless occasions that proprietary software is
used against its users, recent NSA revealings just confirming it. On
the other hand, free software provides much lower incentives for
software developers to mistreat the users, hence we should always
reject proprietary software and use free software only.


Microsoft software "products", along those by Apple, are the very
instruments for making with the regime. The reason this is the case is
that these "products" --- be it Windows, OS X, Skype, Mac computers or
some other --- have been designed in a way to take away the freedom
from the user, and put someone else in control. Once an excuse had
appeared (to fight terrorism), the government started exercising the
control and utilizing these instruments against the people.


Sadly enough, the university sells these "products" on the university
campus. Just like in the previous years, the university offers the
products discounted and tax-free for Thanksgiving. Therefore, you can
save money while becoming a target of the regime. Students at the
university can even get some of the Microsoft "products" at no cost;
in other words, students don't need to pay money to be under the
surveillance --- Microsoft (and the government) will watch students
gratis.


[![Photo by Digitale Gesellschaft](yes-we-scan.jpg "Photo by Digitale Gesellschaft"){ width=100% } ](yes-we-scan.jpg)


The university has become a retail store selling technology that works
against the people. In spite of all the revealings on how harmful the
technology is, the university actively invites all the university
staff, professors, and students to give away their freedom. By doing
so, the university betrays its purpose, which is to form intellectuals
that will question the world around them and bring prosperity to the
society.

		
[iUni]: /2012/11/09/iuniversity/
[att1]: university-attach-1.txt
[att2]: university-attach-2.txt
