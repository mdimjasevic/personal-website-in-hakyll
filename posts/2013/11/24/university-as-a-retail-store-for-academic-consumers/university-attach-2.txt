To: All Faculty and Staff
From: The Campus Store
Subject: Apple Tax Free Day and Staff & Faculty Night are almost here!
Date: Wednesday, November 20, 2013


Please read below for information about these exciting annual events.


Apple Tax Free Day:

Tuesday, November 26th, at the University Campus Store and on Friday,
December 6th at the Health Sciences Store many popular Apple products
will be available tax-free. This is an opportunity for faculty and
staff to save on Apple products for their educational needs.

Limited to:

* iPad WiFi Standard Models
* iPad Air WiFi Models
* iPad Mini WiFi Models
* MacBook Pro 13” Models
* MacBook Pro 13” Retina Display Models
* iPods

The tax savings is in addition to the educational discount pricing already provided by Apple to the University of Utah. The offer is limited to product on hand.

On December 6th for the first time this event will be offered at the Health Sciences Store located in HSEB.

Purchases will be eligible for the Campus Store’s Employee Payroll Deduction Program, as well as the Tech Trade-in Program. Information on Tech Trade-in is available at:
http://utech.refreshedbyencore.com/

For the convenience of faculty and staff located on upper campus, we
will be providing shuttle service every 15 minutes (11:00 am to 3:00
pm) between the Moran Eye Center and the Campus Store. But remember
this event will also be at the Health Sciences Store on December 6th.


Staff & Faculty Night:

Please join us at the Campus Store on Friday, December 6th for fun activities and savings on many of your favorite items:

* 30%* discount on eligible items all day at the Campus Store, Health Sciences Store and Utah Red Zone locations (Layton, Sandy, Jordan Landing).
* Select Apple products academically priced and TAX-FREE at the Health Sciences Store.
* Light refreshments served from 5:00 p.m. to 7:00 p.m. at the Campus Store.
* Santa at the Campus Store from 5:00 p.m. to 6:30 p.m., so bring the kids.
* Prize drawing at 7:00 p.m.  Must be present to win. Tickets given out upon entry starting at 5:00 p.m., only at the Campus Store.
* Free gift wrapping of your purchased items from 5:00 p.m. to 8:00 p.m. at the Campus Store.
* Author signing and live demonstrations from 5:00 p.m. to 8:00 p.m. at the Campus Store.
* Free parking at pay lots with Campus Store validation.

Please bring your Faculty/Staff ID Card (UCard), a pay stub or a copy
of this email to show at time of check-out to receive your 30%*
discount.  The discount is available all day long with valid
ID. Family and friends are welcome to accompany you, so bring them
along! Tickets for the prize giveaways at the Campus Store will be
given upon entry.


*Cannot be combined with other discounts. Excludes computers, electronics, software, diploma frames, textbooks and special services.

SEE "U" THERE!
