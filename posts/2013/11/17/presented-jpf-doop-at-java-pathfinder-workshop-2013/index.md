---
author: Marko Dimjašević
comments: true
date: 2013-11-17 22:30:50+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2013/11/17/presented-jpf-doop-at-java-pathfinder-workshop-2013/
slug: presented-jpf-doop-at-java-pathfinder-workshop-2013
title: "Presented JPF-Doop at Java Pathfinder Workshop 2013"
wordpress_id: 29125
tags:
- concolic execution
- jDART
- JPF
- JPF Workshop
- jpf-doop
- testing
---

This Tuesday I was in Palo Alto, California to present [JPF-Doop](
https://bitbucket.org/psycopaths/jpf-doop) at [The Java Pathfinder
Workshop
2013](http://ti.arc.nasa.gov/events/jpf-workshop-2013/). JPF-Doop is
an automatic software testing tool I've been working on, which
utilizes a software verification technique; I still haven't put any
license on JPF-Doop, but its source code is available and I will make
it free software eventually. Unfortunately, there is no rush to do so
because the Java Pathfinder's jDART module is a dependency, and it
will take at least a year for NASA to release the module.


Anyhow, I am happy that I attended the workshop. It was a great
opportunity to meet other people working on Java Pathfinder and to get
feedback on JPF-Doop after a talk I gave there. JPF-Doop is still a
work in progress, and because of the workshop I have even more ideas
on what to work on.


If you are interested, you can read an [extended
abstract](jpf-workshop-2013.pdf) that was my ticket to the workshop.
