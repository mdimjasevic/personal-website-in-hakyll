---
author: Marko Dimjašević
comments: false
date: 2013-02-16 08:56:36+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2013/02/16/latex-org-mode-and-those-weird-letters-from-the-european-alphabets/
slug: latex-org-mode-and-those-weird-letters-from-the-european-alphabets
title: "LaTeX, Org-mode, and those weird letters from the European alphabets"
wordpress_id: 2862
categories:
- free software
tags:
- Emacs
- european alphabets
- LaTeX
- lmodern
- Org-mode
- type 3 fonts
- UTF-8
---

Back in the days when I was about to graduate from the University of
Zagreb I had problems with getting OpenOffice format my diploma thesis
the way I wanted it to be. My neighbor had been telling me about LaTeX
and how cool it is so I decided to give it a try. It was two weeks
before the diploma thesis submission deadline that I decided to
rewrite the thesis in LaTeX. It took me some time to get it done, but
I was quite happy with the result.

I've been an Emacs user for a year now. Emacs has a cool module called
[Org-mode](http://orgmode.org/). To quote from
[Wikipedia](https://en.wikipedia.org/wiki/Org-mode), Org-mode is "an
editing and organizing mode for notes, planning, and authoring, in the
free software text editor Emacs." Org-mode is the tool that I use all
day long every day, and one special feature it has is that it lets you
write plain text and it will produce the LaTeX output for you ---
among many other output formats --- automatically. Ever since I
discovered the feature I write in LaTeX substantially less. I let
Org-mode do it for me, and only polish the output if needed.

I've been using Org-mode to write notes in a class and to write
homework too. This semester I'm taking the class on technical
communications, and the professor spotted something strange in my PDF
homework submissions. Org-mode uses LaTeX as an intermediate step to
export to PDF, and the text in the PDFs Org-mode produced was hard to
read due to the low contrast in the PDF reader the professor uses. He
figured out it was because of type 3 bitmapped fonts. I was surprised
to find that out, and my hunch was that Org-mode had something to do
with it.

So I'd opened an Org-mode document and exported it to a temporary
LaTeX buffer to inspect what LaTeX packages Org-mode puts there. I
thought it must have been some weird LaTeX package that was the reason
why PDFs had type 3 fonts. By default Org-mode includes these packages
in each LaTeX export:

* inputenc
* fontenc
* fixltx2e
* graphicx
* longtable
* float
* wrapfig
* soul
* textcomp
* marvosym
* wasysym
* latexsym
* amssymb
* hyperref

Wow, 14 packages for a simple plain text homework, and for most of
them I don't even know what they're for. Then I went to the
[CTAN](http://www.ctan.org/) website and to
[Wikipedia](https://commons.wikimedia.org/wiki/File:Latexsym.svg) to
check them out. After reading the package descriptions, I realized I
don't need all of them. Those that I find useful are _inputenc_,
_fontenc_, _fixltx2e_, _longtable_, _float_, _textcomp_, _latexsym_,
and _hyperref_.

Therefore, Org-mode had to be tweaked. To do that, I had to change the
default value of the _org-export-latex-default-packages-alist_ Emacs
variable. The default value included those 14 packages, and I needed
only the subset. First I checked the help page with:

	C-h v org-export-latex-default-packages-alist

and then opened the _~/.emacs_ Emacs configuration file to add the
following command:

```elisp
(setq org-export-latex-default-packages-alist
      '(("utf8" "inputenc" t)
        ("" "lmodern" nil)
        ("T1" "fontenc" t)
        ("" "fixltx2e" nil)
        ("" "longtable" nil)
        ("" "float" nil)
        ("" "textcomp" t)
        ("" "latexsym" t)
        ("" "hyperref" nil)
        "\\tolerance=1000"))
```


As you can see, there is one additional package that I decided to
include: _lmodern_, the Type 1 font set according to [The (Not So)
Short Introduction to
LaTeX2e](http://ctan.tug.org/tex-archive/info/lshort/english/lshort.pdf). That
did away with the type 3 fonts problem and now only PostScript Type 1
fonts are used. With this setting you can have whatever Latin
characters you wish to have in your Org-mode/LaTeX documents without
any problems.


The funny thing is that I changed the course of this blog post while
writing it. I had a problem with copy-pasting non-ASCII letters from
LaTeX-produced PDFs so I went back to the tutorial mentioned earlier
while writing this post and realized that "all you need todo is to add
[...] to the preamble of your document and you are all set for
creating excellent PDF output with full support for the full Latin
character set." I immediately tried it out and that's it, no more
funny copy-pastes of my name spanning two rows. Usually I wouldn't
include the _lmodern_ package, and when I copy-pasted my name from the
output PDF file I'd get the following:

	Marko Dimjaˇevi ́
    s c

Obviously, that's not the desired result. I guess other folks with
their names having those "weird" letters from the European alphabets
have a similar issue when writing in LaTeX/Org-mode. Hopefully this
blog post will help.
