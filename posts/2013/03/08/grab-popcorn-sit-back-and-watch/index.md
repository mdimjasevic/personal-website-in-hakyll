---
author: Marko Dimjašević
comments: true
date: 2013-03-08 07:31:50+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2013/03/08/grab-popcorn-sit-back-and-watch/
slug: grab-popcorn-sit-back-and-watch
title: "Grab popcorn, sit back, and watch"
wordpress_id: 4776
categories:
- free software
---

You know that situation when someone is embarrassing himself, and it
is so funny that you just feel like you want to grab popcorn, sit back
and enjoy watching him doing it? Recently, somebody was generous
enough to pleasure us in such a way.

[![](grab-popcorn-sit-back-and-watch.jpg){ width=100%} ](https://commons.wikimedia.org/wiki/File:Popcorn_NIH.jpg)

<!-- more -->

Imagine that I told you that, on your hardware, you can install
software, for example an office suite, on one computer at a time. In
other words, you can't have it at two or more computers at the same
time. Then, if you want to "move" the software to another computer,
you can do it only once every 42 days. You wonder why 42 days?
Because! I know, this may seem harsh, so I'll be kind and let you
"move" the software sooner if the hardware fails. Furthermore, if you
do "move" the software to another computer, you can have the software
on that other computer only, i.e. you must delete the copy of the
software from the prior computer and any other copies you may have
had.


Imagine I told you this for your favorite office suite, or any tool
from the suite or any software that does the same job, be it
LibreOffice, LaTeX, GNU Emacs, Org-mode, or any other. You would be
like: "Are you out of your mind? No way!" And then Sheldon Cooper
would walk in and say: "Bazinga!" and both you and me would laugh!


The arbitrary severe restrictions in the joke just told are completely
ridiculous for a few very simple reasons. One, we know how easy it is
to copy software or, for that matter, anything digital --- you just
copy-paste and there you go, you have another copy of it! It is as
simple as that. If you don't have it yet, you can find it at so many
places on the Internet and download it in a matter of seconds. People
find software useful, so they share it for the benefit of the whole
community. Generally speaking, people share ideas they appreciate,
which further increases the value of the ideas they share.


Two, since it is so easy to copy anything digital, would it be ethical
to restrict someone in helping himself by making a copy more than once
every 42 days? Even more, would it be ethical to put restrictions to
use the software for any purpose or at any time? With the general
purpose interconnected computers we have today, there is no ethical
excuse to put such restrictions onto anything digital.


And three, since it virtually requires no material goods, why not make
as many copies of the software as you want, on as many computers you
want? On the other hand, to make another house it takes a lot of
material goods; at the end, you have another house, but something was
taken away from the environment. To make another copy of an idea,
i.e. to pass it on, I just have to tell it to you or write it down for
you to read it, or to put it simply, I just copy-paste it; at the end,
both of us have it, without decreasing the value of "my copy" of the
idea. In fact, now that both of us have the idea, the idea is more
valuable. It's the same with software: if people appreciate it, they
will make lots of copies of it and share it.


These simple principles behind ideas, i.e. behind something
non-material, are well-known within the free software community and
within the free culture community. You learn this kind of things as
you discuss within these communities. We don't split ourselves into
two disjoint groups like the society often does, one group being
creative, and the other one being passive and just consuming. Instead,
we share ideas, develop software and create culture together, provide
feedback to each other, issue software defect reports, improve the
software and the culture, and share for the benefit of all of us. It
feels so good to share the software, the culture, and ideas in
general! Therefore, it would be disturbing and incomprehensible to
impose control over the others in terms of what they can do with the
software or the culture, just for the sake of having power over the
others; it wouldn't be to the benefit of all of us.


So, whenever someone comes up with a joke like the one told earlier
--- and however incredible it sounds, people make jokes like that all
the time --- we grab popcorn, sit back and watch and laugh. We laugh
at people that want to have power over the others, and at the ways
they justify their desire to have the power. We laugh because all of
us have as many copies as we wish of every single idea we came up
with, and we share without imposing silly restrictions onto each
other. You need LibreOffice? Here you go! Need an incredible "text
editor"? Take GNU Emacs and copy it once, copy it millions of times!
Take Kile and create publish-quality documents easily! Do all of these
without having your privacy invaded by the powers that be. Sharing
turns out to our advantage.


If you still didn't grab popcorn, do so now because the fun is about
to start. Ready? Ok, lets start! Two days ago, Microsoft made a
popcorn day by making an
[announcement](http://blogs.office.com/b/office-news/archive/2013/03/06/office-2013-retail-license-agreement-now-transferable.aspx)
about the change in the way you can use the proprietary Office:

<blockquote>

Can I transfer the software to another computer or user? You may
transfer the software to another computer that belongs to you, but not
more than one time every 90 days (except due to hardware failure, in
which case you may transfer sooner). If you transfer the software to
another computer, that other computer becomes the "licensed computer."
You may also transfer the software (together with the license) to a
computer owned by someone else if a) you are the first licensed user
of the software and b) the new user agrees to the terms of this
agreement before the transfer. Any time you transfer the software to a
new computer, you must remove the software from the prior computer and
you may not retain any copies.

</blockquote>


Microsoft, two words for you for making the popcorn day:

[![Photo by vistamommy](grab-popcorn-sit-back-and-watch-thank-you.jpg "Photo by vistamommy"){ width=100% } ](https://commons.wikimedia.org/wiki/File:Thank_you_001.jpg)
