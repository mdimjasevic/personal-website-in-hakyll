---
author: Marko Dimjašević
comments: false
date: 2009-09-24 20:40:28+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2009/09/24/otvorenje-dnevnika/
slug: otvorenje-dnevnika
title: "Otvorenje dnevnika"
wordpress_id: 449
---

Dragi Internete,

eto još jednog bloga koji se možda neće po mnogo čemu razlikovati od
nekog postojećeg bloga ili kojeg drugog sadržaja iz bespuća
Interneta. Pisat ću o stvarima koje me vesele i koje me žaloste, što
iz osobnog života, što o nekim drugim stvarima poput softvera i
slobodne kulture. Neke ključne stvari koje me određuju i o kojima ću
najčešće pisati:

* zaljubljenik u fotografiju,

* volonterizam - alumni sam [BEST-a](http://best.hr/), trenutno član
  [HrOpena](http://www.open.hr/), a uključen sam i u projekte
  [KDE](http://kde.org/) i [OpenStreetMap](http://openstreetmap.org/),

* student i računarac na FER-u - u skladu s time softver i standardi i
  formati kao česta tema,

* slobodna kultura i mediji - zbog novih načina na koje možemo i ne
  možemo biti konzumatori i stvaraoci kulture, također bude riječi i o
  tome.

Ocijeni isplati li ti se pratiti ovaj blog. Svrati s vremena na
vrijeme i možda se predomisliš ;) Možeš ga pratiti i preko
[RSS](http://en.wikipedia.org/wiki/Rss)-a što olakšava uočavanje novih
tekstova.

Proglašavam ovaj blog otvorenim :)
