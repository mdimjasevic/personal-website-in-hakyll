---
author: Marko Dimjašević
comments: true
date: 2009-09-27 23:42:09+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2009/09/27/volonterizam-koketiranje-s-bezumljem/
slug: volonterizam-koketiranje-s-bezumljem
title: "Volonterizam — koketiranje s bezumljem"
wordpress_id: 450
categories:
- BEST
- OpenStreetMap
- volontiranje
tags:
- Križevci
---


Do prije nekoliko godina o volonterizmu sam razmišljao u terminima
poput "Nema tu kruha", "Zašto bih volontirao kad mogu zarađivati pare"
i "Time se igraju oni koji si ne mogu naći ozbiljan posao". U
međuvremenu sam uspio biti član dvije volonterske udruge radeći i
sudjelujući u preko 20 volonterskih projekata, provoditi dane i noći u
raznim volonterskim aktivnostima, u pisanju na tisuće mailova i
mnogobrojnih dopisa i izvještaja i provesti cijelu godinu s uloženim
vremenom praktički ekvivalentnom punom radnom vremenu u vođenju jedne
od tih dviju volonterskih udruga. Što mi se tako loše dogodilo da sam
skrenuo s uma i odlučio raditi bez materijalne naknade dok su drugi
oko mene zarađivali i smijali se mojoj naivnosti?

Još kad sam bio srednjoškolac, a naročito tokom faksa često sam čuo o
studentskoj udruzi [BEST](http://best.hr/). O udruzi sam čuo puno
lijepog i prijedloge da se i ja priključim, no nekako sam uvijek imao
drugih obveza, najčešće vezane uz faks. Na 3. godini faksa odlučio sam
barem sudjelovati u projektu koji organizira taj BEST kako bih konačno
dobio barem blijedi uvid u što se tamo zapravo zbiva. Jednostavnost,
otvorenost i zanos BEST-ovaca tokom tog presudnog vikenda promijenili
su moj pogled na tu bezumnu aktivnost zvanu volonterizam.

Tog vikenda provodio sam vrijeme s drugim studentima koji su također
sudjelovali u dotičnom BEST-ovom projektu kao i sa samim BEST-ovcima
dok su svi normalni studenti uživali u svojem slobodnom vremenu kako
su već htjeli. Sama odluka da si uskratim slobodan vikend zbog nečega
što ni sam ne znam što je to točno bila je suluda ideja. I da stvar
bude gora, niti sam dobio kakve novce niti je bilo kakvo predavanje o
brzoj zaradi.

Sav oduševljen BEST-om odlučio sam se učlaniti u BEST pa sam se
pojavio na njihovom prvom sljedećem sastanku. Grupa ljudi u kojoj se
svakoga uvažava, u kojoj svatko može reći svoje mišljenje, u kojoj se
raspravlja, u kojoj jedni druge podupiru, u kojoj hijerarhija nije
sinonim egoizmu i pri tom novac nije pokretač... je li takvo što
moguće?

Susresti se po prvi puta s volontiranjem na takav način čovjeka ne
može ostaviti ravnodušnim. Ima nešto u tome, nešto što pokreće te
volontere, nešto do čega im je svima stalo - žele učiniti nešto za
druge i pri tom ne očekuju ništa zauzvrat. Uložiti svoje vrijeme,
volju i znanje i podijeliti to s drugima ukratko bi bio opis pojma
nesebičnosti.

U BEST-u sam shvatio dvije ključne stvari. Prva je ta da bilo koja
aktivnost, projekt ili posao može uspjeti samo ako oni koji provode tu
aktivnost na skali svog sustava vrijednosti imaju tu aktivnost vrlo
visoko, odnosno ako imaju dovoljno veliku unutrašnju motivaciju da ju
provode. U njima ta stvar izaziva ushićenje, glavna je tema na
jutarnjim kavama, postaje nešto o čemu znaju i baka, susjeda, sestra,
kolega s posla i mesar iz mesnice u susjedstvu, a koji nerijetko imaju
nakošene obrve s velikim upitnikom iznad glave dok im ushićeni
nestrpljivo pokušavaju objasniti što je to tako super.

Druga ključna stvar je da novac pokvari prvu stvar.

Nije teško uočiti da živimo u materijalnom svijetu i da nekako treba
zaraditi za kruh i osigurati si krov nad glavom. Netko tko se hrva s
preživljavanjem ne vidi vajde u volontiranju jer mu ono neće riješiti
tu osnovnu potrebu.

Oni koji imaju zadovoljene osnovne potrebe teže nečem višem, žele
nekako ispuniti svoj život. Nesebično dijeljenje svog vremena i znanja
s drugima jedan je od načina kako to postići. To dijeljenje kroz
volontiranje odvija se zbog onog sustava vrijednosti u kojem novac ne
kotira visoko. U takvim situacijama novac je zapravo nebitan, čak i
nepoželjan. Jer ako je novac u igri, on budi maštu i želju za većim
materijalnim dobrima od onih koje imamo u tom trenu, eventualno dolazi
do pohlepe i ona prvotna motivacija zbog koje smo se zagrijali za
nešto polako iščezava i prelazi u drugi plan. Tada novac postaje
mjerilo pa se uspoređujemo s drugima, razmišljamo kako čim lakše doći
do više novca, pohlepa raste i raste i neizbježno spremnost na
pomaganje drugima postaje sve manja - tada pojam ljudskosti svoje
postojanje ostvaruje tek u rječniku.

Neki dan sam biciklom prevalio 29.5 kilometara primarno radi
volontiranja, a kao kolateralni efekt ta vožnja je korisna za tjelesno
zdravlje. Sudjelujem u volonterskom projektu
[OpenStreetMap](http://openstreetmap.org/) koji za cilj ima stvaranje
zemljopisne karte cijelog svijeta. Princip izrade te karte vrlo je
sličan onom na kojem funkcionira Wikipedija - mali volonterski
doprinosi mnogo pojedinaca. Ti doprinosi, baš kao i u
[Wikipediji](http://en.wikipedia.org/wiki/Main_Page), postaju dostupni
svima i svih ih mogu slobodno koristiti. Odlučio sam sudjelovati u tom
projektu kartiranjem Križevaca, mog rodnog grada, kao i uže
okolice. Kada sam se već skoro vratio kući, sreo sam
prijatelja. Pričali smo i spomenuo sam mu OpenStreetMap. Pitao me tko
me plaća za to pa kad sam mu rekao da je to volonterski on se samo
nasmijao. Taj puta, kao i mnogo puta prije, spomen da nešto radim
volonterski, odnosno da ne primam materijalnu naknadu izazvao je
čuđenje, a nerijetko i podsmijeh jer si, kako pjeva TBF u svom
Toboganu, "blesa ako nemaš interesa i cilj ti nije kesa". Zaista je
hladna društvena klima.

![Križevci na OpenStreetMapu](krizevci-2009-09-28.png "Križevci na OpenStreetMapu"){ width=60% }

Kad sam bio aktivan u BEST-u bilo je slučajeva da su nam nekoliko
tvrtki nudile "suradnju". Za njih volontirati znači biti besplatna
radna snaga i pod krinkom suradnje htjele su iskoristiti nas volontere
za svoju materijalnu dobit. Naravno da te tvrtke nisu bile svjesne
onih dviju meni ključnih stvari... ili možda jednostavno nisu željele
biti svjesne.

Volontiranjem sam se razvio na mnoge načine i imao sam priliku
upoznati divne ljude koje inače ne bih upoznao. To još uvijek radim i
sve sam bogatiji na razne načine. Pokušavam opovrgnuti onu da je
čovjek čovjeku vuk. Dobivam nešto što se ne može mjeriti novcem. Ma
mora da sam raskrstio sa zdravom pameti!
