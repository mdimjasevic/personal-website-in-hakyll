---
author: Marko Dimjašević
comments: false
date: 2009-11-17 01:09:37+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2009/11/17/staro-nepoimanje-slobode/
slug: staro-nepoimanje-slobode
title: "Staro (ne)poimanje slobode"
wordpress_id: 453
---

> There's a new sense of freedom

> Come banging on your door

> Deep down inside you

> You've seen it all somewhere before

> Somewhere in the past

> Somewhere in a dream

> It's got you completely and you know what it means

> Happy to find out even this late

> You're not a number

> > ~ Chris Rea, You're Not a Number


Izmaklo je ljeto. Izmaklo je ono ljeto u kontekstu dugih suncem
okupanih dana, no i ono u smislu godine... barem školske
godine. Umjetni završetak i početak kalendarske godine prijelazom
vremena iz prosinca u siječanj i nema previše smisla jer je pitanje
što se tada bitnog uopće dogodi. Zapravo ništa. Taj formalni početak
godine je samo izlika za odrasle za koji dan slobodan od posla, iako u
slobodnim danima zapravo nema ništa loše. Zašto se taj prijelaz
kalendarske godine odvija u to vrijeme koje ne predstavlja ništa bitno
nije sasvim logično, no to ni nije toliko bitno za ispričati ovu
priču. Danas je počela nova školska godina i Ivana je imala uvodni sat
iz informatike. U srednjoškolskom programu informatike nalazi se
povijest softvera pa je Ivana danas naučila nešto vrlo neobično.


-- Djede, danas smo u školi na satu informatike započeli lekciju o
povijesti softvera i njegovog utjecaja na kulturu. Kad si ti bio mojih
godina, softver je bio vrlo čudan --- ograničavao je ljude u načinu na
koji mogu koristiti računalo i druge uređaje. Zar je zaista bilo tako?
-- upitala je Ivana djeda kojemu je došla u posjetu nakon škole.


-- Ivana, bila su to zaista frustrirajuća vremena. Tada svijesti o
tome što se zapravo događa gotovo i nije bilo. Krajem 20. stoljeća i
početkom 21. stoljeća softver je bio napravljen za druge svrhe. Služio
je partikularnim interesima privatnih tvrtki. Ideja da softver zapravo
treba služiti svima nama radi obrazovanja, radi pružanja prilike
ljudima da se razvijaju, da nadograđuju kulturu i općenito radi
boljitka društva je bila marginalizirana. Baš kao i obrazovanje,
softver je tada pretežno bio privilegija. Softver je dodatno, suprotno
obrazovanju, ograničavao ljude --- bio je to vlasnički softver. Skoro
sav softver bio je pod kontrolom privatnih tvrtki pa su te tvrtke u
tadašnjem kapitalizmu radi stvaranja što većih profita ograničavali
ljude kroz softver na pasivne potrošače --- tada se nije razmišljalo u
kontekstu korisnika djela. U takvom softveru gotovo nije postojala
mogućnost da ti stvaraš i s drugima dijeliš rezultate svoje
kreativnosti. -- sa sjetom je govorio djed. -- Tada su, softver i
glazba, koju se prepoznavalo kao sve utjecajnijeg čimbenika u životu,
pretežito bili zatvoreni. Njihovi autori su zadržavali sva prava na
svoja djela.


-- Zaista? Kako se onda razvijao softver, a i glazba? Da bi nešto
stvarao, uvijek nadograđuješ na prošlosti, no ukoliko autori
zadržavaju sva prava, to nije moguće, zar ne? -- razmišljala je.


-- Moguće je bilo nadograđivati samo na ono čemu su istekla autorska
prava, odnosno što je postalo javno dobro ili na ono za što si bila u
mogućnosti izdvojiti pozamašne svote novca. Međutim, softver, baš kao
i glazbu, kontrolirale su megakorporacije kojima nije bilo u interesu
išta drugo osim profita pa ih dobrobit krajnjih potrošača i nije
odviše zanimala. Svoju moć koja je proizlazila iz novca, a kojeg su
nakupili vrlo restriktivnim mjerama prema potrošačima, koristili su za
mijenjanje zakona i drugih pravnih propisa kao što su i koristili sam
programski kôd za implementaciju nečega čega nema u zakonu, a na štetu
potrošača, kako bi produživali kontrolu nad djelima, a to je bilo
moguće budući da je [institucionalna
korupcija](http://www.ethics.harvard.edu/lab) bila u procvatu.


-- Kako je onda nastao softver i kultura kakvu danas imamo? Zar se sve
stvaralo iznova?


-- Tokom 1980.-ih nastao je [Pokret slobodnog
softvera](http://en.wikipedia.org/wiki/Free_software_movement) kako bi
se suprotstavio zloporabi moći u softverskoj industriji. Slobodni
softver je upravo onaj koji svi koristimo, no danas pridjev slobodni
nije toliko izražen budući da je vlasnički softver stvar
prošlosti. Kroz sljedećih 10 godina stvoren je operacijski sustav
[GNU/Linux](http://en.wikipedia.org/wiki/GNU/Linux) kao i mnogo drugih
osnovnih softverskih alata, sve [slobodni
softver](http://www.gnu.org/philosophy/free-sw.html). To znači da
slobodno možeš:

* koristiti softver bez potrebe za aktivacijom, registracijom,
  obnavljanjem licence i bilo kojim drugim ograničenjem da sa
  softverom radiš što želiš kao što to radiš s drugim proizvodima,
* unaprijediti softver, bilo sama ili preko nekoga te da ta
  poboljšanja i izmjene podijeliš sa svima kako bi svi od toga imali
  koristi,
* napraviti kopije i dati ih svom susjedu kao i instalirati ih na bilo
  koji drugi uređaj,
* koristiti softver bez neovlaštenog nadzora, [upravljanja digitalnim
  ograničenjima](http://www.defectivebydesign.org/), automatskog
  ažuriranja ili bilo kojeg mehanizma koji ograničava tvoju kontrolu,
* odabrati softver koji ćeš koristiti, odnosno da nećeš biti
  ograničena od strane proizvođača hardvera ili isporučitelja
  softvera.


Kultura je bila u vrlo sličnoj poziciji kao i softver -- nastavio je
djed. -- Njome su velikim dijelom vladale megakorporacije. To je bilo
izričito izraženo u glazbi i filmu. Sami glazbenici, redatelji, glumci
i drugi najčešće nisu polagali prava na svoja djela, već je nosioc
prava bila kuća gdje su stvarali. Oni su zadržavali sva prava. To
očito nije pogodovalo razvoju kulture, već gomilanju utjecaja i novca
u sve manje i manje ruku. Baš kao i u softveru, samo nešto kasnije,
nastao je pokret u kojem umjetnici i općenito nosioci autorskih prava
ne zadržavaju sva prava. Nastao je [Creative
Commons](http://en.wikipedia.org/wiki/Creative_commons), skup licenci
koje običan čovjek može brzo i lako primijeniti, dakle bez potrebe
intervencije odvjetnika. Jedan od prvih filmova koji iznosi priču o
[stanju
kulture](https://en.wikipedia.org/wiki/RiP!:_A_Remix_Manifesto) u to
doba je [RiP!: A Remix
Manifesto](https://archive.org/details/RipRemixManifesto). Te licence
su bile nadopuna tadašnjim već zastarjelim zakonima o autorskim
pravima i autori su kroz te licence govorili na koje načine se mogu
koristiti dana djela.


-- U kakvoj su sprezi bili softver i kultura? -- zanimalo je Ivanu dok
je slušala djeda kako joj priča o tim davnim vremenima.


-- Vlasnički softver tadašnjeg vremena je išao na ruku zatvorenoj
kulturi. Vlasnički softver i zatvorena kultura su se promicali kao
nešto pozitivno i sasvim prihvatljivo čak i u školama i na
fakultetima. Zatvoreni Appleov servis za zvuk i video je sprječavao
potrošače u izradi sigurnosnih kopija materijala kojeg su nabavili
putem tog servisa. Zatvoreni Amazonov uređaj za čitanje e-knjiga je
potrošačima uredno plaćenih i nabavljenih primjeraka knjiga putem
Amazonovog servisa priuštio [brisanje dviju takvih
knjiga](http://arstechnica.com/tech-policy/news/2009/07/amazon-sold-pirated-books-raided-some-kindles.ars)
u stilu Velikog brata jer je vlasnički softver to omogućavao, tj. zato
što je Amazon putem takvog softvera imao kontrolu nad potrošačkim
uređajima. Ironično je da je jedna od obrisanih knjiga bila upravo
Orwellova [1984.](http://en.wikipedia.org/wiki/Nineteen_Eighty-Four)
(dok je druga Životinjska farma). Naravno, vrijedilo je i obrnuto;
[slobodna kultura](http://free-culture.cc/) i slobodni softver išli su
jedno uz drugo i međusobno se podupirali. Slobodni servisi dostupni
putem Interneta postali su sastavni dio slobodnog softvera,
npr. [Jamendo](http://www.jamendo.com/en/) u
[Amaroku](http://amarok.kde.org/),
[OpenStreetMap](http://www.openstreetmap.org/) u
[Marbleu](http://edu.kde.org/marble/),
[Flickr](http://www.flickr.com/) u [digiKamu](http://www.digikam.org/)
itd.


-- Zar mogućnost kontrole sadržaja u potrošačkim uređajima nije
narušavanje privatnosti?


-- To je narušavanje privatnosti, no vladala je indiferentnost među
potrošačima. Tada su si ljudi svojevoljno ili u neznanju uskraćivali
pravo na
[privatnost](http://autonomo.us/2008/07/franklin-street-statement/)
koristeći društvene mreže na Internetu poput Facebooka koji je bio
Hotel California tadašnjeg doba, no ni tada vrlo rašireni operacijski
sustav Windows nije zaostajao u narušavanju privatnosti potrošača. --
prisjećao se djed sveprisutnijeg trenda seljenja vlasničkog softvera s
potrošačkih računala i uređaja na udaljene poslužitelje.


-- Hvala ti, djede, za ovu priču o šokantnom dijelu povijesti za
softver i kulturu! -- rekla je Ivana djedu i polako krenula kući sva
zamišljena o tome kako su ljudi nekad bili nesvjesni svoje tužne
stvarnosti.
