---
author: Marko Dimjašević
comments: false
date: 2009-11-29 20:27:54+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2009/11/29/tumarati-s-neznancima/
slug: tumarati-s-neznancima
title: "Tumarati s neznancima"
wordpress_id: 454
categories:
- OpenStreetMap
- volontiranje
---

Ovaj vikend zimovao sam u Zagrebu. Nisam otišao kući jer je danas bila
zabava. Zabava je bila tematska --- obavezno je trebalo ponijeti
GPS-uređaj, fotoaparat, kartu i nešto dobre volje. Radi se o [vikendu
kartiranja](http://wiki.openstreetmap.org/wiki/Mapping_weekends)
Zagreba, točnije kvartova Borongaj i Trnava za kartu
[OpenStreetMap](http://www.openstreetmap.org/) o kojoj sam već
[pisao](/2009/09/27/volonterizam-koketiranje-s-bezumljem/). Ti
kvartovi do sada su na spomenutom OpenStreetMapu bili nešto slabije
označeni pa se [nekolicina
nas](http://wiki.openstreetmap.org/wiki/16.11.2009_-_drugo_okupljanje#Mapping_party)
okupila u hladno nedjeljno jutro i odlučila to promijeniti. Podijelili
smo područja pa svaki njime zavladali --- vozili smo se spomenutim
kvartovima i tumarali bespućima bezbrojnih uskih ulica --- neki
autima, neki biciklima. Većinu ljudi s kojima sam se danas okupio
nisam prethodno poznavao, a nisu ni oni bili u nešto boljoj poziciji i
to nas nije sprečavalo da se dobro zabavimo, kartiramo, a kasnije i
sjednemo na koji šiš-ćevap, pivu i malo luka. Prisutni su bili skoro
svih uzrasta i oba spola. U srijedu ćemo se ponovno okupiti kako bismo
sakupljene podatke unijeli u OpenStreetMap. I nekoliko fotki za kraj.

![](img_9227.jpg){ width=60% }

![](img_9230.jpg){ width=60% }

![](img_9232.jpg){ width=60% }

![](img_9235.jpg){ width=60% }

![](img_9238.jpg){ width=60% }
