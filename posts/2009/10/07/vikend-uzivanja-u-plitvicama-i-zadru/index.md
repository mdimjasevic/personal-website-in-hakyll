---
author: Marko Dimjašević
comments: false
date: 2009-10-07 15:18:51+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2009/10/07/vikend-uzivanja-u-plitvicama-i-zadru/
slug: vikend-uzivanja-u-plitvicama-i-zadru
title: "Vikend uživanja u Plitvicama i Zadru"
wordpress_id: 451
categories:
- BEST
tags:
- BEST
- fotografija
- Plitvička jezera
- Zadar
---

Prošli vikend bio sam na izletu u organizaciji [BEST-a](http://best.hr)
u Plitvičkim jezerima i Zadru. Povod izletu je bilo
[gostovanje](http://www.best.hr/cultural-exchange/2009/) prijatelja
BEST-ovaca iz francuskog [Nancyja](http://en.wikipedia.org/wiki/Nancy)
i želja da vide i upoznaju i druge dijelove Hrvatske osim Zagreba, a
ja kao alumni prikrpao sam se grupi koja je išla na taj izlet.

Vikend je bio zbilja zanimljiv iz više razloga - po prvi puta sam
posjetio Plitvička jezera što sam iskoristio za fotografiranje (slike
su u nastavku), u Zadru sam se odlično proveo kušajući razna francuska
jela i pića što je uključivalo puževe, a dan poslije je uslijedilo
opuštajuće nedjeljno poslijepodne uz dvosatno prepuštanje morskim
orguljama kao i lagana šetnja obalom gdje se odvijala igra svjetla i
mora što sam opet pokušao zabilježiti fotografijom.

Da ne bi sve bilo idilično pobrinuo se prodavač na benzinskoj postaji
koji je osjetio potrebu iskazati svoj lokalpatriotizam i omalovažiti
sve što ima veze s pojmom Zagreb kojeg je uočio na mojoj majici
kratkih rukava. Kusur za kupljenu robu bio je kratak za jednu kunu na
što sam reagirao zamišljenim pogledom u kusur. Nakon te kratke nijeme
tišine krenuo sam i verbalno primijetiti da je kusur neodgovarajući u
čemu me on nepristojno prekinuo i ljutito odvratio: "Jesam ja prvi
primijetio?" Zašto je u toj situaciji bilo bitno to da li je on prvi
primijetio pogrešan iznos, umjesto da se ispričao - nije mi sasvim
jasno, no uz takve ljude hrvatski uslužni sektor zasigurno ima
svijetlu budućnost.

Kako je vodič u Plitvicama napomenuo, potrudit ću se posjetiti taj
nacionalni park u sva četiri godišnja doba i doživjeti čari koje
tamošnja nevjerojatna priroda pruža. Jesen se računa pod obavljeno pa
je zima na redu!

![](img_8620.jpg){ width=60% }

![](img_8630.jpg){ width=60% }

![](img_8675.jpg){ width=60% }

![](img_8690.jpg){ width=60% }

![](img_8711.jpg){ width=60% }

![](img_8720.jpg){ width=60% }

![](img_8734.jpg){ width=60% }

![Sveta Nevenka](img_8761.jpg "Sveta Nevenka"){ width=60% }

![](img_8765.jpg){ width=60% }

![Zalazak sunca u Zadru](img_8787.jpg "Zalazak sunca u Zadru"){ width=60% }

![](img_8835.jpg){ width=60% }
