---
author: Marko Dimjašević
comments: false
date: 2009-10-23 17:36:14+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2009/10/23/averzija-prema-hrvatskom/
slug: averzija-prema-hrvatskom
title: "Averzija prema hrvatskom"
wordpress_id: 452
categories:
- volontiranje
tags:
- hrvatski jezik
- KDE
---

U devedesetima mobitela skoro pa nije ni bilo, walkman nije bio u
modi, a osobna računala kod kuće su imali samo najčudniji, pogrbljeni,
asocijalni pripadnici muškog spola koji su često bili predmet
svakodnevne sprdnje svoje okoline zbog višesatnog buljenja u
ekran. Danas je situacija sasvim suprotna - skoro svi mladi
(uključujući pripadnice ženskog spola) svakodnevno koriste mobitel i
računalo, iz ušiju im vire slušalice raznih uređaja nakrcanih mnoštvom
glazbe dok su na putu od mjesta A do mjesta B i takvo korištenje
moderne tehnologije je društveno prihvaćeno. Više vam se neće smijati
što par sati dnevno provedete za računalom (to da se tih par sati bez
problema magično rastegne u trećinu dana je sasvim druga tema).

Tom "informatičkom" tehnologijom upravlja softver koji najčešće ima
grafičko sučelje koje pak radi interakcije s korisnikom koristi neki
od jezika. U devedesetima taj je jezik gotovo uvijek bio engleski, no
danas više nije tako rijetko da se pojavi i hrvatski jezik. Opet,
nemojte se iznenaditi ako vidite da se onome koji je u devedesetima
bio čudan, pogrbljen i asocijalan, koji u današnje vrijeme ima
autoritet u tehnologijskim raspravama zahvaljujući upravo tom svom
zavidnom stažu buljenja u ekran, diže kosa na spomen da koristite
softver na hrvatskom jeziku. Zašto je tome tako?

U ta davna vremena kad je softver bio gotovo isključivo na engleskom,
među tim rijetkim korisnicima računala nije bilo jezičara (a niti
zamjetne inicijative za pronalaženjem hrvatskih prijevoda) koji bi
mogli tako promptno predlagati i uvoditi prijevode opsegom brzorastuće
terminologije računala. Jednostavno su se koristili termini i kratice
iz engleskog - PC, joystick, monitor, file, slot, server, reset,
taskbar, printer, itd. Kada godinama koristite te termine, uđu u uho i
postanu sastavnim dijelom govora kad je riječ o računalima... barem
među spomenutom populacijom.

Pojavom sve više softvera prevedenim na hrvatski javio se otpor prema
tim prijevodima među tim "guruima". Jedan od razloga je definitivno
bio novost mnogih riječi u hrvatskom jeziku. Drugi je da ti prijevodi
nisu ni uvijek pogađali bit značenja termina na engleskom. Treći je
prirodno opiranje promjenama. Postoji li četvrti razlog? Može li to
biti nedovoljno poznavanje i engleskog i hrvatskog jezika?

Prije nekoliko mjeseci uključio sam se u prevođenje softvera s
engleskog na hrvatski. Točnije, prevodim grafičko okruženje [K Desktop
Environment](http://kde.org/) (KDE). Osnovna motivacija je bila u tome
što i sam koristim KDE i htio bih ga imati na hrvatskom. Ako će jednog
dana ta lokalizirana inačica koristiti nekome na poslu ili u
obrazovnim institucijama, još bolje! Možda začuđujuće, no to mi nije
posao niti dobivam novčanu kompenzaciju za uloženo vrijeme i trud -
volontiram. Zašto općenito volontiram bila je tema jednog od
[proteklih pokušaja kristaliziranja
misli](/2009/09/27/volonterizam-koketiranje-s-bezumljem/). Umjesto
novca učim se raditi u zajednici okupljenoj putem Interneta i
proširujem i produbljujem svoje znanje engleskog i hrvatskog.

![K Desktop Environment](kde430-desktop.png "K Desktop Environment"){ width=100% }

To (ne)poznavanje engleskog i hrvatskog i odbijanje priznavanja te
činjenice vidim kao četvrti razlog onom spomenutom otporu "gurua"
prema softveru na hrvatskom jeziku. Averzija prema hrvatskom samo je
smokvin list za činjenicu da postoji nemar prema hrvatskom jeziku što
se tiče računalne terminologije. Nije da sugeriram da treba prevesti
apsolutno svaki termin, već da se koristi hrvatska inačica ukoliko joj
ništa ne fali. Prolistate li časopise koji se bave informatikom,
računalima i srodnim temama brzo ćete uočiti engleske riječi dok
postoje sasvim dobri prijevodi kao i često korištene jezičnih
konstrukcija iz engleskog koje nimalo nisu u duhu hrvatskog jezika.

Valja reći da prevođenje softvera ima i negativnosti, a to je promjena
interakcije korisnika s računalom. Naime, tipkovnički prečaci koji
koriste slova iz engleskog jezika i riječi nekad nisu izvedivi u
hrvatskom; npr. Quit koji vrlo često ima prečac Ctrl + Q u hrvatskom
prijevodu "Izlaz" ne sugerira odakle dolazi slovo Q. To prvo slovo Q
moguće je u engleskoj verziji iskoristiti i iz padajućeg izbornika
aplikacije na što se korisnik može naviknuti, a što se mijenja
korištenjem hrvatskog prijevoda.

Ukoliko i ti želiš sudjelovati u prevođenju KDE-a na hrvatski,
slobodno se javi našem timu prevodioca o kojem više možeš saznati na
stranicama [lokalizacije
KDE-a](http://l10n.kde.org/team-infos.php?teamcode=hr).

Ubuduće bih volio vidjeti više hrvatskih termina u spomenutim
časopisima kao i argumentirane rasprave oko toga treba li koristiti
softver na hrvatskom i općenito hrvatske termine.
