---
author: Marko Dimjašević
comments: false
date: 2019-01-02 17:16:04+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/01/02/structurelessness/
slug: structurelessness
title: "Structurelessness"
wordpress_id: 35995
categories:
- ephemera
post_format:
- Status
tags:
- political organisation
---

Jo Freeman has an interesting essay on [the tyranny of
structurelessness](https://www.jofreeman.com/joreen/tyranny.htm). The
lack of structure implies the lack of power to act.
