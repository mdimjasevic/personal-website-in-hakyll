---
author: Marko Dimjašević
comments: false
date: 2019-02-27 16:15:59+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/02/27/competing-against-wadler/
slug: competing-against-wadler
title: "Competing Against Wadler"
wordpress_id: 36039
categories:
- ephemera
post_format:
- Status
tags:
- theorem proving
---

I applied to give a theorem proving seminar at a university. They
chose Philip Wadler as a lecturer over me. That must have been close!
Still, so disappointed...
