---
author: Marko Dimjašević
comments: false
date: 2019-02-06 15:48:28+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/02/06/gave-a-talk-terminating-and-productive-functions/
slug: gave-a-talk-terminating-and-productive-functions
title: "Gave a Talk: Terminating and Productive Functions"
wordpress_id: 36007
categories:
- functional programming
tags:
- function totality
- Haskell
- Idris
- Lambda Zagreb
- type systems
---

[![Courtesy of Tomislav Grospić](lambda-zagreb-totalf-example.jpeg "Courtesy of Tomislav Grospić"){ width=100% }](https://www.meetup.com/lambdazagreb/photos/29669911/478436562/)

At the Lambda Zagreb Meetup this Monday I gave [a talk on terminating
and productive
functions](https://www.meetup.com/lambdazagreb/events/257933910/). It
was an interesting discussion with questions that tied the topic and
examples I had with natural numbers, linear types and communicating
systems.


<!-- more -->


After the talk I slightly reworked the slides. You can get them and
code examples if you are interested:
[download](lambda-zagreb-total-functions-talk.tar.xz).

[![Courtesy of Tomislav Grospić](lambda-zagreb-totalf-audience.jpeg "Courtesy of Tomislav Grospić"){ width=100% }](https://www.meetup.com/lambdazagreb/photos/29669911/478436559/)
