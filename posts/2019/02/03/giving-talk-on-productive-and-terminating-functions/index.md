---
author: Marko Dimjašević
comments: false
date: 2019-02-03 05:48:38+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/02/03/giving-talk-on-productive-and-terminating-functions/
slug: giving-talk-on-productive-and-terminating-functions
title: "Giving Talk on Productive and Terminating Functions"
wordpress_id: 36003
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- function totality
- Haskell
- Idris
- lambda days
---

Today I'll be giving a talk on [terminating and productive
functions](https://www.meetup.com/lambdazagreb/events/257933910/) as
part of the Lambda Zagreb Meetup group. This talk will be very
familiar to my upcoming [Lambda Days
2019](/2018/12/18/speaking-at-lambda-days-2019/) talk.
