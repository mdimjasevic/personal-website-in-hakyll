---
author: Marko Dimjašević
comments: true
date: 2019-02-08 15:51:12+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/02/08/google-and-its-own-software/
slug: google-and-its-own-software
title: "Google and its own Software"
wordpress_id: 36018
categories:
- ephemera
post_format:
- Status
tags:
- Chromium
- Firefox
---

Isn't it ironic --- Google's own proprietary Meet service isn't
supported in Chromium, a web browser by Google, but it is supported in
Firefox, a web browser by Google's competitor.
