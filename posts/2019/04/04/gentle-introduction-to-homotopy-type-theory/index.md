---
author: Marko Dimjašević
comments: false
date: 2019-04-04 06:09:20+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/04/04/gentle-introduction-to-homotopy-type-theory/
slug: gentle-introduction-to-homotopy-type-theory
title: "Gentle Introduction to Homotopy Type Theory"
wordpress_id: 36066
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- Agda
- homotopy type theory
- program optimisation
- type theory
---

If you're a programmer and you want to understand what is homotopy
type theory about, Dan Licata has a [talk on the
topic](https://www.youtube.com/watch?v=caSOTjr1z18).
