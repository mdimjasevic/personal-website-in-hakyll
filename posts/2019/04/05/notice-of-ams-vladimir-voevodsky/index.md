---
author: Marko Dimjašević
comments: false
date: 2019-04-05 06:13:34+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/04/05/notice-of-ams-vladimir-voevodsky/
slug: notice-of-ams-vladimir-voevodsky
title: "Notice of AMS: Vladimir Voevodsky"
wordpress_id: 36069
categories:
- ephemera
post_format:
- Status
tags:
- homotopy type theory
- type theory
---

The American Mathematical Society wrote a [memorial
notice](https://www.ams.org/journals/notices/201904/rnoti-p526.pdf) on
Vladimir Voevodsky where, among others, his contributions to type
theory are considered.
