---
author: Marko Dimjašević
comments: false
date: 2019-04-08 18:58:15+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/04/08/boeing-and-deaths-due-to-software-bugs/
slug: boeing-and-deaths-due-to-software-bugs
title: "Boeing and Deaths due to Software Bugs"
wordpress_id: 36078
categories:
- software engineering
tags:
- 737 max
- boeing
- correct software
- mcas
- software bugs
- type theory
---

The IT field has been aware for decades that software can kill people
and so we are witness of this fact again with Boeing and its two
airplanes 737 Max [crashing][nyt] and killing everyone on board due to
[erroneous software][cnn] in the accompanying MCAS system.


<!-- more -->


The race to profit is actually the root cause and lethal software is
merely its consequence. While I believe we won't fix the way our
society works any time soon, we should insist on what is achievable in
the foreseeable future; one such thing is striving to develop correct
software. The US Federal Aviation Administration has one way of
confirming aircraft software is good to go, which often boils down to
having test runs of software for a big number of hours. They should
also adopt a much more rigorous notion of software correctness based
on formal methods. Writing a mathematical specification and proving
the implementation meets it would dramatically improve the safety of
everyone on an airplane and anywhere nearby an airplane operated by
software.

[nyt]: https://www.nytimes.com/2019/02/03/world/asia/lion-air-plane-crash-pilots.html
[cnn]: https://edition.cnn.com/2019/04/04/world/ethiopian-airlines-crash-preliminary-report-intl/index.html
