---
author: Marko Dimjašević
comments: false
date: 2019-03-10 16:32:01+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/03/10/recording-of-my-talk-at-lambda-days-2019/
slug: recording-of-my-talk-at-lambda-days-2019
title: "Recording of My Talk at Lambda Days 2019"
wordpress_id: 36049
categories:
- functional programming
tags:
- Agda
- dependent tpyes
- function totality
- Haskell
- Idris
---

In February I was at the [Lambda Days
2019](http://www.lambdadays.org/lambdadays2019) conference to give a
talk "[Function Totality: Abstraction Tool in
Programming](http://www.lambdadays.org/lambdadays2019/marko-dimjasevic)"
and a [workshop](http://www.lambdadays.org/lambdadays2019/#workshops)
"Introduction to Dependent Types". The talk had examples in Haskell
and Idris while the workshop was in Agda. Both the talk and the
workshop went quite well and I received nice comments for both. I was
fortunate to have David Turner and John Hughes at my talk and to have
a discussion with them around the topic.

The talk was recorded and if you're interested in watching it, you can
find it [available
online](https://www.youtube.com/watch?v=H6d89kYjSqI). [Talk
slides](lambda-days-2019-talk-slides.pdf) are available too.
