---
author: Marko Dimjašević
comments: false
date: 2019-03-09 08:34:53+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/03/09/insisting-on-a-three-valued-boolean-type/
slug: insisting-on-a-three-valued-boolean-type
title: "Insisting on a Three-valued Boolean Type"
wordpress_id: 36046
categories:
- functional programming
- software engineering
tags:
- type theory
---

If your program can have only two legal states, what sense does it
make to extend a Boolean type with a third data constructor to account
for the possibility of an "illegal program state"? Use a coproduct
type with a Boolean type on the right instead. Make illegal program
states irrepresentable in your types. I'm baffled by folks that insist
on having their three-valued Boolean type.
