---
author: Marko Dimjašević
comments: false
date: 2019-03-19 18:13:28+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/03/19/web-page-is-back/
slug: web-page-is-back
title: "Web Page is Back"
wordpress_id: 36054
categories:
- ephemera
post_format:
- Status
---

After an interruption today, my web page is back online.
