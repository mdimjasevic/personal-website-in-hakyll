---
author: Marko Dimjašević
comments: true
date: 2019-03-05 18:18:41+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/03/05/comparison-of-type-theory-and-set-theory/
slug: comparison-of-type-theory-and-set-theory
title: "Comparison of Type Theory and Set Theory"
wordpress_id: 36043
categories:
- ephemera
post_format:
- Status
tags:
- type theory
---

If you ever wondered how are types different from sets or more
generally what is the difference between type theory and set theory, I
recommend watching a [talk by Thorsten
Altenkirch](https://www.youtube.com/watch?v=bNG53SA4n48) at
Foundations of Mathematics: Univalent Foundations and Set Theory 2016.
