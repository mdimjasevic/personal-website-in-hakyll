---
author: Marko Dimjašević
comments: false
date: 2019-03-23 17:42:19+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/03/23/giving-a-talk-on-propositions-as-types/
slug: giving-a-talk-on-propositions-as-types
title: "Giving a Talk on Propositions as Types"
wordpress_id: 36057
categories:
- functional programming
tags:
- Agda
- Curry-Howard correspondence
- Haskell
- logic
- Programming Language Foundations in Agda
- propositions as types
- theorem proving
- type theory
---

On Tuesday, March 26, I'll be giving a [talk on propositions as
types](https://www.meetup.com/lambdazagreb/events/259929136/) to the
Lambda Zagreb Meetup group. The talk will be in Croatian and it is
titled _¬ sve za ∀ (no ∃ uvod u propozicije kao tipove)_, which can be
translated as _¬ everything is for ∀ (but there ∃ an introduction to
propositions as types)_. Obviously, I incorporated logic operators
into the title with the goal to make it catchy and to attract more
audience with an unusual title. Given that only about 10 people RSVPed
so far and that there are only three days left until the talk, it
looks like I've achieved quite the opposite!


<!-- more -->


In the talk I will glance over the first half of the book [Programming
Language Foundations in Agda](http://plfa.inf.ed.ac.uk) by Wadler and
Kokke, which is about logic and how to represent it via programs in
Agda. I'm going to to give an introduction to propositions as types
(an idea also known as the Curry-Howard correspondence and by many
other names) and motivate people to think in terms of types instead of
sets when writing programs. Although few programming languages have as
advanced type system as Agda does, I'll give guidance to what can be
done in languages like Haskell to program in a type-theoretic way
rather than in a set-theoretic way.
