---
author: Marko Dimjašević
comments: false
date: 2019-03-24 20:54:05+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2019/03/24/interactive-theorem-proving/
slug: interactive-theorem-proving
title: "Interactive Theorem Proving"
wordpress_id: 36063
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- Agda
- Programming Language Foundations in Agda
- programming languages
- propositions as types
- theorem proving
- type theory
---

Interactive theorem proving such as one provided by Agda is so
empowering. The help provided by the compiler while interactively
developing a proof is invaluable. Types guide you to a solution at
every single step.
