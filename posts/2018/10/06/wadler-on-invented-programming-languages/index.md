---
author: Marko Dimjašević
comments: false
date: 2018-10-06 17:27:39+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/10/06/wadler-on-invented-programming-languages/
slug: wadler-on-invented-programming-languages
title: "Wadler on Invented Programming Languages"
wordpress_id: 35863
categories:
- ephemera
- functional programming
- science
- software engineering
post_format:
- Status
tags:
- computer science
- Curry-Howard correspondence
- logic
- type theory
---

Philip Wadler makes a fun note about today's programming languages:

> "Most of you use programming languages that are invented, and you
> can tell, can't you?"


I recommend watching his whole talk [Propositions as
Types](https://www.youtube.com/watch?v=IOiZatlZtGU)!
