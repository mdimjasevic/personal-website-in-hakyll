---
author: Marko Dimjašević
comments: false
date: 2018-01-07 19:12:42+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/01/07/idris-1-2-0-released/
slug: idris-1-2-0-released
title: "Idris 1.2.0 Released"
wordpress_id: 35696
categories:
- functional programming
tags:
- Idris
- LaTeX
---

[Idris 1.2.0](https://www.idris-lang.org/idris-1-2-0-released/) has
been released. There are various updates to the language, including in
a linear types language extension, reworked operator fixity, a new
interface Abs, a few fixes for pretty printing in LaTeX, a new format
for error and warning messages and other.
