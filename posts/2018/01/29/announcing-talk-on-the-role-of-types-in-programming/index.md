---
author: Marko Dimjašević
comments: false
date: 2018-01-29 20:34:46+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/01/29/announcing-talk-on-the-role-of-types-in-programming/
slug: announcing-talk-on-the-role-of-types-in-programming
title: "Announcing: Talk on the Role of Types in Programming"
wordpress_id: 35724
categories:
- functional programming
- software engineering
tags:
- dependent types
- Haskell
- Idris
- Lambda Zagreb
- Meetup
- refinement types
- Scala
- state machines
- type systems
---

On February 6 in Zagreb I'll be giving a talk on the role of types in
programming as part of the [Lambda Zagreb
group](https://www.meetup.com/lambdazagreb/events/247285369/). It will
be about pushing the domain into types. I'll cover various topics,
including dependent types and refinement types. An example that I'll
give and that will hopefully be most interesting is about encoding a
state machine at the type level. This means that if you try to make an
illegal state transition in the code, it won't compile. My code
snippets are going to be in Idris, but I'll provide Haskell and Scala
equivalents where applicable.


The talk is going to be in Croatian, but I'll do my best to have the
slide deck ready in English just in case.
