---
author: Marko Dimjašević
comments: false
date: 2018-01-25 19:08:51+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/01/25/syncthing/
slug: syncthing
title: "Syncthing"
wordpress_id: 35721
categories:
- ephemera
- free software
post_format:
- Status
---

[Syncthing](https://syncthing.net/) is a peer-to-peer file
synchronization software that is cross-platform. For example, it's
available in [F-Droid][fdroid]. It sounds like a great alternative to
dominant proprietary services!

[fdroid]: https://f-droid.org/repository/browse/?fdid=com.nutomic.syncthingandroid
