---
author: Marko Dimjašević
comments: false
date: 2018-01-24 19:05:50+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/01/24/visualizing-types/
slug: visualizing-types
title: "Visualizing Types"
wordpress_id: 35715
categories:
- functional programming
tags:
- Elm
- Haskell
- PureScript
- typedraw
- types
- visualization
---

There is a visualization tool that describes Haskell, PureScript and
Elm types: [typedraw](https://github.com/pkamenarsky/typedraw). How
about Idris and its dependent types? The following images are from
typedraw's [repository](https://github.com/pkamenarsky/typedraw).


[![](typedraw.png){ width=100% }](typedraw.png)

[![](typedraw-2.png){ width=100% }](typedraw-2.png)

This idea of visualizing types nicely goes with an image giving an
[analogy between types and a shape
sorter](/2017/11/14/types-and-proofs/).
