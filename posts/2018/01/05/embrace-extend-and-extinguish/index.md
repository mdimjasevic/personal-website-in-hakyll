---
author: Marko Dimjašević
comments: false
date: 2018-01-05 18:55:27+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/01/05/embrace-extend-and-extinguish/
slug: embrace-extend-and-extinguish
title: "Embrace, Extend and Extinguish"
wordpress_id: 35691
categories:
- free software
tags:
- Android
- Chrome
- Firefox
- Google
- Internet Explorer
- Microsoft
- open standards
- web
- web browser
---

[The Verge][verge] writes on history on web browser dominance, how it
influenced the web and why is the unfortunate history repeating
itself, but this time with Chrome instead of IE. Google has learned
from the best about the [Embrace, Extend and Extinguish][eee]
strategy. They did pretty much the same with Android: it started as a
free software project (if we exclude drivers from consideration for a
moment) and now it is full of Google's proprietary apps and
dependencies. Am I too fast to conclude that this kind of things
happens inevitably in the economic system that we have?


[verge]: https://www.theverge.com/2018/1/4/16805216/google-chrome-only-sites-internet-explorer-6-web-standards
[eee]: https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish
