---
author: Marko Dimjašević
comments: false
date: 2018-07-17 06:16:14+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/07/17/2fa-smartphone-app/
slug: 2fa-smartphone-app
title: "2FA Smartphone App"
wordpress_id: 35785
categories:
- ephemera
- free software
post_format:
- Status
tags:
- 2FA
- andOTP
- Android
---

Whenever they tell you that you have to install a very specific
proprietary software smartphone app for two-factor authentication such
as Google Authenticator or Authy, just nod, install a free software
app such as
[andOTP](https://f-droid.org/packages/org.shadowice.flocke.andotp/)
and pretend you're following instructions. It will work without them
noticing you use free software.
