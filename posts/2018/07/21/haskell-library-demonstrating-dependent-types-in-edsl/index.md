---
author: Marko Dimjašević
comments: false
date: 2018-07-21 12:52:23+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/07/21/haskell-library-demonstrating-dependent-types-in-edsl/
slug: haskell-library-demonstrating-dependent-types-in-edsl
title: "Haskell Library Demonstrating Dependent Types in eDSL"
wordpress_id: 35790
categories:
- functional programming
tags:
- dependent types
- eDSL
- Haskell
- Idris
- servant
- type systems
---

[![Haskell Servant's logo](servant.png "Haskell Servant's logo")](https://haskell-servant.github.io/)

In Haskell, [servant](https://haskell-servant.github.io/) is a library
for declaring web APIs. What is interesting about the library is that
it utilises the power of Haskell's type system, in particular the
power of dependent types in computing a return type of a function.


<!-- more -->


The library has a whole eDSL for writing servers and obtaining client
functions. In a [very well written educational
post](https://haskell-servant.github.io/posts/2018-07-12-servant-dsl-typelevel.html),
Alp Mestanogullari gradually explores the problem of constructing a
link from its components and motivates type-level programming in
servant and in Haskell in general. This allows for more precise
expression of the intent behind the library and for far fewer bugs to
creep into the library and target program using the library. Of
course, the post demonstrates a simple usage of dependent types, but
still a very useful and powerful one. In case you are interested to
learn more about dependent types, I encourage you to also take a look
at [Idris](https://www.idris-lang.org/), a pure functional language
with Haskell-like syntax and full support for type-level programming,
i.e. full support for dependent types.
