---
author: Marko Dimjašević
comments: false
date: 2018-07-25 15:25:41+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/07/25/explicit-forall-in-haskell/
slug: explicit-forall-in-haskell
title: "Explicit forall in Haskell"
wordpress_id: 35794
categories:
- functional programming
post_format:
- Aside
tags:
- GHC
- Haskell
- RankNTypes
- type systems
---

Haskell's forall, in combination with some GHC extensions, allows for
liberal type synonyms and scoped type variables, among others. Source:
[School of Haskell][soh].

[soh]: https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/guide-to-ghc-extensions/explicit-forall
