---
author: Marko Dimjašević
comments: false
date: 2018-08-24 14:15:07+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/08/24/typed-holes-in-programming/
slug: typed-holes-in-programming
title: "Typed Holes in Programming"
wordpress_id: 35822
categories:
- ephemera
- functional programming
post_format:
- Aside
tags:
- Agda
- Haskell
- Idris
- type systems
- Type-driven development
---

Typed holes are an amazing tool in a developer's toolbox. The holes
give hints via types how to finish an implementation. They're
available in [Haskell][h], [Agda][a] and [Idris][i].

[h]: https://downloads.haskell.org/~ghc/7.8.1/docs/html/users_guide/typed-holes.html
[a]: https://agda.readthedocs.io/en/v2.5.4/getting-started/quick-guide.html
[i]: http://docs.idris-lang.org/en/latest/tutorial/typesfuns.html
