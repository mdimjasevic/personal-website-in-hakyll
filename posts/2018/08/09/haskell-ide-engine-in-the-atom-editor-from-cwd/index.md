---
author: Marko Dimjašević
comments: false
date: 2018-08-09 15:17:14+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/08/09/haskell-ide-engine-in-the-atom-editor-from-cwd/
slug: haskell-ide-engine-in-the-atom-editor-from-cwd
title: "Haskell IDE Engine in the Atom Editor from cwd"
wordpress_id: 35804
categories:
- ephemera
- functional programming
post_format:
- Aside
tags:
- Atom
- Haskell
- Haskell IDE Engine
---

To get the Haskell IDE Engine working in the Atom editor, one has to
start Atom in the directory of a project. I'd call this a bug.
