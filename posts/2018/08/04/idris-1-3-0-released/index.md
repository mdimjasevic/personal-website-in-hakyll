---
author: Marko Dimjašević
comments: false
date: 2018-08-04 08:02:00+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/08/04/idris-1-3-0-released/
slug: idris-1-3-0-released
title: "Idris 1.3.0 Released"
wordpress_id: 35800
categories:
- ephemera
- functional programming
post_format:
- Aside
tags:
- Idris
---

It happened two months ago, but [Idris 1.3.0 is
out](https://www.idris-lang.org/idris-1-3-0-released/).
