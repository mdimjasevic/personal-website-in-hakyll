---
author: Marko Dimjašević
comments: false
date: 2018-08-21 15:47:06+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/08/21/totality-in-programming/
slug: totality-in-programming
title: "Totality in Programming"
wordpress_id: 35813
categories:
- functional programming
- software engineering
tags:
- Abstraction
- Agda
- Idris
- total function
- totality
---

You might think it's only imperative programmers that don't see value
in function totality, but neither do many functional programmers. The
ability to abstract in programming is severely impaired when
[totality](https://en.wikipedia.org/wiki/Total_functional_programming)
is ignored. A programmer should strive to work in a programming
language with a compiler that supports totality checking, such as Agda
and Idris.
