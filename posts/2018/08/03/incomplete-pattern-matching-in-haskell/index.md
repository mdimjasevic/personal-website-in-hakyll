---
author: Marko Dimjašević
comments: false
date: 2018-08-03 14:06:25+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/08/03/incomplete-pattern-matching-in-haskell/
slug: incomplete-pattern-matching-in-haskell
title: "Incomplete Pattern-matching in Haskell"
wordpress_id: 35797
categories:
- ephemera
- functional programming
post_format:
- Aside
tags:
- Agda
- Haskell
---

From Haskell's
[safe-exceptions](https://hackage.haskell.org/package/safe-exceptions)
library: "[I]ncomplete pattern matches can generate impure
exceptions." This makes me think of Agda, its totality and absence of
exceptions.
