---
author: Marko Dimjašević
comments: false
date: 2018-08-15 15:18:02+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/08/15/haskell-ide-the-memory-hog-engine/
slug: haskell-ide-the-memory-hog-engine
title: "Haskell IDE The Memory Hog Engine"
wordpress_id: 35810
categories:
- functional programming
- software engineering
tags:
- GHC
- Haskell
- Haskell IDE Engine
---

Momentarily, the Haskell IDE Engine (HIE) is in a bad shape. In its
version 0.2.2, it eats a huge amount of memory, much more than my
machine can provide (I have 8 GB in total). Users have
[already](https://github.com/haskell/haskell-ide-engine/issues/412)
[reported](https://github.com/haskell/haskell-ide-engine/issues/665)
[this](https://github.com/haskell/haskell-ide-engine/issues/580), and
even though there is a memory leak in GHC, Haskell IDE Engine
developers haven't started tackling this problem yet. What you can do
in the meantime is to provide runtime system (RTS) options to an
engine executable to limit its heap and stack memory sizes. For
example, you can write a wrapper script around the `hie` executable
that would run it with the following RTS options:

```bash
	hie +RTS -c -M1500M -K1G -A16M -RTS --lsp $@
```


This limits the heap size to 1500 MB and the stack size to 1 GB. The
executable will crash once it surpasses these limits, but your editor
(e.g., Atom) should restart it. In other words, HIE will keep on
crashing and restarting in a loop, which is ugly, but that's what we
have now.
