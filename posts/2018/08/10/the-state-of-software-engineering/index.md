---
author: Marko Dimjašević
comments: false
date: 2018-08-10 14:37:31+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/08/10/the-state-of-software-engineering/
slug: the-state-of-software-engineering
title: "The State of Software Engineering"
wordpress_id: 35806
categories:
- ephemera
- software engineering
post_format:
- Aside
tags:
- xkcd
---

The state of software engineering is best illustrated by an [XKCD on
voting software](https://xkcd.com/2030/).
