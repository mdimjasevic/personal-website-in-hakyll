---
author: Marko Dimjašević
comments: false
date: 2018-09-07 15:35:12+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/09/07/fast-grepping-source-code/
slug: fast-grepping-source-code
title: "Fast Grepping Source Code"
wordpress_id: 35840
categories:
- ephemera
- software engineering
post_format:
- Aside
tags:
- Haskell
---

For fast searching of source code that avoids peeking at your build
files use the `ag` tool from Debian's `silversearcher-ag` package,
like this:

```bash
	ag -s --haskell what where
```
