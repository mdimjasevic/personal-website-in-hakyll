---
author: Marko Dimjašević
comments: false
date: 2018-09-06 14:40:39+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/09/06/less-bad-mtl/
slug: less-bad-mtl
title: "Less bad MTL"
wordpress_id: 35838
categories:
- functional programming
tags:
- effects
- free monad
- Haskell
- interpreter
- monad transformers
- MTL
---

I stumbled upon a recording of a [talk by George
Wilson](https://www.youtube.com/watch?v=GZPup5Iuaqw) on the monad
transformer library (MTL) in Haskell. He goes on to demonstrate how
monad transformers can be made more composable by using constraints
instead of directly putting a monad transformer in the return type of
a function. What is presented there is definitely an improvement,
however it is still unsatisfactory. In particular, in his examples
there is no separation of describing and running a program, which
means everything happens in the IO monad (the MonadIO constraint in
function signatures in the examples). In other words, anything goes
and such functions are again too powerful.


<!-- more -->


Can one avoid specifying right away what effect is at a particular
stack level in a monad transformer? Can a monad transformer be used to
stack embedded domain-specific languages (eDSLs) instead of effects?
In such a case, a monad transformer would give a stack of eDSLs that
would get interpreted by an interpreter. I am not sure if this is even
achievable and what it would represent, but it seems to me it would be
something similar to free monads.
