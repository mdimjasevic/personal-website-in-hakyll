---
author: Marko Dimjašević
comments: false
date: 2018-09-04 14:46:39+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/09/04/idris-for-haskellers/
slug: idris-for-haskellers
title: "Idris for Haskellers"
wordpress_id: 35833
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- Haskell
- Idris
---

In case you are a Haskeller interested in learning Idris, check out a
[short list of
things](https://github.com/idris-lang/Idris-dev/wiki/Idris-for-Haskellers)
to look out for.
