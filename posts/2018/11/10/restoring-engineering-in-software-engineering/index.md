---
author: Marko Dimjašević
comments: false
date: 2018-11-10 18:31:20+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/11/10/restoring-engineering-in-software-engineering/
slug: restoring-engineering-in-software-engineering
title: "Restoring Engineering in Software Engineering"
wordpress_id: 35931
categories:
- free software
- software engineering
---

[![Courtesy of Mark Bonica](studying.jpg "Courtesy of Mark Bonica"){ width=250px }](https://www.flickr.com/photos/23119666@N03/8028219837/)


Software has become ubiquitous. It's all around us and in us. It is in
our pockets, in cars, airplanes, stock exchanges, our bodies, our
offices and in lots of other places. As a computer scientist and a
programmer I've worked in aeronautics, computer security, finance and
automotive industry and I can say that software engineering is a
mess. The Atlantic demonstrates this point rather thoroughly in an
interesting piece on [the state of the software engineering][saving]
profession. I agree with the article and here I will briefly tie this
to what I see to be a solution, split into four parts: 1) the
licensing of professional software engineers, 2) a requisite degree in
computer science, 3) mandatory formal methods in software development,
and 4) all software should be free as in freedom.


<!-- more -->


## The licensing of professional software engineers

[The licensing][lic] is needed so that we have a common standard of
what are minimums to become a software engineer. The impact that
software has on our everyday lives is too great to be in the hands of
those without a license. Many lives have been lost due to errors in
software and more will be lost. We need to do all we can to minimise
such unfortunate events in the future, and requiring a license to be a
software engineer is a great step in that direction. Having such a
license would be a great responsibility, which could also be taken
away from an engineer if they wouldn't adhere to principles set by the
field. It is appalling to imagine what the world would look like if we
didn't expect doctors, lawyers and architects to have a license.


## A requisite degree in computer science

Today anyone can be a professional programmer. They don't even have to
be an engineer. As expected, bad practices take place in software
development, which has negative consequences for software users. It
ought to be mandatory to have a university degree in computer science
to be a professional programmer. To program, one has to approach this
task in a systematic way, relying on knowledge of giants that came
before them. University is a place where a student is guided in a
systematic way in accumulating, expanding and refining their knowledge
and skills, all based on the common wealth of knowledge the field has
acquired.


## Mandatory formal methods in software development

Far too many programmers see programming and mathematics as two
disparate fields. Such programers don't see much utility of
mathematics in programming. Yet mathematics is the best tool we will
ever have in programming. Even more so, thanks to the Curry-Howard
isomorphism, we know that mathematical proofs are in direct
correspondence with computer programs. Mathematics, and in particular
formal methods, should be taught at universities and required in
professional software development.


## All software should be free as in freedom

As Lawrence Lessig put it in Code and Other Laws of Cyberspace, code
is law. Those who control the code are in position of power. To avoid
subjugating software users, all software has to be free as in
freedom. Software users wouldn't be passive consumers anymore; they
would have a chance to engage with what controls their
surroundings. If all software was free and thus available for others
to inspect, programmers wouldn't be incentivised to sweep under the
rug their bad practices and bad code. Fewer software errors would go
unnoticed. Finally, students and professionals would have a plethora
of good examples to learn from.

If the four proposals were to be adopted, only then we could talk
about "engineering" as in software engineering.


[saving]: https://web.archive.org/web/*/https://www.theatlantic.com/technology/archive/2017/09/saving-the-world-from-code/540393/
[lic]: http://theinstitute.ieee.org/ieee-roundup/members/achievements/misconceptions-about-licensing-software-engineers
