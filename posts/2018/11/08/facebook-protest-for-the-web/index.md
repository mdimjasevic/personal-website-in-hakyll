---
author: Marko Dimjašević
comments: true
date: 2018-11-08 15:59:16+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/11/08/facebook-protest-for-the-web/
slug: facebook-protest-for-the-web
title: "Facebook Protest for the Web"
wordpress_id: 35925
categories:
- ephemera
- free culture
post_format:
- Status
---

After [having
destroyed](https://www.defectivebydesign.org/blog/w3c_sells_out_web_eme_1_year_later)
one part of the Web, Sir Tim Berners-Lee is organising his Facebook
protest for the Web:
[https://contractfortheweb.org/](https://contractfortheweb.org/). Now
that him, Google and Facebook RSVPed, we rest assured nothing's gonna
happen.
