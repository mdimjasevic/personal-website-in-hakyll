---
author: Marko Dimjašević
comments: false
date: 2018-11-07 16:18:57+00:00
excerpt: "\n\t\t\t\t\t\t"
layout: post
link: https://dimjasevic.net/marko/2018/11/07/hooked-on-dependent-types/
slug: hooked-on-dependent-types
title: "Hooked on Dependent Types"
wordpress_id: 35916
categories:
- functional programming
tags:
- Agda
- books
- dependent types
- Idris
- type theory
---

Just this morning these two books landed my mailbox:

[![The Littler Typer and Verified Functional Programming in Agda](the-little-typer-verified-functional-programming-in-agda-small-300x225.jpg "The Littler Typer and Verified Functional Programming in Agda")](the-little-typer-verified-functional-programming-in-agda-small.jpg)

The Little Typer is a new book on dependent type theory, written by
Daniel P. Friedman, an author of The Little Schemer, and David Thrane
Christiansen, an Idris contributor. Verified Functional Programming in
Agda is a book by Aaron Stump, on using dependent types in Agda to
prove various properties of programs. After having read the
Type-driven Development with Idris book by Edwin Brady, I am hoping
these two books will significantly expand my knowledge of and improve
my skills in type theory, theorem proving and typed functional
programming. Looking forward to reading the books! If you haven't
noticed by now, I got hooked on dependent types!
