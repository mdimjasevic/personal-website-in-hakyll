---
author: Marko Dimjašević
comments: false
date: 2018-11-16 21:23:30+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/11/16/proof-assistant-in-teaching/
slug: proof-assistant-in-teaching
title: "Proof Assistant in Teaching"
wordpress_id: 35953
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- Curry-Howard
- logic
- programming languages
- proof assistant
- teaching
- type theory
---

Benjamin C. Pierce writes what it is like to [use a proof assistant to
teach](https://www.cis.upenn.edu/~bcpierce/papers/plcurriculum.pdf)
programming language foundations.
