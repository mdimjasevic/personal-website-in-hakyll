---
author: Marko Dimjašević
comments: false
date: 2018-06-03 19:54:15+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/06/03/haskell-user-survey-2018/
slug: haskell-user-survey-2018
title: "Haskell User Survey 2018"
wordpress_id: 35773
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- Haskell
- software tooling
---

If you're interested in how Haskell adoption in practice looks like in
2018 compared to 2015, read the [State of Haskell
2018](https://www.fpcomplete.com/hubfs/Haskell-User-Survey-Results.pdf)
user survey's results by FP Complete.
