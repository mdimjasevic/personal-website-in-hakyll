---
author: Marko Dimjašević
comments: false
date: 2018-03-01 07:58:21+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/03/01/in-the-meantime-in-radiology/
slug: in-the-meantime-in-radiology
title: "In the Meantime in Radiology"
wordpress_id: 35752
categories:
- science
tags:
- artificial intelligence
- machine learning
- radiology
---

In the meantime at the European Congress of Radiology 2018...

![](ml-ai-ecr-edited-225x300.jpg)
