---
author: Marko Dimjašević
comments: false
date: 2018-03-08 06:42:08+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/03/08/debian-meltdown/
slug: debian-meltdown
title: "Debian Meltdown"
wordpress_id: 35758
categories:
- ephemera
- free software
post_format:
- Status
tags:
- Debian
- Linux
- meltdown
- security
---

For some reason it happened only a few days ago that Debian GNU/Linux
in the current stable release code-named Stretch got a kernel patch
for the meltdown horror. That's over two full months since Meltdown
made it to news.
