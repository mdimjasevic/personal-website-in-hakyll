---
author: Marko Dimjašević
comments: false
date: 2018-12-13 06:59:57+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/12/13/programming-lego-mindstorms-ev3-in-haskell/
slug: programming-lego-mindstorms-ev3-in-haskell
title: "Programming Lego Mindstorms EV3 in Haskell"
wordpress_id: 35986
categories:
- functional programming
tags:
- EV3
- Haskell
- Lego Mindstorms
- NXT
- robotics
---

Earlier this week I got a Lego Mindstorms EV3 robotics kit. I was
aware there is a Haskell binding, but I didn't know it is compatible
only with the earlier NXT generation of the Mindstorms line, at least
according to the [Wikipedia page on
EV3](https://en.wikipedia.org/wiki/Lego_Mindstorms_EV3). I'm hoping to
find out today if the [NXT Haskell
library](http://hackage.haskell.org/package/NXT) can nevertheless be
used with the EV3. If not, I'm likely to start writing a Haskell
library for EV3.
