---
author: Marko Dimjašević
comments: false
date: 2018-12-07 16:55:11+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/12/07/giving-a-talk-at-ieee-on-function-totality/
slug: giving-a-talk-at-ieee-on-function-totality
title: "Giving a Talk at IEEE on Function Totality"
wordpress_id: 35974
categories:
- ephemera
- functional programming
- software engineering
post_format:
- Status
tags:
- IEEE
- total function
---

The upcoming Tuesday I will be giving a
[talk](https://web.archive.org/web/20181207095301/https://www.ieee.hr/ieeesection?@=2ozxh)
at the IEEE Croatian Section on function totality. The talk will take
place at the University of Zagreb, the Faculty of Electrical
Engineering and Computing.
