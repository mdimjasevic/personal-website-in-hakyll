---
author: Marko Dimjašević
comments: false
date: 2018-12-11 06:19:32+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/12/11/gave-a-talk-on-function-totality/
slug: gave-a-talk-on-function-totality
title: "Gave a Talk on Function Totality"
wordpress_id: 35983
categories:
- functional programming
tags:
- Haskell
- Idris
- talk
- totality
- type theory
---

As
[announced](/2018/12/07/giving-a-talk-at-ieee-on-function-totality/)
last week, yesterday I [gave a talk][ar] on function totality at the
Institute of Electrical and Electronics Engineers, Croatian
Section. The talk was located at the University of Zagreb at the
Faculty of Electrical Engineering and Computing. Slides used during
the talk are freely
[available](https://gitlab.com/mdimjasevic/predavanje-totalne-funkcije). The
talk was in Croatian.


<!-- more -->


I'd say the talk went pretty well. It was just under an hour long. I
was pleasantly surprised by the level of knowledge of students that
attended the talk! They were fluent with Haskell and Idris, with type
systems, they were familiar with the Curry-Howard isomorphism, and one
student even brought up category theory in a question. There were
interesting discussions about types, testing, and software
verification. I'll have to do these talks more often!


[ar]: https://web.archive.org/web/20181207095301/https://www.ieee.hr/ieeesection?@=2ozxh
