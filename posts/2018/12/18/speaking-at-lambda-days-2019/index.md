---
author: Marko Dimjašević
comments: true
date: 2018-12-18 21:34:40+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/12/18/speaking-at-lambda-days-2019/
slug: speaking-at-lambda-days-2019
title: "Speaking at Lambda Days 2019!"
wordpress_id: 35989
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- lambda days
- total function
---

I'm happy to announce that I'll be speaking at the [Lambda Days 2019
conference](http://www.lambdadays.org/lambdadays2019#speakers)! I'll
be giving a talk on function totality.
