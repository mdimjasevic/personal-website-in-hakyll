---
author: Marko Dimjašević
comments: true
date: 2018-05-24 16:09:32+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/05/24/felleisen-on-dynamically-typed-languages/
slug: felleisen-on-dynamically-typed-languages
title: "Felleisen on Dynamically Typed Languages"
wordpress_id: 35770
categories:
- ephemera
- functional programming
- software engineering
post_format:
- Status
tags:
- type systems
---

> "If you are not married, if you don't want to spend time with your
> kids, if you hate vacations, [dynamically typed languages] is the
> way you program!"

> > ~ [Matthias Felleisen](https://youtu.be/XTl7Jn_kmio?t=1577)		
