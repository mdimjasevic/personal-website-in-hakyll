---
author: Marko Dimjašević
comments: true
date: 2018-05-10 04:45:36+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/05/10/on-dynamically-typed-languages/
slug: on-dynamically-typed-languages
title: "On Dynamically Typed Languages"
wordpress_id: 35767
categories:
- ephemera
- software engineering
post_format:
- Status
tags:
- dynamically typed languages
- type systems
---

> "Test-driven development replaces a type checker in a dynamically
> typed language in the same way that a bottle of whiskey replaces
> your daily problems."

> > ~ [Matt Gumbley](https://mobile.twitter.com/mattgumbley/status/727140296912441345)		
