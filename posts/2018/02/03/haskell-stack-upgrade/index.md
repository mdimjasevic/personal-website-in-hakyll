---
author: Marko Dimjašević
comments: false
date: 2018-02-03 16:01:39+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/02/03/haskell-stack-upgrade/
slug: haskell-stack-upgrade
title: "Haskell Stack Upgrade"
wordpress_id: 35726
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- Haskell
- Stack
---

To upgrade to the latest available stable version of [Haskell
Stack](https://haskellstack.org/), just run `stack update`.
