---
author: Marko Dimjašević
comments: false
date: 2018-02-15 07:01:32+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/02/15/haskell-at-google-summer-of-code-2018/
slug: haskell-at-google-summer-of-code-2018
title: "Haskell at Google Summer of Code 2018"
wordpress_id: 35744
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- GSoC
- Haskell
- type systems
---

Haskell got accepted to [the Google Summer of Code
2018](https://summerofcode.withgoogle.com/organizations/5706672807346176/). Check
out [the list of ideas](https://summer.haskell.org/ideas.html) to work
on.
