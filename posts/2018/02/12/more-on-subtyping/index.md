---
author: Marko Dimjašević
comments: false
date: 2018-02-12 19:23:00+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/02/12/more-on-subtyping/
slug: more-on-subtyping
title: "More on Subtyping"
wordpress_id: 35729
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- Scala
- subtyping
- type systems
---

"Those who learn more about subtyping are more likely to avoid it,
just as those who learn more about typing are more likely to advocate
it", says [Stephen Compall][subt] and [I couldn't agree
more](/2017/11/10/a-subtyping-polymorphism-misfortune/).

[subt]: https://failex.blogspot.hr/2018/02/writing-about-subtyping.html?m=1
