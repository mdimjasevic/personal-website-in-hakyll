---
author: Marko Dimjašević
comments: false
date: 2018-02-27 17:52:59+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2018/02/27/on-function-types/
slug: on-function-types
title: "On Function Types"
wordpress_id: 35747
categories:
- ephemera
- functional programming
post_format:
- Status
tags:
- type systems
- types
---

Ken Scambler has [an interesting point on
types](https://twitter.com/KenScambler/status/621933432365432832). "Should
I use String in this method signature?" "Is the Mandarin edition of
the complete works of Shakespeare valid input?"
