---
author: Marko Dimjašević
comments: false
date: 2010-03-24 17:01:29+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/03/24/sok-za-sacicu/
slug: sok-za-sacicu
title: "Šok za šačicu"
wordpress_id: 181
---

Jučer navečer u Križevcima održao sam svoju prvu ovogodišnju
šok-terapiju u sklopu [najavljenog
Kulturšoka](/2010/03/13/kultursok-u-krizevcima/). Prostor
Kluba kulture, poprište šokova, bio je ispunjen praznim stolcima te je
taj sklad razbila tek šačica Križevčana, neki odvažni šokirati, neki
odvažni da budu šokirani, a neki jednostavno zatečeni svojim obvezama
na tom mjestu visoke koncentracije šokova.  Došao sam kojih pola sata
prije početka kako bih se pripremio. To se posebno ticalo
računala. Naime, koristim GNU/Linux i kako ga promoviram i drugima
preporučam, to mi stvara određeni pritisak pa sam morao biti siguran
da će sve raditi kako spada. I zaista, sve je radilo bez greške.
Prisutne sam počastio animiranim filmom Sita Sings The Blues
producentice Nine Paley. Ako si kojim slučajem zažalio što si
propustio priliku pogledati taj film, ne žali više jer film možeš
sasvim besplatno i legalno
[preuzeti](http://www.sitasingstheblues.com/wiki/index.php?title=SitaSites),
a dotična Paley neće se požaliti ako ti se film toliko svidi da se
odlučiš na [donaciju](http://www.sitasingstheblues.com/donate.html).

Film je zbog licence pod kojom je objavljen bio odlična uvertira u
prezentaciju i raspravu o autorskim pravima i njihovom utjecaju na
razvoj kulture. Prezentaciju sam više nego voljan podijeliti sa svima
pa se osjećaj slobodnim [preuzeti
ju](http://www.c-shock.org/wp-content/uploads/2010/03/utjecaj-autorskih-prava-na-razvoj-kulture.odp). Nakon
rasprave dobio sam poziv za gostovanje u emisiji Radio Križevaca Ne
dirajte mi krugove urednika Ratka Matića. Rado ću se odazvati tom
pozivu i ukoliko se to ima dogoditi, bit će sljedećeg utorka
(30. ožujka) u 17:00 kada ću pričati o autorskim pravima u kulturi i
dati uvod za
[tribinu](http://www.c-shock.org/bogatstvo-mreza-utjecaj-drustvene-proizvodnje-na-trzista-i-slobodu/)
koju održavam istog dana navečer opet u sklopu Kulturšoka. Ukoliko
poželiš poslušati tu emisiju, dovoljno je radio prijemnik ugoditi na
96,6 MHz. Ako pak nemaš dobar prijem, ne očajavaj, Radio Križevce
možeš slušati i putem Interneta. Za čuti poznate glasove i melodije
potrebno je upotrijebiti adresu
[http://78.129.201.228:9002/](http://78.129.201.228:9002/).

		
