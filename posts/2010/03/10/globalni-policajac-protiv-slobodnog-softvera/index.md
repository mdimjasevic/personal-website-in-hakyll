---
author: Marko Dimjašević
comments: false
date: 2010-03-10 14:19:36+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/03/10/globalni-policajac-protiv-slobodnog-softvera/
slug: globalni-policajac-protiv-slobodnog-softvera
title: "Globalni policajac protiv slobodnog softvera"
wordpress_id: 147
tags:
- autorska prava
- IIPA
- intelektualno vlasništvo
- MPAA
- patenti
- RIAA
- SAD
- slobodan softver
- ZAMP
---

SAD, zemlja s one strane Bare, poznata je zemlja u svijetu po svojoj
nametljivoj ulozi u praktički svemu. Sve žele regulirati i svugdje
moraju imati glavnu riječ. Ta zemlja mora stalno i ratovati, jer ako
ne ratuje, mase u brojnim naraštajima mladića imali bi priliku ostati
živi i živjeti sa svojim obiteljima ili, ukoliko ne bi sudjelovali u
kojem ratu, ne bi se mogli patriotski hvalisati jer su branili zemlju
svoju od nedefiniranog neprijatelja, a bez patriotizma vlast SAD-a
našla bi se u velikim problemima jer bi zapravo morali dati argumente
za svoje postupke. Kad SAD ne bi ratovao, propala bi cijela industrija
proizvodnje oružja ili bi u najmanju ruku bila ugrožena, a to znači da
bi mnogi Amerikanci ostali bez posla što, naravno, nije
poželjno. Ironično je da je predsjednik SAD-a, Barack
Obama, 2009. godine dobio Nobelovu nagradu za mir. Poznanici su se
tome nasmijali govoreći kako ju je zaista i zaslužio jer SAD vodi samo
dva rata, a mogao je voditi četiri!

Vlada SAD-a puni budžet zahvaljujući i perverziji zvanoj
"intelektualno vlasništvo". Pojam je nastao 60-ih godina prošlog
stoljeća i obuhvaća autorskih prava, patente i zaštitne
znakove. Zakoni o svakoj od tih pojedinih stvari toliko su različiti
kao i stvari kojima se bave da je neozbiljno pokušati ih svesti pod
[jedno zajedničko
ime](http://www.gnu.org/philosophy/not-ipr.html). Koliko je naziv
nategnut pokazuje i pokušaj stvaranja osjećaja krivnje prilikom
"krađe" ideje u analogiji s krađom tuđeg vlasništva. Naime, kad mi
netko ukrade bicikl ja ga zaista više nemam, a lopov ima jedan
više. Međutim, ideje nije moguće "ukrasti". Ako imam neku ideju i
prenesem ju nekome drugome, tada tu ideju imamo oboje i ja nisam
siromašniji za tu jednu ideju. Problem je, dakle, što se materijalni i
nematerijalni svijet očito razlikuju u tome što je jedan definiran
oskudnošću dobara, a drugi nije.

Kao što sam spomenuo, u "intelektualno vlasništvo" spadaju i
patenti. Nažalost, patenti su prodrli u domenu softvera. Da je
besmisleno i štetno po društvo patentirati algoritme, odnosno brojeve
složili su se i najveći svjetski umovi u računarstvu poput [Donalda
Knutha](http://progfree.org/Patents/knuth-to-pto.txt). Najkrupniji
"proizvođači" softvera u svijetu žele zadržati i povećati svoj
antikompetetivni položaj pa godišnje prijavljuju na tisuće softverskih
patenata kako bi legalizirali nejednakost startnih pozicija u tržišnom
nadmetanju. Tu spadaju IBM s rekordnih 4914 patentnih prijava
u 2009. godini, Microsoft, Apple, HP, Intel i dr. Zanimljivo je uočiti
da sve te tvrtke imaju sjedište u SAD-u. To znači i da značajan
priljev u državni proračun SAD-a dolazi od poslovanja tih tvrtki. Kada
tvrtke postanu tako velike i utjecajne kao spomenute, država se nalazi
u velikom problemu jer vlada [počinje zastupati interese tih tvrtki u
privatnom vlasništvu](http://www.storyofstuff.com/) umjesto interesa
građana koji su izabrali vladu!

Međutim, interesi privatnih tvrtki koji su na uštrb društva ne
promoviraju se samo unutar granica SAD-a, već i puno šire, praktički
gdje god mogu. Tako je SAD, točnije Veleposlanstvo Sjedinjenih
Američkih Država nedavno branilo interese tih megakorporacija u
Lijepoj našoj u Zagrebu --- održan je
[Intelektiv](http://www.intelektiv.com/), konferencija o
"intelektualnom vlasništvu". Nije izostao blagoslov domaćih
institucija i organizacija u nazadovanju društva --- konferenciju su
podržali Ministarstvo gospodarstva, rada i poduzetništva, Državni
zavod za intelektualno vlasništvo, Hrvatska gospodarska komora,
Institut Ruđer Bošković, Hrvatsko društvo za autorsko pravo, Američka
gospodarska komora u Hrvatskoj, Hrvatsko društvo skladatelja ZAMP i
dr. Zanimljivo je uočiti kako neke od tih institucija i organizacija
zapravo rade u interesu svoga džepa tako da legalno otimaju od građana
kao što to npr. radi ZAMP. Svaki puta kada kupite prazan medij kao što
su CD, DVD, USB memorija, tvrdi disk i dr. plaćate harač ZAMP-u jer su
uspješno lobirali za zakon koji presumira da smo mi kupci takvih
medija kršitelji zakona o autorskim i srodnim pravima pa nas pri
svakoj kupnji preventivno udare po džepu. Nije teško pogoditi koje su
dvije američke korporacije bile glavni sponzori Intelektiva. Radi se,
naravno, o Microsoftu i IBM-u. Iskreno se divim sposobnosti tih
korporacija da na legalan način i uz pljesak vladinih institucija i
organizacija štete društvu. Dođe mi da zaplačem što nisam i ja mogao
biti u svom sjaju Hotela Sheraton na otvorenju Intelektiva rame uz
rame vladinim visokim poslanicima pa da i ja zaplješćem jer pljesak
zaista zaslužuju!

Ako ste poželjeli vladi svoje države predložiti jedinu moralnu i
ispravnu opciju što se tiče nabavke softvera, a koja ujedno i
dugoročno smanjuje troškove, razmislite još jednom jer vlada koja ima
makar preporuke u vezi preferencija slobodnog softvera (nekad se još
zove i softver otvorenog koda) završit će na ucjenjivačkoj listi SAD-a
što se tiče trgovine. International Intellectual Property Alliance
(IIPA), čiji su članovi neke najveće američke organizacije --- kad je
riječ o "intelektualnom vlasništvu" --- poput Motion Picture
Association of America (MPAA) i Recording Industry Association of
America (RIAA), u domeni podupiratelja digitalne slobode često znani
kao karteli koji koriste svoju moć za gušenje sloboda na Internetu i
kako bi postavili društvo tako da radi za njihovo profiterstvo, poslao
je [dokument od 498
stranica](http://www.regulations.gov/search/Regs/contentStreamer?objectId=0900006480aa8547&disposition=attachment&contentType=pdf)
predstavniku Sjedinjenih Američkih Država za trgovinu Ronu Kirku u
kojem u biti govori da je jedini način da se vlasnički softver bori
protiv slobodnog softvera ne kvalitetom, već [stvaranjem pravnih i
trgovinskih
ucjena](http://news.northxsouth.com/2010/03/07/special-301-report-versus-free-software-strong-arm-tactics-is-the-only-way-proprietary-software-can-compete/). Tako
IIPA SAD-u preporučuje da sve države u svijetu koje imaju smjernice
oko usvajanja slobodnog softvera stavi na posebnu listu te da ju
koristi tokom trgovinskih ucjena, ako hoćete --- pregovora. U te
države spadaju i Brazil, Indonezija, Filipini, Tajland, Vijetnam i
Ekvador. Tim i takvim državama, smatra IIPA, SAD ubuduće treba
uvjetovati povlačenje preporuke ili regulative za usvajanje slobodnog
softvera ako i dalje žele trgovati. Drugim riječima, IIPA smatra da te
države nisu suverene i da netko drugi za te države treba odlučivati
kakav je softver najbolji za njih, a po njihovom mišljenju najbolji je
vlasnički softver, odnosno onaj koji više košta i koji uskraćuje
slobodu krajnjim korisnicima. Jedino kroz takav softver moguće je
provlačiti netransparentnost, patente, reket i sve drugo za što se
članice IIPA-e zalažu. Tom dokumentu žestoko se usprotivio i Open
Source Initiative koji [prokazuje redikuloznost tog
dokumenta](http://www.opensource.org/node/511).

Vrijeme će pokazati primijenjuje li SAD [selektivnu
prisilu](http://en.wikipedia.org/wiki/Selective_enforcement) ili će s
vremenom na posebnoj listi za ucjenu završiti i Ujedinjeno
Kraljevstvo. Jedino zanimljivije od toga bilo bi da se apsurd upotpuni
embargom na samog sebe jer [Bijela kuća](http://www.whitehouse.gov/)
ima web koji je slobodan softver.


**Dopuna 16. ožujka 2010.**

Na godišnjoj konferenciji američke banke za izvoz i uvoz Obama je
potvrdio prihvaćanje izvještaja IIPA-e, odnosno izrazio je svoje
[slaganje s unutra navedenim
nebulozama](http://news.cnet.com/8301-31001_3-20000347-261.html).

Obama upakirava ucjenu u patriotizam i to nudi kao jedino moguće
rješnje za smanjenje nezaposlenosti Amerikanaca. U obraćanju kao
papiga ponavlja ono što su mu utjecajne američke organizacije i
korporacije došapnule u dokumentu od 498 stranica, a to je besmislica
da preporuke vlada nekoliko država o usvajanju slobodnog softvera
znače neravnopravnost i nepovoljniji položaj za američke korporacije
iz softverske industrije. Naime, vlade, ukoliko se radi o suverenim
državama, imaju potpuno pravo odrediti uvjete pod kojima se nabavlja
softver za vladine organzacije te ako postave slobodan softver kao
jedan od uvjeta, to ne sprečava ni jednu američku korporaciju da se
prijavi na raspisani natječaj i ponudi svoje rješenje u slobodnom
softveru. Vjerojatno ono što ih boli je činjenica da samo jedna
korporacija u 500 najvećih u svijetu bazira svoje biznis na slobodnom
softveru (Red Hat, Inc.), a mnogo je više onih koje se baziraju na
vlasničkom softveru.

Predsjednik je zaključio i kako je promicanje prljavih interesa
američkih korporacija dobro i za globalnu ekonomiju što samo potvrđuje
tezu o njihovoj želji za dominacijom nad ostatkom svijeta. Sjećanja
na 11. rujna još uvijek su vrlo bolna pa predsjednik mudro koristi
priliku i priziva ta sjećanja čestim spominjanjem američke sigurnosti
i aludiranjem na borbu protiv terorizma, odnosno rat u nedogled protiv
nedefiniranog neprijatelja.

Predsjednik je nagovijestio i upotrebu Anti Counterfeiting Trade
Agreementa (ACTA) koji omogućuje cenzuru Interneta, a sve za dobrobit
američkog gospodarstva. Prijedlog tog zakona (bio) je obavijen [velom
tajne](http://www.keionline.org/acta-petition) jer nije postojala
javna rasprava i ako je tko od privilegiranih želio vidjeti prijedlog
zakona, morao je potpisati ugovor kojim se obavezuje da to što pročita
neće komunicirati s javnošću. Poznati profesor, odvjetnik i aktivist
[Lawrence Lessig](http://identi.ca/lessig) potpisnik je otvorenog
pisma poslanog Obami u vezi prijedloga ACTA-e koje upućuje na to kako
je tajnost tog prijedloga uvreda za inteligenciju.

Da bi predsjednik dobio potporu vjernika i vjerskih institucija,
upotrijebio je otrcanu frazu često viđenu u hollywoodskim filmovima
kojom i zaključuje obraćanje: "God bless you. God bless United States
of America." Da vjeruje u boga na kojeg se poziva i da gaji zajedničke
moralne vrijednosti koje su prisutne u svim svjetskim religijama, ne
bi mu trebala figa u džepu dok izgovara te riječi.

Predstavnik SAD-a za trgovinu (US Trade Representative) već je
posjetio Brazil koji se nalazi na spomenutoj ucjenjivačkoj listi, a
začudo, Obama će uskoro u Indoneziju koja se isto nalazi na
ucjenjivačkoj listi.

Izabran od naroda, umotan u zastavu SAD-a putuje svijetom i zastupa
interese korporacija.

_God bless you. God bless United States of America._

		
