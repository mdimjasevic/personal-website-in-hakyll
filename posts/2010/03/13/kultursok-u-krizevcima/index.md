---
author: Marko Dimjašević
comments: false
date: 2010-03-13 21:05:23+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/03/13/kultursok-u-krizevcima/
slug: kultursok-u-krizevcima
title: "Kulturšok u Križevcima!"
wordpress_id: 150
tags:
- autorska prava
- Culture Shock Festival
- Križevci
- Sita Sings The Blues
- zajednica
---

Ove godine održava se 6. po redu [Culture Shock
Festival](http://www.c-shock.org/) ili kako bi po naški rekli -
kulturšok. Razlog zbog kojeg spominjem taj festival je
samopromidžbeni. Bit će projekcija jednog animiranog filma nakon kojeg
ću povesti raspravu o autorskim pravima i njihovom utjecaju na razvoj
kulture. U drugom terminu održat ću tribinu o stvaranju raznolikog
sadržaja putem Interneta i bez (presuđujućeg) utjecaja tržišta. Da ne
okolišam, evo egzaktnih podataka:


Projekcija

[Sita Sings The Blues](http://www.c-shock.org/sita-sings-the-blues/) \
23. ožujka, 2010. u 19:30 \
Klub Kulture, Mažuranićev trg 1, Križevci


Tribina

[Bogatstvo mreža - utjecaj društvene proizvodnje na tržišta i slobodu](http://www.c-shock.org/bogatstvo-mreza-utjecaj-drustvene-proizvodnje-na-trzista-i-slobodu/) \
30. ožujka, 2010. u 19:30 \
Klub Kulture, Mažuranićev trg 1, Križevci


Bilo bi mi drago da na projekciju dođe čim više umjetnika i onih koji
se tako osjećaju, a ako među čitateljima ovog bloga ima i nekoga iz
glazbenih izdavačkih kuća ili filmskih kuća, kako bi temi pristupili
iz što više različitih kuteva.

Na tribini bih posebno volio vidjeti sociologe, psihologe, volontere i
one kojima je stalo do zajedničkog dobra.
