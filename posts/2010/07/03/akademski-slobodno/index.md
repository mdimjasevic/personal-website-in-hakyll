---
author: Marko Dimjašević
comments: false
date: 2010-07-03 15:46:38+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/07/03/akademski-slobodno/
slug: akademski-slobodno
title: "Akademski — slobodno?"
wordpress_id: 203
tags:
- slobodan softver
---

Već sam u nekoliko navrata pisao o slobodnom softveru i za čudo tako
je i ovdje --- ovoga puta vezano je uz diplomski rad koji trenutno
pišem.

Ukratko --- oduševljen sam radom na svojem diplomskom, a sve zbog
slobodnog softvera. Diplomski je implementacija [jednog
protokola](http://www.apps.ietf.org/rfc/rfc4555.html) koja je u
konkretnom slučaju [slobodni softver](http://ikev2.zemris.fer.hr/) i
sve što radim u vezi tog diplomskog pišem u slobodnom softveru. Alati
i općenito softver koji koristim su:
	
* [OpenOffice.org](http://www.openoffice.org/) za pisanje teksta
  (Writer) i izradu grafova (Draw),
* [Qt
  Creator](http://qt.nokia.com/products/appdev/developer-tools/developer-tools#qt-tools-at-a),
  okruženje za razvoj softvera,
* [Graphviz](http://www.graphviz.org/), alat za vizualizaciju
  (mrežnih) grafova,
* [Doxygen](http://www.doxygen.org/), sustav za dokumentaciju za
  brojne programske jezike među kojima je i C u kojem pišem kôd,
* [Flex](http://flex.sourceforge.net/) i [GNU
  Bison](http://www.gnu.org/software/bison/), alati za leksičku i
  sintaksnu analizu,
* [GLib](http://library.gnome.org/devel/glib/), biblioteke za programe
  pisane u C-u,
* [GCC](http://gcc.gnu.org/), skup kompajlera,
* [Git](http://git-scm.com/), distribuirani sustav za reviziju (kôda),
* [Gitorious](http://gitorious.org/), pružatelj infrastrukture za
  projekte slobodnog softvera koji koriste Git i na njega pohranjujem
  [kôd](http://gitorious.org/ikev2) dok je i sam Gitorious slobodni
  softver i, naravno,
* operacijski sustav [GNU/Linux](http://www.gnu.org/), točnije
  distribucija [Chakra](http://chakra-project.org/).


Moje oduševljenje proizlazi iz privilegije koju imam obzirom na
trenutno stanje u akademskoj zajednici po pitanju softvera. Slobodan
softver i akademska zajednica trebali bi imati mnogo toga zajedničkog,
no s usponom i dominacijom vlasničkog softvera zadnjih nekoliko
desetljeća situacija je bitno drugačija. Na fakultetima je uobičajeno
i mnogo puta obavezno korištenje softvera čiji su autori odlučili
zabraniti akademskoj zajednici da ga proučava, unaprijeđuje i dijeli
taj softver sa širom zajednicom. Akademska zajednica je prihvatila
takve uvjete, odnosno korištenje softverskih crnih kutija za svoj
svakodnevni rad. Za mnoge fakultete, odnosno područja znanosti ne
postoji istovrsni slobodni softver. Žalosno je što se situacija ne
mijenja pa fakulteti, barem koliko sam se uspio uvjeriti na nekoliko
fakulteta Sveučilišta u Zagrebu, ne ulažu resurse u razvijanje njima
potrebnog softvera. Potrebni resursi po fakultetu za takvo što mogu se
podijeliti među fakultetima kojima treba istovrstan softver.

U javnim školama i na javnim sveučilištima troši se novac poreznih
obveznika. To je dodatni razlog zašto se taj novac ne bi smio trošiti
na tako neefikasan način, odnosno na vlasnički softver. Za privatne
škole i učilišta po ovom pitanju ne marim jer oni koji pohađaju
privatne škole očito imaju veći izbor zahvaljujući svojem financijskom
stanju --- njihov je izbor pohađati privatnu školu.

Odgovor na pitanje zašto bi se slobodan softver uopće trebao koristiti
u javnim školama dan je u [eseju Richarda
Stallmana](http://www.gnu.org/philosophy/schools.html).
