---
author: Marko Dimjašević
comments: false
date: 2010-11-29 14:56:33+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/11/29/kralj-bez-krune/
slug: kralj-bez-krune
title: "Kralj bez krune"
wordpress_id: 242
---

> Bilo jednom davno jedno kraljevstvo. Kralj se zaželio nove tvrđave s
> velebnim bedemima pa je raspisao natječaj. Izabran je vrsni arhitekt
> Pero. Pero je kralju postavio jedan uvjet --- dok se gradi tvrđava,
> kraljevska kruna mora biti na gradilištu kako bi inspirirala
> radnike. Kralju je isprva bilo nelagodno, no kad mu je Pero obećao
> da će to biti najveća i najljepša tvrđava u ovim krajevima, kralj je
> omekšao i udovoljio tom zahtjevu. Naredio je da se nabavi sav
> potreban materijal za izgradnju tvrđave jednom kad je Pero osmislio
> tvrđavu.

> Dani su tekli, izvođači radova su radili danonoćno i izgradnja
> tvrđave je lijepo napredovala. Kralj je bio oduševljen. S
> nestrpljenjem je iščekivao novu tvrđavu i povratak krune. Noćima
> nije mogao zaspati od uzbuđenja i iščekivanja dana svečanog
> otvorenja tvrđave.

> Pero je javio kralju da je tvrđava završena i da ju dođe svečano
> otvoriti. Kralj se veselo uputio prema tvrđavi. Kada je stigao do
> bedema tvrđave, dočekala su ga zatvorena vrata. Pero je stajao na
> bedemima. Kralj je doviknuo Peri da mu otvori vrata, no Pero je
> oholo odbio. U tom trenu Pero je stavio krunu na svoju glavu i
> proglasio se kraljem. Vojsci je naredio da uhvate bivšeg kralja i
> bace ga u tamnicu, a ovaj se dao u bijeg. Ogorčeni bivši kralj je iz
> straha za svoju glavu pobjegao van kraljevstva i tako pao u
> zaborav. Čiča miča...


Predlažem jednu misaonu igru --- u gornjoj priči zamijenite neke
riječi. Zamijenite oznaku vremena jednom davno s danas, kraljevstvo s
Republikom Hrvatskom, kralja s poreznim obveznicima, tvrđavu s
nematerijalnim bogatstvom kao što su znanje, podaci i umjetnost,
bedeme sa zakonima i tehnološkim arhitekturama koje uvode podjele,
arhitekta Peru i izvođače radova sa znanstvenim, kulturnim i
obrazovnim institucijama, znanstvenicima, umjetnicima, inženjerima i
društvenjacima koje financiraju porezni obveznici, kraljevsku krunu s
autorskim i srodnim pravima i pravima na patente, građevni materijal s
novcem poreznih obveznika, a vojsku s izvršnom vlasti. Kada to
napravite, shvatite da je od nas poreznih obveznika otuđeno bogatstvo
koje je zbog svoje opsežnosti teško i popisati --- geodetski podaci
koje smo platili nisu naši pri čemu je izvođač radova Državna
geodetska uprava, rezultati znanstvenih istraživanja koje smo platili
nisu naši pri čemu su izvođači radova javni fakulteti, filmovi koje
smo platili nisu naši pri čemu su izvođači filmski umjetnici,
računalni programi koje smo platili nisu naši pri čemu su izvođači
privatne tvrtke, a ono što možda i je naše, do toga ne možemo zbog
tehnoloških arhitektura koje su nastale da nam onemoguće pristup,
korištenje, unapređenje i dijeljenje znanja, podataka i
umjetnosti. Zakoni i tehnološke arhitekture su ti koji nas sprečavaju
da nazad dobijemo ono što nam pripada. Spomenuta smo prava predali
izvođačima radova. Ako se i usudimo pokušati uzeti ono što nam
pripada, ubrzo ćemo se naći u marici i nakon toga na optuženičkoj
klupi.

Hrvatska iz dana u dan postaje sve siromašnija i to svojom krivnjom
--- krunu je odlučila povjeriti izvođačima radova. Svakodnevno našim
novcem nastaje ogromno nematerijalno bogatstvo, a ono izmiče iz naših
ruku.

Da bi se to promijenilo, izvođačima radova trebamo reći: "Kako biste
stvorili nematerijalno bogatstvo, nije vam potrebna naša kruna. Kruna
može ostati i ostaje na našoj glavi." Prevedeno u pravne termine, to
znači da izvođačima radova trebamo postaviti uvjet da autorska, srodna
i patentna prava ostaju naša, odnosno da budu takva tako da svaki
građanin ima slobodan pristup tom nematerijalnom intelektualnom
bogatstvu. Tek tada će to postati bogatstvo, dostupno svima jer svima
i pripada. Pripadnost tog bogatstva velikom broju ljudi učinit će to
bogatstvo još većim, ono će učiniti sve građane bogatijima bez gubitka
vrijednosti pri dijeljenju tog bogatstva s drugima.
