---
author: Marko Dimjašević
comments: false
date: 2010-10-14 11:45:27+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/10/14/poziv-na-debatu-debate-challenge/
slug: poziv-na-debatu-debate-challenge
title: "Poziv na debatu/Debate challenge"
wordpress_id: 214
categories:
- BEST
tags:
- BEST
- free software
- slobodan softver
---

(English version below)

**Izmjena:** Mario je, nastavljajući u istom tomu, odbio debatu jer
nema vremena, a i ne raspravlja s ekstremistima. Unatoč tome javio se
netko treći i preuzeo izazov pa izgleda da će doći do debate, iako s
drugačijom motivacijom. Dogovor debate je u tijeku i javit ću detalje
ovdje na blogu.

**Update:** In the same polite manner Mario has declined the
challenge saying he has no time and that he won't discuss with
extremists. Despite that someone else has spoken and accepted the
challenge so it looks like there will be a debate, but with different
kind of motivation than the first time. Stay tuned, I'll keep you
informed via this blog.

Nedavno se na mailing listi jedne studentske volonterske udruge čiji
sam bio član nekoliko godina razvila rasprava o slobodnom
softveru. Mario Bošnjak, također nekadašnji član, odgovorio je uvredom
prema meni. Kako bi raščistili račune, pozivam ga na debatu koju
možemo, nadam se, organizirati na FER-u u Zagrebu. U nastavku je mail
upućen njemu i s BEST-ovom ležernom listom u CC-u.


> Bok Mario!

> U nedavnom mailu na BEST-ovoj ležernoj listi [1] donio si neke
> ocjene o slobodnom softveru i povrh svega mene osobno poprilično
> uvrijedio. Šokantno mi je da alumni BEST-a i akademski obrazovan
> građanin postupi tako kako si ti postupio.

> U spomenutoj raspravi si moja stajališta o slobodnom softveru
> poistovjetio s "potpunim open sourceom", besplatnim softverom za
> sve, bez ograničenja i bez vlasničkih prava. Zagovornici "open
> sourcea" imaju sasvim drugačiji pristup od zagovornika slobodnog
> softvera, slobodan softver nije stvar cijene, nije bez ograničenja i
> itekako se oslanja na autorska prava.

> Obzirom da si akademski obrazovan i da ću ja to, nadam se, uskoro
> postati, mislim da je razumno pretpostaviti da obojica imamo
> određenu razinu uljuđenosti ili jednostavno, da se možemo ponašati
> kao gospoda. Da stvar ne bi ostala na uvredi koju si mi uputio,
> predlažem da to riješimo na gospodski način.

> Izazivam te na javnu debatu "Slobodan ili vlasnički
> softver?". Odredit ćemo mjesto i vrijeme, npr. rezervirat ćemo
> prostoriju na FER-u za određen termin.

> Vjerujem da ti neće biti problem odazvati se i pobijediti "jednog
> inženjera" koji je "očito bez iskustva i šire perspektive na svijet
> IT-a i ekonomije" i obzirom da "ovakvu spiku" možeš očekivati "od
> nekog školarca ili mladog studoša".

> Moja stajališta o slobodnom softveru nazvao si "retardiranim" i
> izjednačio ih s ekstremnom ljevicom, ekstremnom desnicom, fanatičnim
> vjernikom, "ukratko, ekstrem i fanatizam, bez veze s mozgom". Kako
> se radi o gospodskoj debati, vjerujem da nema razloga da se čega
> bojiš i da neće biti problem pobijediti nekoga tko nema "veze s
> mozgom".

> Da je "ovakva spika", kako nazivaš moja stajališta, naširoko
> "uvaljena" mnogima govori i, ako ćemo o statistici, činjenica o
> raširenosti slobodnog softvera. Slobodan softver pokreće gotovo sva
> od 500 najjačih super-računala u svijetu, baza je mnogim uslugama na
> Internetu kao što su Facebook, YouTube, Twitter, Googleova
> tražilica, GMail i mnogim drugim Googleovim uslugama. Slobodan
> softver pokreće stotine tisuća poslužitelja koji pružaju te
> usluge. Također je baza jednom od najraširenijih platformi za
> "pametne" mobitele --- Android.

> Hajmo se susresti i razriješiti spomenutu uvredu kroz debatu o
> vlasničkom i slobodnom softveru, pošteno i s najboljim namjerama.

> Ako pak uz astronomski male šanse ja pobijedim u toj debati,
> zahtijevam ispriku meni i svima koji dijele ta, kako kažeš,
> "retardirana" stajališta o slobodnom softveru i to na mjestu debate
> i na BEST-ovoj ležernoj listi (adresa liste o kojoj pričam je:
> best_zagreb_leisure@googlegroups.com).

> Ispriku bez poštene debate ne prihvaćam.

> Odgovor u kojem ćeš se odazvati ili odbiti izazov smatram važećim
> jedino na spomenutoj BEST-ovoj ležernoj listi u odgovoru na poziv na
> debatu.

> Najlakše je nekoga uvrijediti i onda pobjeći. Takve ljude narod
> doživljava kao kukavice i osobe bez integriteta. Nadam se da to nije
> slučaj kod tebe.

> U međuvremenu možeš više pročitati o slobodnom softveru na linkovima
> koje sam dao u svom prvom mailu na ovu temu. Također možeš podržati
> kampanje organizacija koje promiču slobodan softver,
> npr. http://www.fsf.org/campaigns/ i
> http://fsfe.org/projects/work.en.html.

> U iščekivanju tvog odaziva na debatu,

> Dim

> P.S. Ovaj poziv na debatu je javno objavljen na webu na adresi:
> http://akuzativ.wordpress.com/2010/10/14/poziv-na-debatu-debate-challenge/

> [1] http://groups.google.hr/group/best_zagreb_leisure/msg/1826ca7a01b5ae8c?dmode=source&output=gplain



**English version**

Recently there was a debate about free software on a mailing list of
one particular NGO, which is student and volunteers' organisation by
the way, and I was a member of that NGO for a few years. Mario
Bošnjak, who is also an ex-member, replied to my email with insults
directed towards me. In order to make it even, I'm challenging him to
a debate which will be hopefully hosted by Faculty of Electrical
Engineering and Computing (FER) in Zagreb, Croatia. You can find
translation of my email sent to him with BEST leisure list in CC
below.


> Hello Mario!

> In a recent mail at BEST leisure list [1] you've made some judgement
> about free software and, most of all, severly insulted me. It's
> shocking to me that BEST alumnae and citizen with faculty degree can
> do such thing.

> In a aforementioned discussion, you've said my personal views are
> equal to "complete open source", gratis software for everyone,
> without any limits and without ownership rights. Advocates of "open
> source" have completely different approach than free software
> advocates, free software is not about price, it doesn't come without
> any limits and it heavily relies upon copyright.

> Since you have faculty degree and I hope I'll have it soon too, I
> find it reasonable to suppose both of us can be decent, i.e. we can
> behave like gentlemen do. So, in order not to leave this assault in
> status quo, I suggest we settle it in a way decent souls do.

> I challenge you to the public debate "Free software vs. proprietary
> software". We will arrange place and time; for example, we could ask
> FER to host the event.

> I believe it won't be a problem for you to accept it and win "one
> engineer" who is "obviously without any experience and broader
> perspective over an IT and economy world", and since "this kind of
> buzz" you expect from "some pupil or a young student".

> You've called my views about free software "retarded" and have made
> it equal to extreme left-wing, extreme right-wing, fanatic believer,
> "in short, extreme and bigotry, unrelated to the mind". Because this
> is a gentlemen's debate, I'm sure there is nothing to be feared
> about, and it won't be a problem for you to win someone who is
> "unrelated to the mind".

> That many have been "set up" with "this kind of buzz", as you've put
> it, speaks the statistical fact about how widespread free software
> is. Free software runs almost all of the world's top 500
> supercomputers, it's a foundation for many Internet services such as
> Facebook, YouTube, Twitter, Google Search, GMail, and many other
> Google services. Free software runs hundreds of thousands of
> comupter servers that deliver these services. It's a foundation for
> one of the most popular smartphone platforms, i.e. Android.

> Let's meet and address aforementioned assault through the debate
> about proprietary and free software with honesty and good faith.

> If I win the debate, no matter how small and piteous chances I
> stand, I demand you to apologise to me and to all of the people
> with, as you call it, "retarded" views about free software, at the
> debate event, and on the BEST leisure mailing list (mailing list
> address I'm talking about is best_zagreb_leisure@googlegroups.com).

> I won't accept apology without a decent debate.

> You can reply with confirmation or denial on this challenge here at
> the BEST leisure list in a thread I'm challenging you. It's the only
> place I will find valid to look for your reply.

> It's rather easy to insult someone and then run away. People find
> such person cowardish and as one without personal integrity. I hope
> you're not that kind of person.

> Meanwhile, you can read more about free software on the web pages
> I've provided links to in my first mail on this topic. You can also
> support free software campaigns, such as
> http://www.fsf.org/campaigns/ and
> http://fsfe.org/projects/work.en.html.

> In an anticipation for your reply about challenge,

> Dim

> P.S. This challenge to a debate has been published on the web at:
> http://akuzativ.wordpress.com/2010/10/14/poziv-na-debatu-debate-challenge/

> [1] http://groups.google.hr/group/best_zagreb_leisure/msg/1826ca7a01b5ae8c?dmode=source&output=gplain
