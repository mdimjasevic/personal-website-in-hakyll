---
author: Marko Dimjašević
comments: false
date: 2010-01-19 14:31:49+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/01/19/umrezimo-se-na-mikro-razini/
slug: umrezimo-se-na-mikro-razini
title: "Umrežimo se na mikro-razini!"
wordpress_id: 106
tags:
- Twitter
---


Tamo negdje 1992. kada u Hrvata još nije bilo Interneta i u počecima
šireg korištenja e-maila postojale su tvrtke koje su nudile uslugu
e-maila, no tu je bila jedna caka --- mailove ste mogli slati samo
unutar iste domene, odnosno samo onima koji su imali e-mail pri istoj
toj tvrtki! Zamislite, imate adresu ivan.horvat@twitt.er i mailove vam
je dozvoljeno slati samo onima kojima adresa također završava na
@twitt.er! Poprilično ograničavajuće, zar ne? Nagađam zašto je bilo
takvih pokušaja --- tvrtka je htjela zaraditi na toj usluzi, no imala
je loš poslovni model. Taj se model očito bazirao na ograničavanju
korisnika u komunikaciji, onome čemu je ta usluga zapravo trebala
služiti. To se zove [zaključavanje
korisnika](http://en.wikipedia.org/wiki/Vendor_lock-in) na jednog
proizvođača. Danas kad se na takvu ideju s uslugom e-maila možemo samo
nasmijati i pitati se kako je netko mogao imati tako glupe ideje jer
je to očito loše za korisnike i na sreću stvar prošlosti, svjedočimo
istovrsnom potezu u domeni društvenih mreža na Internetu. Još gore,
mnogi toga nisu ni svjesni pa se shodno tome i ne smiju. I zaista,
nije za smijati se, već je, dapače, situacija vrlo zabrinjavajuća.

Prije četiri godine rijetko kome bi blog od najviše 140 znakova zvučao
smisleno. Pa što pobogu možeš napisati u 140 znakova, bila bi tipična
reakcija. Danas je imati mikroblog vrlo popularno i rijetko tko tko je
"mrežno osviješten" nema mikroblog. Godine 2006. nastao je Twitter,
servis za pisanje kratkih status-poruka u duljini od najviše 140
znakova što se još zove mikroblog. Twitter je štošta promijenio,
posebice u domeni izvještavanja. Tako ste o raznim velikim nesrećama
mogli čitati na Twitteru prije negoli koja velika medijska kuća
izvijesti o istom tom događaju. Taj bi Twitter bio izvrstan servis da
nema ovdje zanimljivu manu, a to je, sa stajališta korisnika, loš
poslovni model analogan onome s uslugom e-maila s početka priče iz
kojeg proizlazi veliko ograničenje u funkcionalnosti, a to je
nemogućnost povezivanja s korisnicima koji su registrirani pri drugim
pružateljima mikroblogova. Tvrtki Twitter Inc. stvaranje ili usvajanje
sada postojećeg standarda za povezivanje s drugim servisima
mikroblogova ni nije u interesu jer bi time, naravno, otvorila priliku
za druge da pokrenu istovrsni servis i sebi smanjila broj mušterija
zbog čega bi bilanca bila mršavija. Neusvajanje standarda je sa
stajališta jednog poduzetnika u još neiskorištenom tržištu sasvim
razumljivo, a takvo se ponašanje, u vremenu kad je maksimizacija
profita jedina misao vodilja u poslovanju tvrtke, prihvaća i odobrava.

Trenutno stanje u svijetu mikroblogova može se ovako vizualizirati:

![](mikroblogovi.png){ width=80% }

Dakle, ako netko ima račun na Twitteru i poželi se povezati s nekime s
drugog servisa mikroblogova, to, nažalost, neće biti u mogućnosti
napraviti. To je ionako svejedno kad su skoro svi na Twitteru,
pomislit ćete. Primijenimo li takvu logiku na etničke manjine, Srbi su
u Hrvatskoj sasvim nebitni jer su Hrvati ionako većinski
narod. Naravno, takvo bi razmišljanje bilo na štetu i Srba i Hrvata u
RH, a isto vrijedi za povezivanje ljudi s različitih servisa
mikroblogova. Servis koji razdvaja, umjesto da spaja, no opet
najpopularniji u svijetu mikroblogova. Zašto je tome tako?

Pogledate li na Wikipediji [usporedbu servisa
mikroblogova](http://en.wikipedia.org/wiki/Comparison_of_micro-blogging_services)
i izdvojite li [Identi.cu](http://identi.ca/) koja jedina navedena
podržava spajanje s drugim servisima mikroblogova (stupac
[OpenMicroBlogging](http://en.wikipedia.org/wiki/OpenMicroBlogging)) i
Twitter koji ne podržava, uočit ćete da Twitter nije popularniji
zahvaljujući tehničkoj nadmoći u pogledu mogućnosti koje nudi jer ju
niti nema, već je, dapače, Identi.ca superiornija. Tada opravdanje za
takvu popularnost Twitter može naći jedino u krajnjim
korisnicima. Zašto korisnici preferiraju Twitter?

Kad si već na Twitteru i povezao si se s određenim brojem ljudi teško
je preći na drugi servis za mikroblogove. Kod Twittera postoji
spomenuti problem zaključavanja korisnika. Prelazak u drugi servis bi
značio stvaranje novog korisničkog računa, ponovno povezivanje s
drugima, no kako su skoro svi na Twitteru, ali ne i na drugim
servisima mikroblogova, takvih bi bilo vrlo malo. Uostalom, navike je
teško mijenjati. Makar postoji nešto bolje, često nismo voljni to
odabrati jer smo se navikli na ono drugo. Ako netko pak nema račun na
Twitteru, a razmišlja o otvaranju mikrobloga, u neznanju postojanja
zaključavanja korisnika ili usred ignoriranja istog otvorit će ga
upravo na Twitteru jer su svi tamo. Kako rijetko tko pomišlja ili
brine o zaključavanju korisnika dok svoj pristanak na [uvjete
korištenja Twittera](https://twitter.com/tos) daje klikom na "Create
my account", stvara se začaran krug.

I ja imam mikroblog --- na [Identi.ci](http://identi.ca/mdim). Na
Twitteru ni na bilo kojoj drugoj mreži koja zaključava korisnike ne
želim imati korisnički račun. Uostalom, ako je netko od mojih
prijatelja na nekoj drugoj mreži (koja podržava OpenMicroBlogging), s
njim se mogu lako povezati. Budući da je šansa da si ti na Twitteru i
samo tamo vrlo velika, bilo bi mi drago da, sada kad znaš malo više o
svijetu mikroblogova i zaključavanju korisnika, otvoriš korisnički
račun na nekom od servisa mikroblogova koji podržava spajanje s drugim
istovrsnim mrežama[^serveri1][^serveri2]. Na Identi.ci ti olakšavaju
prijelaz tako što ti omogućavaju istovremeno slanje poruka i na
Twitter.

Nije da se ne usuđujemo jer je teško, već je teško jer se ne
usuđujemo, kaže Seneka. Hoćemo li se za desetak godina smijati
današnjem Twitteru kao e-mailu s početka priče? Ovisi o tebi!

[^serveri1]: [http://status.net/wiki/ListOfServers](http://status.net/wiki/ListOfServers)
[^serveri2]: [http://github.com/voitto/openmicroblogger/wikis/servers](http://github.com/voitto/openmicroblogger/wikis/servers)
