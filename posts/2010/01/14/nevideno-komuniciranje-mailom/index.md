---
author: Marko Dimjašević
comments: false
date: 2010-01-14 22:22:45+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/01/14/nevideno-komuniciranje-mailom/
slug: nevideno-komuniciranje-mailom
title: "Neviđeno komuniciranje mailom"
wordpress_id: 455
---

Elektronska pošta ili mail --- kako je ustaljeno u žargonu --- postao
je dio svakodnevice. Mailom se dopisujemo s prijateljima,
nastavnicima, profesorima, koristimo ga u radu udruge, tvrtke ili koje
druge organizacije, primamo razna izvješća, naloge, a mail ne bi bio
mail da s vremena na vrijeme ne kapne i koja forwarduša. Mail je
jednostavno primiti, jednostavno ga je i poslati. Da bi takav sustav
dopisivanja funkcionirao, moramo se oslanjati na brojne pružatelje
takve usluge, odnosno kod jednog ili čak više njih imamo svoj
elektronski poštanski pretinac u kojem se pohranjuju naši mailovi ---
oni koji stižu na našu adresu, no i oni koje smo poslali. Da stvar
bude slađa, takva pošta je najčešće besplatna usluga --- ne morate
plaćati markicu koju za vas zalijepi vaš pružatelj usluge maila. Ako
ste kojim slučajem iz znatiželje ili potrebe potražili informacije o
tome kako funkcionira mail, vjerojatno ste uočili da nije sve baš tako
bajno.

Jedan od osnovnih problema leži u tome što možete falsificirati mail,
odnosno poslati nekome mail i predstaviti se kao neka treća osoba. To
se [nedavno i
navodno](http://www.index.hr/vijesti/clanak/milinovic-lazni-mail-potpore-sanaderu-je-jos-jedan-stupidni-pokusaj-narusavanja-odnosa-u-hdzu/469309.aspx),
točnije 09.01.2010., dogodilo vrlom nam ministru Darku Milinoviću ---
netko je, predstavljajući se kao on, odnosno njegovim imenom,
prezimenom i e-adresom, poslao mail u nekoliko medijskih kuća s
isprikom bivšem premijeru Ivi Sanaderu. O samom sadržaju tog maila ne
želim ovom prilikom, već o mogućnosti da falsificirate mail. Za takav
podvig nisu potrebne hakerske vještine, već je to, dapače, vrlo lako
učiniti. Sve što trebate je podesiti program u kojem pišete mail da
stoji da ste vi, recimo, Darko Milinović s e-adresom
darko.milinovic@vlada.hr.

Možda ste već zaključili da se takvo što zasigurno ne može dobiti u
tradicionalnoj pošti, odnosno pismo uvijek možete potpisati i potpis
se uvijek može provjeriti. Što je s mailovima? Možda se pitate: "Na
kraju maila uvijek napišem svoje ime i prezime, zar nije to dovoljno
da primatelj bude siguran da sam mail zaista ja poslao?" i odgovor je
--- ne --- jer svatko može utipkati vaše ime i prezime. Tko zna, možda
ste i vi koji puta dobili falsificirani mail, a da toga niste ni
svjesni. Pa tko bi meni ili u moje ime slao falsificirani mail? Ne
znam i nije me briga, no postoji način da se zaštitite od toga.

Zahvaljujući kriptografiji možemo potpisivati čak i mailove. Da je
vlada ozbiljnija što se tiče komunikacije mailom, svi u vladi bi imali
tzv. privatni i javni ključ kojim bi potpisivali, odnosno potvrđivali
svoj identitet, odnosno sebe kao pošiljatelja maila. Tada bi medijske
kuće koje su primile sporni mail vrlo lako provjerile je li taj mail
zaista poslao ministar ili se netko odlučio našaliti. Ovako je
potrebno provesti policijsku istragu da se utvrdi govori li ministar
istinu kad kaže da on nije poslao taj mail.

Zamislite da se spremate na putovanje u Ukrajinu i htjeli biste
rezervirati smještaj. Na Internetu nađete web od nekog hostela, prvi
puta čujete za njega i čini vam se kao dobar hostel. Šaljete mail
vlasniku hostela na e-adresu koju ste našli na njihovom webu, pitate
je li moguće rezervirati i dobivate potvrdan odgovor, no da je
potrebno unaprijed uplatiti pola iznosa od 150 eura na taj i taj
račun. Vrlo ste skeptični jer u Ukrajini niste nikad bili, niste
nikada upoznali tu osobu i ako želite biti u tom hostelu, unaprijed
trebate platiti nemalu svotu. Čak ne možete biti sigurni ni da je
pošiljatelj maila vlasnik kao ni u podatke o računu na koji trebate
izvršiti uplatu jer iz uvoda u ovu priču znate da je mail vrlo lako
falsificirati. Čini li vam se sada potreba za digitalnim potpisivanjem
maila bitnijom? I mislio sam da da.

Digitalni potpis je dodatak mailu u obliku jedinstvenog niza znakova,
a koji je određen privatnim ključem pošiljatelja i sadržajem
poruke. Primatelj se u vjerodostojnost maila može uvjeriti pomoću
javnog ključa pošiljatelja koji je dostupan na nekom od javnih
poslužitelja ključeva. Svatko si može stvoriti takav par privatnog i
javnog ključa. Privatni ključ zadržite za sebe, a javni ključ
postavite na neki od poslužitelja. Za pravne osobe je poželjno da to
rade kod ovlaštenih tijela.

Da bi stvar bila još bolja, moguće je komunicirati mailom tako da samo
vi i sugovornik znate koji je sadržaj vaših poruka, a da pritom
zadržite autentičnost, integritet i neporecivost poruka. U tom slučaju
se radi o digitalnom pečatu. Takve poruke su kriptirane, odnosno
nečitke za sve osim za dvije osobe koje komuniciraju.

Danas sam odlučio ponovno koristiti digitalni potpis i digitalni pečat
pa sam stavio svoj javni ključ na [MIT-ov poslužitelj za javne
ključeve](http://pgp.mit.edu:11371/pks/lookup?search=Dimjasevic&op=vindex). Ako
želite sigurno komunicirati sa mnom putem maila, sad imate moj javni
ključ.

Ukoliko sam vas zainteresirao i pitate se kako stvoriti privatni i
javni ključ, pogledajte upute na
[GnuPG.org](http://www.gnupg.org/documentation/howtos.en.html).

Na kraju ću vas možda i razočarati --- ako ste se zainteresirali i
poželjeli unaprijediti svoju komunikaciju mailom, a za čitanje i
pisanje maila koristite web-sučelje, npr. GMail, zaboravite! Da biste
digitalno potpisali mail kroz web-sučelje, svoj privatni ključ morali
biste staviti na raspolaganje samom poslužitelju, odnosno Google, vaš
pružatelj internetskih usluga ili tko već, ovisno gdje vam je smješten
elektronski poštanski pretinac, imao bi vaš privatni ključ i mogao bi
mailove slati u vaše ime. Moguće da to ne biste htjeli. Možda ne biste
htjeli ni da drugi mogu čitati vašu poštu. No vi nemate što skrivati,
zar ne? Postoji razlika između tajnosti i privatnosti, no o tome nekom
drugom prilikom.

*Ispravak:* Cimer je pročitao ovaj članak pa me ispravlja --- nije
točno da ne možete koristiti web-sučelje za mail i istovremeno
digitalno potpisivati, kriptirati i dekriptirati sadržaj. Naime, za
Firefox, sve popularniji primjerak slobodnog softvera koji je web
preglednik, postoji dodatak [FireGPG](http://getfiregpg.org/) kojeg
možete skinuti i s [Mozillinih
stranica](https://addons.mozilla.org/en-US/firefox/addon/4645) pomoću
kojeg možete raditi razne operacije GnuPG-a, odnosno možete
potpisivati, kriptirati, dekriptirati i dr. bilo koji dio web
stranice, a posebna podrška postoji i za GMail. Ako i koristite
web-sučelje za čitanje maila, to vjerojatno radite jer želite biti
mobilni. Ako to želite zadržati i koristiti GnuPG, trebali biste sa
sobom nositi sve kriptografske ključeve i na svakom računalu za koje
sjednete trebao bi se nalaziti Firefox s dotičnim dodatkom za
GnuPG. Ako u to ne možete biti sigurni, onda možda i nije najpametnije
kriptirati mailove :)
