---
author: Marko Dimjašević
comments: false
date: 2010-01-15 21:25:49+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/01/15/rjesavanje-krize-na-haitiju-pomocu-openstreetmapa/
slug: rjesavanje-krize-na-haitiju-pomocu-openstreetmapa
title: "Rješavanje krize na Haitiju pomoću OpenStreetMapa"
wordpress_id: 456
categories:
- OpenStreetMap
tags:
- Haiti
---

![](haiti-pogoden-potresom-na-osm-u-stanje-15-01-2010.png){ width=100% }

Zasigurno ste već čuli za potres na Haitiju kao i za ogromne
posljedice. Za rješavanje krize koristi se i
[OpenStreetMap](http://openstreetmap.org), slobodna karta
svijeta. OpenStreetMap je vrlo fleksibilan sustav što potvrđuje i
uvođenje novih oznaka kako bi se pomoglo u lociranju spontanih kampova
preživjelih, oštećenih ključnih zgrada i infrastrukture. Kako to
trenutno izgleda možete vidjeti na
[http://zvenzzon.mine.nu:81/map.html](http://zvenzzon.mine.nu:81/map.html). Pogođeno
područje Haitija je do potresa bilo vrlo slabo kartirano, a sada je,
zahvaljujući volonterima koji označavaju i nekolicini vlasnika zračnih
fotografija tog područja koji su ih dali na korištenje, stanje bitno
drugačije. Skoro svake minute netko nadopuni kartu. [Spasilac iz
Kolumbije](http://www.mail-archive.com/talk@openstreetmap.org/msg26244.html)
zahvaljuje na tako ažurnim kartama:


> Mil gracias Fredy.

> Muy oportunos los mapas de Haiti que me envio para el Garmin. Ya los
> bajamos e instalamos en los GPS de nuestros equipos de busqueda y
> rescate.

> Sin duda OSM será una gran ayuda para nuestros grupos de respuesta,
> especialmente los que vamos a desplazar hacia zonas rurales.

> GRACIAS A LA FAMILIA OSM POR SU APOYO!

> Carlos Andres Barrera

> Sistema Nacional para la Prevencion y Atencion de Desastres

> Direccion de Gestion del Riesgo

> Mision Colombiana en Haiti


Na wikiju OpenStreetMapa možete pratiti [zbivanja na
Haitiju](http://wiki.openstreetmap.org/wiki/WikiProject_Haiti) vezana
uz OpenStreetMap.
