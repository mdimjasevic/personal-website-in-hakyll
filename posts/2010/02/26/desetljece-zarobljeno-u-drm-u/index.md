---
author: Marko Dimjašević
comments: false
date: 2010-02-26 17:52:56+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/02/26/desetljece-zarobljeno-u-drm-u/
slug: desetljece-zarobljeno-u-drm-u
title: "Desetljeće zarobljeno u DRM-u"
wordpress_id: 143
---

Nerijetko vidim prijatelje kako se vesele kupljenim tehnološkim
novotarijama poput mobitela, računala i raznih prijenosih uređaja za
reprodukciju multimedije koje su tako skupo platili. Vanjština tih
proizvoda blista, no što se krije unutra? I ja se smijem s njima,
točnije njima --- skupo su platili uređaj, softver, sadržaj ili
općenito proizvod u koji je ugrađeno ograničenje i udaljena kontrola
putem koje proizvođač kontrolira na koji način kupljeno možete
koristiti. Ne, ne radi se o rijetko viđenim proizvodima nepoznatih
tvrtki, već o vrlo velikim i poznatim poput Microsofta, Applea,
Sonyja, Amazona i drugih.

Radi se o
[DRM](http://en.wikipedia.org/wiki/Digital_rights_management)-u što je
izvorno skraćeno od Digital Rights Management, no kako je namjera te
tehnologije ograničiti nas korisnike, prikladniji je naziv Digital
Restrictions Management. DRM može ograničiti korisnikov pristup
filmovima, knjigama, softveru i glazbi i općenito bilo kojem
digitalnom sadržaju. Tako proizvod koji ste platili zapravo ne
kontrolirate vi, već proizvođač ili distributer. Primjeri su [Sonyjevi
CD-i](http://www.boingboing.net/2005/11/14/sony_anticustomer_te.html),
[Microsoft Windows
Vista](http://www.cs.auckland.ac.nz/%7Epgut001/pubs/vista_cost.html),
Appleov [iPhone i App Store](http://fsf.org/iphone), Appleov [iTunes
Store](http://www.defectivebydesign.org/itunes-drm-free), Amazonov
[Kindle](http://pogue.blogs.nytimes.com/2009/07/17/some-e-books-are-more-equal-than-others/),
no potpuni popis je mnogo dulji. DefectiveByDesign.org se osvrnuo na
[proteklo desetljeće puno
DRM-a](http://www.defectivebydesign.org/decade-in-drm) i koje su
akcije poduzete za vraćanje kontrole korisnicima.


[![](drm_banners_125x125.jpg)](http://www.defectivebydesign.org/decade-in-drm)


Podržavam kampanju
[DefectiveByDesign.org](http://defectivebydesign.org/) i pozivam i
tebe da se informiraš o čemu se radi. Smatram to bitnim za svakoga od
nas jer je tehnologija uronjena u svakodnevnicu ljudi, naročito onih
mojih godina i mlađih. Dana 4. svibnja 2010. bit će _Dan protiv DRM-a_
kako bi se šira javnost osvijestila o velikim problemima koje uzrokuje
DRM. I ti možeš podržati ideju tako da npr. staviš ovu sliku i
poveznicu na web DBD-a na svoju web stranicu, blog i sl.
