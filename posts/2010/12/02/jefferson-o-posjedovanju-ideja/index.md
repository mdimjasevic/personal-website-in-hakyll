---
author: Marko Dimjašević
comments: false
date: 2010-12-02 23:25:50+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/12/02/jefferson-o-posjedovanju-ideja/
slug: jefferson-o-posjedovanju-ideja
title: "Jefferson o posjedovanju ideja"
wordpress_id: 250
tags:
- free culture
- free software
- intelektualno vlasništvo
- Jefferson
- slobodan softver
- slobodna kultura
---


Slobodan softver i "intelektualno vlasništvo" ne idu skupa. Osim u
softver, "intelektualno vlasništvo" kao termin pokušava se useliti i u
kulturu općenito. Ukratko, to je termin koji predstavlja spoj
autorskog prava, patenata, tržišnih imena i još nekih manje važnih
stvari za softver i kulturu općenito. Kad netko koristi termin
intelektualno vlasništvo, pokazuje da ne želi pričati jasno o točno
određenoj stvari. Mogao je jasno pričati o autorskom pravu. Mogao je
jasno pričati o patentima. Mogao je jasno pričati o tržišnim
imenima. Umjesto toga je odabrao pričati o "intelektualnom
vlasništvu". Pretplatnici tog termina pokušavaju materijalne predmete
izjednačiti s idejama; kao da vlasništvo nad materijalnim predmetima
samo po sebi povlači vlasništvo nad idejama. Nisam pravnik, no oni
koji su više od mene upućeni u tu problematiku kažu da su te tri
stvari --- autorsko pravo, patenti, tržišna imena --- bitno različite
te da se zbog toga ne mogu svrstavati pod jedan termin. Uostalom, za
svaku od tih tri stvari postoji zasebni državni zakon. Vjerojatno s
razlogom.

Koliko je to promašeni termin i da ga treba odbaciti govori i Thomas
Jefferson u jednoj drugačijoj perspektivi. U pismu Isaacu
Mcphersonu, 13. kolovoza 1813., piše:


> Ako je priroda stvorila bilo koju stvar manje podatnom od drugih da
> bude isključivo vlasništvo, onda je to radnja misleće moći nazvana
> ideja, koju neki pojedinac može isključivo posjedovati dokle god je
> drži za sebe, ali u onom trenutku kada je objelodanjena, ona ulazi u
> svačiji posjed, a primatelj ne može sebe razvlastiti od nje. Njezino
> čudno svojstvo je i to što nitko ne posjeduje manje zato što svatko
> drugi posjeduje nju u cijelosti. Onaj tko primi neku ideju od mene,
> prima poduku a da ne umanjuje moju; kao i onaj tko zapali svoju
> svijeću pored moje, prima svjetlo a da mene ne zatamnjuje. Kada je
> stvorila ideje da se slobodno šire od jednoga na drugoga preko
> zemaljske kugle, za moralnu i uzajamnu poduku čovjeka, te
> poboljšanje njegovoga stanja, priroda ih je čudno i benevolentno
> napravila da se, poput vatre, mogu rasprostirati po cijelom
> prostoru, a da na nijednom mjestu ne gube svoju gustoću i da se,
> poput zraka u kojemu dišemo, ne mogu zatvoriti ili isključivo
> prisvajati. Stoga izumi ne mogu, po prirodi, biti predmet
> vlasništva.


Više o tome piše Lawrence Lessig u _Kôd i drugi zakoni kiberprostora_.		
