---
author: Marko Dimjašević
comments: false
date: 2010-12-13 10:27:12+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/12/13/debata-drustvo-treba-priznati-cetiri-slobode-kao-neotudivo-pravo-korisnika-racunala/
slug: debata-drustvo-treba-priznati-cetiri-slobode-kao-neotudivo-pravo-korisnika-racunala
title: "Debata \"Društvo treba priznati četiri slobode kao neotuđivo pravo korisnika računala\""
wordpress_id: 259
tags:
- debata
- free software
- slobodan softver
---


Prije 2 mjeseca objavio sam [poziv na
debatu](/2010/10/14/poziv-na-debatu-debate-challenge/) o
vlasničkom i [slobodnom
softveru](http://fsfe.org/about/basics/freesoftware.hr.html). U
međuvremenu je poziv odbijen, no prihvatio ga je netko kome poziv nije
ni bio upućen --- Stjepan Pavlek. U dogovoru sa Stjepanom, odlučili
smo proširiti debatu tako da ju čine timovi od po 3 člana. Teza debate
je: "Društvo treba priznati četiri slobode kao neotuđivo pravo
korisnika računala". Timove čine:
	
* Afirmacijski tim: Andrej Dundović, Matija Nalis i ja

	
* Negacijski tim: Stjepan Pavlek, Ivan Petrović i Marina Ulemek


Bit će prisutni suci, mahom iskusni u debatama. Za sada potvrđena
sutkinja je Kristina Gavran, nekad aktivna u debatnom klubu Sagito. U
potrazi smo za još 2 suca i nadamo se da ćemo ih pronaći do održavanja
debate. Vrlo moguće je da će publika imati ulogu suca. Za format
debate je odabran [Karl
Popper](http://en.wikipedia.org/wiki/Karl_Popper_debate#Karl_Popper_debate). Mislio
sam pomaknuti Đoletov koncert, no odustao sam i odlučio održati debatu
u isto vrijeme kad je njegov koncert --- debata će se održati u subotu
18.12.2010. [na
FER-u](http://www.openstreetmap.org/?lat=45.80072&lon=15.97166&zoom=17&layers=M)
u dvorani B4 s početkom u 20 sati. Ako kojim slučajem imaš kartu za
Đoleta, riješi je se i dođi na debatu! ;) U organizaciji debate
sudjeluje udruga [BEST](http://best.hr). U publici će, očekivano, biti
mnogo BEST-ovaca (nadam se i tada preobraćenih bivših Đoletovih
fanova), a kako je debata otvorena za javnost, svatko je dobrodošao.

Vidimo se na debati!		
