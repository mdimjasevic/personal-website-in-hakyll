---
author: Marko Dimjašević
comments: false
date: 2010-05-07 22:18:39+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2010/05/07/otvoreno-na-zatvoreni-nacin/
slug: otvoreno-na-zatvoreni-nacin
title: "Otvoreno na zatvoreni način"
wordpress_id: 187
tags:
- dorscluc
- hropen
- hulk
- identica
- slobodan softver
- statusnet
- Twitter
---

Ove srijede bio sam na [DORS/CLUC-u](http://www.open.hr/dc2010/),
odnosno Danima otvorenih računarskih sustava/Hrvatskoj konvenciji
korisnika Linuxa. Kako sami organizatori kažu, radi se o "središnjoj i
najstarijoj hrvatskoj manifestaciji iz područja otvorenih sustava,
operacijskog sustava GNU/Linux, normi u informatici i Open Source
filozofije". Konvenciju su organizirali [Hrvatska udruga Linux
korisnika](http://www.linux.hr/) i [Hrvatska udruga za otvorene
sustave i Internet](http://www.open.hr/). Potonje organzacije sam čak
i sam član, no iz nekoliko razloga nebitnih za ovu priču na konvenciji
sam bio samo sudionik.

Prvi dan konvencije je jedini dan kada sam bio sudionik i zaista je
bila zanimljiva što po izlaganjima što za druženje s ostalim
sudionicima i organizatorima, iako sam zbog drugih obveza morao otići
usred [programa](http://www.open.hr/dc2010/program.php). Ovakvo
okupljanje je nešto što zaljubljenici u slobodan softver, softver
otvorenog koda, otvorene standarde i slobodnu kulturu jedva čekaju i
za nas je sudjelovanje na tome pravi užitak tim više što takvog
sadržaja inače ima vrlo malo. Međutim, kada sam došao na konvenciju,
ušao u dvoranu i kao svaki pravi prisutni _geek_ izvadio prijenosno
računalo i zakačio se na Net, uočio sam dodatno platno na zidu pri dnu
dvorane na kojem se odvijalo nešto za mene nepojmljivo --- projekcija
aktualnih poruka sudionika s isključivo Twittera.  Pomislit ćeš ---
_što fali Twitteru?  Pa to je jedan od najpopularnijih servisa na
Internetu i skoro svi ga koriste_. Problem je u tome što je to
centralizirani vlasnički servis (o čemu sam prije
[pisao](/2010/01/19/umrezimo-se-na-mikro-razini/)) i njega se
koristi na konvenciji gdje su teme vezane isključivo za slobodne i
otvorene stvari u softveru, a istovremeno postoji slobodan softver i
slobodan servis koji služi istoj svrsi i po kvaliteti nimalo ne
zaostaje za spomenutim vlasničkim servisom; dapače, usudio bih se reći
da je kvalitetniji.

To što se taj vlasnički servis ne izvodi na
korisničkom računalu pri čemu bi netko mogao pomisliti da je tada sve
u redu jer se na njegovom računalu ne izvodi nikakav vlasnički softver
ne umanjuje problem. Korisnik tog servisa i dalje je u milosti
vlasnika servisa; drugim riječima, korisnik ne može utjecati na to
hoće li sutra servis biti dostupan, što vlasnik servisa radi s
njegovim podacima, hoće li i kada pogreške i nedostatci u servisu biti
uklonjeni itd. Twitter je dodatno i centraliziran što znači da je
nekompatibilan s drugim postojećim servisima za mikroblogove, odnosno
korisnicima Twittera uskraćeno je povezivanje s korisnicima drugih
istovrsnih servisa.

Takva situacija na DORS/CLUC-u me neugodno iznenadila pa sam se ubrzo
spojio na IRC kanal #linux.hr na Freenodeu gdje su bili prisutni mnogi
sudionici DORS/CLUC-a i tamo sam
[upitao](http://www.linux.hr/chat/archive/index.php?d=2010-05-05#msg617005)
zašto se prenose samo poruke s Twittera, a ne i s
[Identi.ce](http://identi.ca), najpopularnijeg slobodnog servisa za
mikroblogove. [kost](http://identi.ca/kost), inače predsjednik
[HULK-a](http://www.linux.hr/), [objasnio mi
je](http://www.linux.hr/chat/archive/index.php?d=2010-05-05#msg617009)
da implementator aplikacije koja povlači poruke sa Twittera vjerojatno
nikad nije ni čuo za Identi.cu pa da zato nije ugradio podršku te da
je to implementirao u Moonlightu. Na to sam ja odlučio biti
[ironičan](http://www.linux.hr/chat/archive/index.php?d=2010-05-05#msg617010),
a _kost_ je izjavio "Moonlight should be open" i pozvao me neka
napravim novu implementaciju i da će ju rado upotrijebiti.

Nažalost jednostavno i dostupno rješenje tada mi nije palo na pamet:
organizatori konvencije takvog tipa ne bi trebali poticati korištenje
vlasničkog servisa tokom konvencije o slobodnom softveru. Slobodni
softver koji služi istoj svrsi zove se [StatusNet](http://status.net/)
i za razliku od Twittera nudi decentraliziranost, a slobodni servis
koji koristi dotičan softver zove se [Identi.ca.](http://identi.ca/)
Organizatori su mogli iskoristiti postojeću Identi.cu ili otvoriti
novi servis na svojem poslužitelju i pozvati sudionike neka se
povežu. Prikaz aktualnih poruka o konvenciji koji se na Twitteru
filtrirao _hash-tagom_ _#dorscluc_ moguće je dobiti na vrlo
jednostavan način --- sve što treba učiniti je u web pregledniku, u
slučaju Identi.ce, otići na adresu
[http://identi.ca/tag/dorscluc](http://identi.ca/tag/dorscluc). Evo
koliko lijepo to može izgledati:

![Identi.ca - poruke s oznakom #dorscluc](identica-tag-dorscluc.png "Identi.ca - poruke s oznakom #dorscluc"){ width=100% }

Dakle, organizatori su mogli od početka npr. odbaciti Twitter, na
Identi.ci otvoriti grupu _dorscluc_ i pozvati sudionike koji su na
Identi.ci da ili koriste _hash-tag #dorscluc_ ili da šalju u grupu
[!dorscluc](http://identi.ca/group/dorscluc), a one koji su na nekom
drugom servisu (kad je riječ o decentraliziranim servisima koji
koriste standardne protokole komunikacije) da se jednostavno učlane u
grupu _!dorscluc_ na Identi.ci i tamo šalju poruke. U svakom slučaju
bi poruke svih sudionika bile prikazane na adresi
[http://identi.ca/tag/dorscluc](http://identi.ca/tag/dorscluc). Na taj
način se od sudionika ne bi očekivalo da koriste vlasnički servis i ne
bi se očekivalo da koriste točno određeni servis za
mikroblog. Dovoljno je koristiti onaj koji implementira [otvoreni
protokol](http://ostatus.org/).

Tokom drugog dana konvencije čak je i otvorena grupa _!dorscluc_ i
novi istoimeni korisnički račun na Identi.ci s
[porukom](http://identi.ca/notice/31239618):

> [!dorscluc](http://identi.ca/group/dorscluc)
> [!hulk](http://identi.ca/group/hulk) Due to popular demand, we're
> starting to use identi.ca as well

Osobno mi nije jasno koja je to velika potreba kad sam ja jedini pitao
zašto se ne koristi Identi.ca, a uz mene još je poruke na Identi.cu
slao samo [Florian Schießl](http://identi.ca/floschie). Nakon toga od
korisnika [dorscluc](http://identi.ca/dorscluc), kao ni od koga do
tada neviđenog, nije došla ni jedna nova poruka. Toliko o
potrebi. Istovremeno su na Twitter stizale daleko mnogobrojnije poruke
od, koliko sam grubo prebrojao, preko 30 različitih entiteta među
kojima je i _službeni dorscluc_:
[^1],[^2],[^3],[^4],[^5],[^6],[^7],[^8],
[^9],[^10],[^11],[^12],[^13],[^14],[^15],[^16],
[^17],[^18],[^19],[^20],[^21],[^22],[^23],[^24],
[^25],[^26],[^27],[^28],[^29],[^30],[^31],[^32],[^33].

Ova statistika jasno govori da
[FLOSS](http://en.wikipedia.org/wiki/FLOSS)-entuzijastima u Hrvatskoj,
točnije sudionicima na DORS/CLUC-u i nije toliko stalo do slobode,
standarda i otvorenosti kada se radi o softveru koji se izvršava
"negdje na Internetu". U toj domeni entuzijasti su samo za
besplatno. No možda i griješim. Možda u tom vlasničkom servisu ima
nešto otvoreno, a ne postoji u Identi.ci, samo to do sada nisam
dokučio. To je otvoreno na vlasnički način ili još bolje --- otvoreno
na zatvoreni način. Tko zna, možda se na godinu ipak izbjegnu ovakvi
"oblici otvorenosti" na _središnjoj i najstarijoj hrvatskoj
manifestaciji iz područja otvorenih sustava_.

[^1]: [http://twitter.com/AloneWolfCRO](http://twitter.com/AloneWolfCRO)
[^2]: [http://twitter.com/billyg03](http://twitter.com/billyg03)
[^3]: [http://twitter.com/burgulgoth](http://twitter.com/burgulgoth)
[^4]: [http://twitter.com/carr_](http://twitter.com/carr_)
[^5]: [http://twitter.com/democratie20](http://twitter.com/democratie20)
[^6]: [http://twitter.com/dorscluc](http://twitter.com/dorscluc)
[^7]: [http://twitter.com/dpavlin](http://twitter.com/dpavlin)
[^8]: [http://twitter.com/ivanbrezakbrkan](http://twitter.com/ivanbrezakbrkan)
[^9]: [http://twitter.com/ivan_gustin](http://twitter.com/ivan_gustin)
[^10]: [http://twitter.com/ivoras](http://twitter.com/ivoras)
[^11]: [http://twitter.com/jaksabasic](http://twitter.com/jaksabasic)
[^12]: [http://twitter.com/k0st](http://twitter.com/k0st)
[^13]: [http://twitter.com/kicdu](http://twitter.com/kicdu)
[^14]: [http://twitter.com/kisasondi](http://twitter.com/kisasondi)
[^15]: [http://twitter.com/kristijanzimmer](http://twitter.com/kristijanzimmer)
[^16]: [http://twitter.com/linuxzasve](http://twitter.com/linuxzasve)
[^17]: [http://twitter.com/miceq](http://twitter.com/miceq)
[^18]: [http://twitter.com/mirpod](http://twitter.com/mirpod)
[^19]: [http://twitter.com/moondowner](http://twitter.com/moondowner)
[^20]: [http://twitter.com/mrak](http://twitter.com/mrak)
[^21]: [http://twitter.com/nikolaplejic](http://twitter.com/nikolaplejic)
[^22]: [http://twitter.com/nixa](http://twitter.com/nixa)
[^23]: [http://twitter.com/nvucinic](http://twitter.com/nvucinic)
[^24]: [http://twitter.com/oresk](http://twitter.com/oresk)
[^25]: [http://twitter.com/p0bailey](http://twitter.com/p0bailey)
[^26]: [http://twitter.com/robertbasic](http://twitter.com/robertbasic)
[^27]: [http://twitter.com/sokac](http://twitter.com/sokac)
[^28]: [http://twitter.com/unixland](http://twitter.com/unixland)
[^29]: [http://twitter.com/valentt](http://twitter.com/valentt)
[^30]: [http://twitter.com/vegyraupe](http://twitter.com/vegyraupe)
[^31]: [http://twitter.com/Ylodi](http://twitter.com/Ylodi)
[^32]: [http://twitter.com/zeljkofilipin](http://twitter.com/zeljkofilipin)
[^33]: [http://twitter.com/zriha](http://twitter.com/zriha)
