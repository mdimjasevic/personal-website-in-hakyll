---
author: Marko Dimjašević
comments: false
date: 2012-10-09 04:00:47+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2012/10/08/multidimenzionalna-selidba/
slug: multidimenzionalna-selidba
title: "Multidimenzionalna selidba"
wordpress_id: 409
---

Od kako sam zadnji puta pisao na blogu dogodila se koja veća promjena
pa slijede samo sažeci važnijih promjena jer bi bilo previše da o
svakoj pišem detaljno.

Najveća promjena je u tome što sam nekih 10 tisuća kilometara dalje
nego kada sam zadnji puta pisao --- točnije, više nisam u Križevcima,
već se od kasnog srpnja nalazim u Salt Lake Cityju. Selidba se
dogodila jer sam upisao doktorski studij na University of Utah,
točnije pri [School of Computing](http://www.cs.utah.edu/) (nisam
siguran želim li prevoditi naziv sveučilišta i naziv
fakulteta). Područje računarstva koje će mi uzrokovati brojne
glavobolje je verifikacija softvera. Zbog te selidbe preko bare
slijedi i koja promjena u vezi ovog bloga.

<!-- more -->

Blog selim s [akuzativ.wordpress.com](http://akuzativ.wordpress.com)
na svoju web-stranicu koja se nalazi na adresi
[http://dimjasevic.net/marko](http://dimjasevic.net/marko). Mnogo je
ugodnije i lakše, tehnički gledano, održavati nešto nad čime imaš punu
kontrolu. Nova stranica --- pa tako i novi blog --- u pozadini i dalje
koristi [WordPress](https://en.wikipedia.org/wiki/Wordpress), no sada
ga mogu podešavati do mile volje. Akuzativ će i dalje ostati online
radi postojećih poveznica koje upućuju na njega, no ovo je zadnja
objava na Akuzativu. Stoga, ako ste do sada čitali moja piskaranja i
želite tako nastaviti, uskladite svoje (RSS) pokazivače prema [novoj
adresi](http://dimjasevic.net/marko). Teme o kojima ću pisati samo ću
proširiti onima vezanim uz akademsku zajednicu, fakultet, studij i
istraživanje. Želim novi blog koristiti za suradnju s drugim
istraživačima u području formalne verifikacije i srodnih
područja. Blog vidim kao nešto što će mi u najmanju ruku pomoći u
pisanju znanstvenih članaka. Tehnički detalj koji mi je danas upao u
oko je WordPressova (ograničena)
[podrška](https://wordpress.org/extend/plugins/wp-latex/) za pisanje u
LaTeX-u (https://en.wikipedia.org/wiki/LaTeX) što sam odmah omogućio
na novom blogu --- tako će objave i komentari bez teškoća sadržavati
razne matematičke izraze koje je gotovo nemoguće ili vrlo teško
napisati na bilo koji drugi način. Kako će ti članci biti na
engleskom, više-manje sve daljnje objave na blogu će isto tako biti na
engleskom.

Nova web-stranica povezivat će razne servise koje koristim ili ću tek
početi koristiti na Internetu. Do sada sam uključio mikroblog na
Identi.ci, [galeriju multimedijalnog
sadržaja](http://mediagoblin.dimjasevic.net) [GNU
MediaGoblin](http://mediagoblin.org), kartu OpenStreetMap i pripadni
softver za geo-referenciranje raznog sadržaja, a kasnije ću dodati i
Gitorious. Veselim se svim tim selidbama i novostima koje donose!

Komentari su više nego dobrodošli!		
