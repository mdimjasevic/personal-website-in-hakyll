---
author: Marko Dimjašević
comments: false
date: 2012-10-31 11:45:12+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2012/10/31/support-free-as-in-freedom-media-infrastructure/
slug: support-free-as-in-freedom-media-infrastructure
title: "Support free as in freedom media infrastructure"
wordpress_id: 486
categories:
- free software
tags:
- gnu
- MediaGoblin
---

Have you heard of GNU MediaGoblin? To quote [Wikipedia](https://en.wikipedia.org/wiki/MediaGoblin):

> GNU MediaGoblin (also MediaGoblin) is a free web platform for
> hosting and sharing digital media, aimed to provide an extensible,
> adaptive, decentralized and freedom-respectful software alternative
> to major media publishing services such as Flickr, deviantArt,
> YouTube, etc.


With that one single sentence, everything is said. Therefore, it is
very important to have such a tool.

<!-- more -->

![](support_mediagoblin-blogbanner.png)

I've supported MediaGoblin by donating. I'm also using it to host [my
media](http://mediagoblin.dimjasevic.net). Once it gets federation
support, we'll all be able to share our media easily without sharing
it with corporations first and without the need to agree to all those
ever-changing disrespectful terms of service that nobody actually
reads.

To find out more about MediaGoblin and why it is crucial to have a
platform like MediaGoblin, watch the video for the [fund raising
campaign](http://www.mediagoblin.org/pages/campaign.html) the project
is running and read about it at the project
[homepage](http://www.mediagoblin.org/). Consider
[donating](http://www.mediagoblin.org/pages/campaign.html) and
spreading out the word! Put the banner on your homepage!
