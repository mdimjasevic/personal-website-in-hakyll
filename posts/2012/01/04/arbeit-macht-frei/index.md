---
author: Marko Dimjašević
comments: false
date: 2012-01-04 16:46:37+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2012/01/04/arbeit-macht-frei/
slug: arbeit-macht-frei
title: "Arbeit macht frei"
wordpress_id: 397
---

Bilo je ljeto 2004. kada sam dobio priliku da se prikopčam na državnu
sisu i upišem FER uz financijsku potporu Ministarstva znanosti,
obrazovanja i športa. Profesori su nama studentima prvih dana tepali
kako je FER dobar fakultet i cijenjen u industriji, ili bolje rečeno
za prilike u Hrvatskoj, u "industriji". Mislio sam: "Samo da završim
FER i na konju sam!" Bili su to dani ponosa i slave, dani maštanja o
svijetloj budućnosti koja me čeka nakon fakulteta. Kao da sam, skupa s
drugim brucošima FER-a, iščekivao odlazak u obećanu zemlju.


Sada je 2012. godina. FER sam završio još u listopadu 2010. Moja
diploma s FER-a ovog trena služi jedino u dekorativne svrhe. Odmah
priznajem --- dio krivice za to što sam trenutno nezaposlen sigurno
snosim i sam jer do sada nisam dao sve od sebe u traženju posla. U tih
nešto više od godinu dana od kako sam završio petogodišnji studij
računarstva brojim više dana van radnog odnosa, nego onih kada sam bio
u radnom odnosu. Koješta sam doživio u to vrijeme: radost što sam
dobio posao, iako van struke, no svejedno posao i to posao nastavnika
matematike u osnovnoj školi; razočaranje kada sam u ruke dobio prvu
platnu listu na kojoj je sumarni iznos bio oko 3700 kn (doduše, počeo
sam raditi 10. u mjesecu s početkom školskog polugodišta); veselje
koje nastavnika obuzme kada vidi da je đak usvojio gradivo; kao i
gorčinu kamilice koju osjetiš ispijajući čaj na razgovoru za posao
kada dobiješ ponudu s početnom plaćom od 2800 kn.

Naime, nedavno sam krenuo put Bjelovara zbog dogovorenog razgovora za
posao. Posao se nudio u informatičkoj tvrtci i može se reći da je
vezan uz moju struku jer se radi o radnom mjestu
programera. Potencijalnom poslodavcu iznio sam svoje kvalitete,
vještine i kvalifikacije, a on je meni rekao na kojim bih konkretnim
tehnologijama radio. Nisam se previše nadao pa me nije iznenadilo kada
sam čuo da se radi o vlasničkom softveru. S druge strane teško je
držati se ideala pa odbiti takvu ponudu jer kad se uzme u obzir
situacija u Hrvatskoj, rijetki su poslovi za programera koji se ne
bave vlasničkim softverom. Stoga, nekome tko je po struci računarac
odbiti razvijati i raditi na vlasničkom softveru više-manje znači
odreći se prihoda kojim bi se koliko-toliko osigurala egzistencija.


No čak i kada progutam ideale i pristanem na nemoralne radnje kao što
je razvijanje vlasničkog softvera, jednu stvar ne mogu nikako
zanemariti --- vrijeđanje od strane potencijalnog poslodavca. Da bih
pojasnio kako je došlo do uvrede na tom nedavnom razgovoru u
Bjelovaru, prvo moram zaboraviti skromnost (ako me je ikad pratila ta
vrlina) i reći nešto o sebi što ionako uglavnom napišem u životopisu.

Programiranjem se bavim od 4. razreda osnovne škole i od tada sam bio
na brojnim natjecanjima iz informatike. Na raznolikim natjecanjima na
državnoj razini osvojio sam 1., 2. i 3. mjesto. Mogli bismo reći ---
državni vrh. Mislim da bi to svakom ozbiljnom poslodavcu trebalo
dovoljno govoriti o mojoj sposobnosti i motivaciji za rad (Googleu to
dovoljno govori). Dodajte na to i završen diplomski studij računarstva
na FER-u i ono što dobijete na razgovoru za posao je ponuda od 2800
kn. Ako radiš kao smetlar ili čistačica --- ovdje nemam namjeru
podcjenjivati te hvale vrijedne poslove, već ih samo uzimam kao
primjer zbog stručne spreme koju zahtijevaju kao i zbog broja ljudi
koji su kvalificirani za taj posao --- zaradiš barem toliko, ako ne i
preko 3000 kn, čak i sa prvom plaćom. Dotični poslodavac je rekao da
je to plaća za početak, prvih mjesec, mjesec i pola dok se ne dokažem
u poslu, a opravdanje tako niskoj plaći nalazi u tome što se već mnogo
puta "opekao" na radnike koji ne znaju raditi svoj posao. Uopće ne
sumnjam u to da postoji barem jedan čovjek koji se javio u toj tvrtci
za posao programera, a da ga nije znao obavljati, no ako mu moje
reference ništa ne znače i smatra da moj rad kao programera vrijedi
jednako kao i rad čistačice --- ako ćemo rad vrednovati plaćom ---
neka mu je sa srećom sa radnicima koje toliko cijeni. Da bi stvar bila
bolja, uporno je tvrdio da je to minimalac s kojim si mogu pokriti
troškove. Ne znam tko danas u Hrvatskoj s 2800 kn plaće može normalno
pokriti troškove ako živi sam, a kud tek ako još nekoga uzdržava.


Tako sjedimo šef te tvrtke i ja na kavi, a mene pere teški
neskužitis. Kažem mu da je 2800 kn zaista premalo, čak i za prvu
plaću. Pitao sam ga da li bi njemu bila velika razlika da me plati
5000 kn što bi meni bila prihvatljiva plaća (jer bih s tim iznosom
mjesečno mogao "ostati na nuli"), na što je rekao da to ne bi bio
problem. I naravno da mu to ne bi bio problem, posebno kad se uzme u
obzir da je u tom razgovoru spomenuo da je tvrtci protekla 2011. bila
najbolja godina. Iako luđački, očekivao sam da će reći nešto u stilu:
"Ok, onda 5000 kn". Za naslutiti je da se to nije dogodilo, odnosno
stvar je ostala na samo jednoj, prvoj i zadnjoj ponudi. Kada sam došao
doma, bacio sam pogled na za ovu priču relevantne brojke Državnog
zavoda za statistiku. Prosječna mjesečna isplaćena neto plaća za
listopad 2011. iznosila je 5406 kn. Prosječna plaća visokoobrazovanih
sigurno je i veća. No eto, meni je ponuđeno jedva preko 50% prosječne
plaće svih radnika. Kao da sam bio na sjednici gradske skupštine u
Zagrebu, a ne na kavi zbog razgovora za posao pa mi se obraća sam
Bandić: "Tej kit ot liv it".


Ono što se nakon toga je dogodilo je da je šef natuknuo da već ima
kandidata koji su upoznati s tehnologijom u vezi koje zapošljava i da
su spremni početi s 2800 kn. To je očita igra na strah koja bi u
kandidata za posao trebala izazvati razmišljanje u smjeru: "Nezaposlen
sam i sad imam priliku raditi, iako za sitan novac. Postoje i drugi
nezaposleni, netko će već pristati na ovu plaću, a ja ću i dalje biti
bez posla i bez plaće. Možda mi je bolje da prihvatim ponudu." Nisam
se libio reći da je sasvim razumljivo da postoje ljudi koji će
prihvatiti takvu ponudu jer su ljudi bez posla očajni ljudi, pogotovo
u ovo vrijeme kada kriza trese Hrvatsku. Dok god postoji vojska
očajnih nezaposlenih ljudi, naravno da će se naći netko tko je spreman
raditi za još manji iznos. Uvijek se nađe netko tko je očajniji i od
tebe.


Volio bih da sam jedini koji traži posao i da nema očaja zbog
(ne)zaposlenosti. Nažalost, u jednom mjestu negdje na pola puta između
Križevaca i Bjelovara u proizvodnom pogonu rade vrlo očajni
ljudi. Kažem očajni jer se 60 sati teškog fizičkog rada tjedno,
odnosno oko 240 sati mjesečno na kraju mjeseca nagradi crkavicom od
1400 kn. Radnici se boje i svojem susjedu reći koliku imaju plaću. Za
platnu listu ne žele ni pitati jer bi to značilo da sumnjaju u šefa, a
to je ravno iskazanoj želji za otkazom. Šef im je usadio toliku
paranoju da nikad ne znaju tko ih sve može otkucati zbog nečasne
radnje kao što je odavanje iznosa plaće. A jednom kad si otkucan, znaš
da je otkucalo i tebi i tvom poslu u tvornici, a niti tvojoj djeci se
ne piše dobro. Šef im konstantno prijeti vojskom nezaposlenih koji kao
da udarcima navaljuju na tvorničku kapiju pa glasnim lavežom utjeruju
strah u kosti njima koji mogu biti sretni i zahvalni da za tako loše
obavljen posao dobiju i toliko koliko dobe. A kada u tvornicu dođe
državni inspektor i radnike pita imaju li osmosatno radno vrijeme,
pauzu za gablec i plaćene prekovremene, radnici pogledaju prema gazdi
koji s rukom na uhu kao da osluškuje lavež ispred tvornice, zatim
pogledaju nazad prema inspektoru, na lice nabace osmijeh i u isti glas
uzviknu: "Gospodine inspektore, sve je u najboljem redu!" Inspektor
potom zatvori knjižicu s evidencijom radnog vremena iz koje je svakom
pučkoškolcu jasno da radnici dolaze debelo prije početka i odlaze
debelo poslije završetka radnog vremena, ustane od stola, zadovoljno
stisne ruku šefu tvornice i pozdravi radnike: "Bog i Hrvati!"
