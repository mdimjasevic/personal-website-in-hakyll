---
author: Marko Dimjašević
comments: false
date: 2012-01-20 00:30:49+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2012/01/20/demokracija-i-suverenitet/
slug: demokracija-i-suverenitet
title: "Demokracija i suverenitet"
wordpress_id: 404
---

U nedjelju se u Hrvatskoj održava referendum o pripojenju Hrvatske
Europskoj uniji. Da mi građani baš i nemamo izbora ni alternative
(Thatcher: "There Is No Alternative") ovih nam dana do znanja daje
Vesna Pusić, ministrica Ministarstva vanjskih i europskih
poslova. Tako npr. kaže da će Hrvatskoj pasti kreditni rejting (kojeg
određuju strane privatne korporacije) ako se građani na referendumu
većinski ne izjasne za pripojenje Hrvatske EU-u. Istog boga koji ima
oblik privatne korporacije i zove se S&P
[naljutilo](http://http//www.businessinsider.com/sp-downgrades-europe-2012-1)
je nekoliko zemalja eurozone kada nisu učinile dovoljno kako bi
riješile ono što boga muči, a to su "neprekidni sustavni stresovi u
eurozoni". Nedavno je građanima Grčke uskraćen referendum o
prihvaćanju novčane "pomoći" jer bi demokratski proces kao što je
referendum mogao uznemiriti financijska tržišta.

Noam Chomsky, razmatrajući Bretton Woodsov sustav, dobro uočava kako
se radi o reduciranju suvereniteta u korist privatne moći:

<blockquote>

[S]lobodni protok novca stvara "virtualni parlament svjetskog kapitala
koji može staviti veto na vladinu politiku ako je smatra
nerazumnom". Vladina politika koja se smatra nerazumnom doticala bi se
radničkih prava, obrazovnih programa, zdravstva, pokušaja stimuliranja
ekonomije, zapravo bilo što što bi pomoglo ljudima, a ne profitu (i
zato je ona nerazumna u tehničkom smislu).

</blockquote>

To savršeno opisuje svaku od tri spomenute financijske ucjene.

<blockquote>"Republika Hrvatska jedinstvena je i nedjeljiva demokratska i socijalna država.  
U Republici Hrvatskoj vlast proizlazi iz naroda i pripada narodu kao zajednici slobodnih i  
ravnopravnih državljana.

Narod ostvaruje vlast izborom svojih predstavnika i neposrednim
odlučivanjem."

> ~ Članak 1. [Ustava Republike
Hrvatske](http://www.usud.hr/uploads/Redakcijski prociscen tekst
Ustava Republike Hrvatske, Ustavni sud Republike Hrvatske, 23. ozujka
2011.pdf) </blockquote>

  


		
