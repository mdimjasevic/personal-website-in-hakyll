---
author: Marko Dimjašević
comments: false
date: 2012-01-14 12:19:04+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2012/01/14/drugaciji-pristup-trazenju-posla/
slug: drugaciji-pristup-trazenju-posla
title: "Drugačiji pristup traženju posla"
wordpress_id: 401
---

Jednom kada počnete tražiti posao pa prolistate oglase za posao, ubrzo
shvatite da su gotovo svi oglasi napisani po istoj špranci -- kao na
pladnju nudi se vodeća tvrtka u svom području, stimulativna plaća,
fleksibilno radno vrijeme, prilika za napredovanjem i dinamična radna
okolina. Svi ti oglasi zvuče bajno. Odgovara li stvarnost toj
bajkovitoj slici?

Naišao sam na molbu za posao meni nepoznatog autora koja je odgovor na
takve oglase i današnjoj situaciji. Prenosim ju u cijelosti:

<blockquote>

Potpuno nespreman za rad pod pritiskom, kao i prekovremeni rad, osim
ako nije pošteno plaćeno, ne dozvoljavam da me netko iskorištava ili
da se iživljava na meni. Iskreno me zaboli za ciljeve vaše kompanije
ako plaćom ne mogu pokriti svoje tekuće troškove, plus da mi ostane za
malo garderobe, knjiga, putovanja, izlaske, sport i provod, što spada
u normalan život, a ne luksuz.

Funkcioniram po principu koliko-para-toliko-muzike. Također uopće me
ne zanima što ste u problemima, jer ja ne sudjelujem u dobiti u vašoj
kompaniji, već radim za plaću, i očekujem da izvršavate ugovorom i
zakonom predviđene obaveze, i to na vrijeme, kao i da poštujete
praznike i godišnji odmor. Baš kao što i od mene tražite da ja
izvršavam svoje obaveze i da budem na poslu radnim danom. Pa naravno
da ću vas napustiti čim nađem nešto bolje. Da ponovim - ja radim za
plaću, vi za dobit.

Komunikativan s pristojnim i ljubaznim svijetom, za drkadžije imam kratak fitilj.  
Psujem na nekoliko svjetskih jezika, ali ozbiljnu prepisku i razgovor
vodim samo na hrvatskom i engleskom, i ne preseravajte se s
multinacionalnim korporativnim tripom, jer vam je i to previše.

Studirao sam 100 godina, ali smatram da je uspjeh što sam uopće
završio bilo šta u ovoj zemlji, ovakvoj kakva je. Usavršavam se stalno
i bez vas, jer sam radoznao i mislim da život ima smisla i bez
umiranja na poslu.

Reagiram samo na pozitivnu stimulaciju. Ne gušite me sa testiranjima I
timbilding budalaštinama. Ako se već preseravate s tim zapadnjačkim
forama, onda ponudite i zapadnjačku plaću i uvjete rada. Šta ne znam,
naučit ću, nisam debil.

I da, postoje stvari koje bih u životu radio iz čistog entuzijazma,
besplatno, ali žalim što Vas moram obavijestiti da to nije posao koji
Vi nudite.

</blockquote>

		
