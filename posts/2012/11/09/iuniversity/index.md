---
author: Marko Dimjašević
comments: false
date: 2012-11-09 06:02:43+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2012/11/09/iuniversity/
slug: iuniversity
title: "iUniversity"
wordpress_id: 503
categories:
- University of Utah
tags:
- Apple
- defective by design
- educational pricing
- iTrash
- UofU
---


As you may already know, as of Fall 2012 I'm a student here at the
[University of
Utah](https://en.wikipedia.org/wiki/University_of_utah). I would say
it's a peculiar university, but I may be proven wrong (you can use the
comment section below the post to do that). The university keeps on
shocking me almost on a regular basis. It is not only a university
that has the Information Security and Privacy Office doublespeak-named
office, that lets the motion picture, music recording, and software
corporations keep under surveillance the University of Utah's computer
networks, and thereby invade staff, faculty and students' security and
privacy at the university; it will also provide a special shuttle bus
line that will take you from one part of the university campus to the
other where you can happily spend money on Apple-disabled
devices. These devices are a terrible purchase for a very obvious
reason: they're loaded with proprietary software, and you can't get
rid of all of the anti-features embedded in Apple's (not Apple)
devices. This must be a one-time offer, because it is not only offered
with the educational pricing as usual; this time it is tax-free as
well! I quote the email message that I received from the University
Campus Store, and that was approved for distribution by the
university's Interim Senior Vice President for Academic Affairs and
Interim Vice President for Human Resources:


<blockquote>

From: "University Campus Store" \
Subject: [UofU] Apple Tax-Free Day is November 20th (7:30 am to 7:00 pm) \
Date: Fri, 09 Nov 2012 17:03:04 -0700

Dear Faculty and Staff:

On Tuesday, November 20th, popular Apple products will be available tax-free
at the University Campus Store.  This is an opportunity for faculty and staff
to save on Apple products for their educational needs.

Limited to:

* iPad Wi-Fi Standard Models
* MacBook Pro 13" Models
* MacBook Pro 15" Retina Display Models
* iPods

The tax savings is in addition to the educational discount pricing
already provided by Apple to the University of Utah. The offer is
limited to product on hand.

Purchases will be eligible for the Campus Store's Employee Payroll
Deduction Program, as well as the new Tech Buyback
Program. Information on Tech Buyback is available at:
http://utech.refreshedbyencore.com.

For the convenience of faculty and staff located on upper campus, we
will be providing shuttle service every 15 minutes (11:00 am to 3:00
pm) between the Moran Eye Center and the Campus Store.

For more information about Apple Tax-Free Day, please call
801-581-4776.

</blockquote>


But maybe there is hope. If this waste bin at the campus represents
someone's opinion on what are the best
[Apple](http://www.defectivebydesign.org/taxonomy/term/20)'s products,
we have something to look forward to.

![](IMG_20120823_155205.jpg){ width=70% }
