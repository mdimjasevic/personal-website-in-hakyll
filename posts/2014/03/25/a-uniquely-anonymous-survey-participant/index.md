---
author: Marko Dimjašević
comments: false
date: 2014-03-25 17:48:19+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/03/25/a-uniquely-anonymous-survey-participant/
slug: a-uniquely-anonymous-survey-participant
title: "A uniquely anonymous survey participant"
wordpress_id: 34109
categories:
- University of Utah
---

[![War is peace](1984-war-is-peace-199x300.jpg "Illustration by Patrick Hoesly")](1984-war-is-peace.jpg)


Here at the University of Utah people like to use doublespeak. One example are
anonymous surveys sent out by email to students, which contain a per-student
unique identifier in their URLs. I have received such emails from the
university's International Center (these days it's called International
Student and Scholar Services). Disturbed by their claim that their surveys are
anonymous, while at the same time they provide per-student unique URLs for the
surveys, I have sent them a request to stop claiming such surveys are
anonymous, but I was simply ignored and no reply was sent back to me.


<!-- more -->


A more recent example is a survey from Associated Students of the
University of Utah (ASUU). They have sent me an email with a call to
provide them with feedback through a survey, which is on decreasing
textbook costs at the university. Again, the claim is that the survey
is anonymous, yet they provided me with a URL that raises a few
eyebrows when it comes to anonymity.


To make it absolutely clear what I am talking about, here is the email I've
received from ASUU:

<!-- > From: Student Body President <info baseline.mail.campuslabs.com> \ -->
<!-- > Reply-to: <rwootton asuu.utah.edu> \ -->
<!-- > To: u0808640 utah.edu \ -->
<!-- > Subject: Help us decrease student textbook costs \ -->
<!-- > Date: Mon, 17 Mar 2014 20:56:20 +0000 -->

<blockquote>

From: Student Body President <info baseline.mail.campuslabs.com> \
Reply-to: <rwootton asuu.utah.edu> \
To: u0808640 utah.edu \
Subject: Help us decrease student textbook costs \
Date: Mon, 17 Mar 2014 20:56:20 +0000

Hello University of Utah Students,

ASUU is working with many organizations on campus to decrease textbook
costs for students at the University of Utah. As students, we know
this is an important issue to many of you and would love your help in
figuring out the best ways to decrease student textbook expenses.
This survey is anonymous and should take just 3-5 minutes to
complete. Your feedback will be a critical part of our recommendations
for the University of Utah.

To access the survey please click here. If the survey does not open
automatically, please copy and paste the following link to your
internet browser's address bar:

http://studentvoice.com/p/?uuid=db996ddd07b943d8911312904917e1a9

Any additional feedback, questions, or suggestions may be sent to
Rachel Wootton, ASUU Director of Academic Affairs, at
rwootton@asuu.utah.edu.

Thank you so much for your help and have a great rest of your
semester!

Best, \
Sam Ortiz \
Student Body President

Distribution of this message was approved by Mary G. Parker, Associate
Vice President for Enrollment Management, University of Utah Phone
(801) 581-3490, 201 S. Presidents Circle, Room 206, Salt Lake City, UT
84112

</blockquote>


So, you can see my unique university identifier in my email address and a
suspicious anonymity link in the message. What raises even more eyebrows is a
follow-up email from ASUU titled "Reminder: Help us decrease student textbook
costs", with exactly the same message body as the message above. If the survey
was anonymous, how come they know I haven't filled it out?

Just to make sure I am talking about a yet another example of fluency in
doublespeak at the university, I've asked another student to forward me his
copy of the same message from ASUU. The message body in his email is the same
as in mine, except for the link:

http://studentvoice.com/p/?uuid=5a2c802b3575458ca8ebec97163c17e1

As you can see, the same anonymous survey was pointed to by two different
links to two different students.

I would say using doublespeak is a great way to build trust around what you
work on and to get students involved. Go Utes!		
