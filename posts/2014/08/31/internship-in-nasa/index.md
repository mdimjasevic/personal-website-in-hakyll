---
author: Marko Dimjašević
comments: true
date: 2014-08-31 05:01:07+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/08/31/internship-in-nasa/
slug: internship-in-nasa
title: "Internship in NASA!"
wordpress_id: 34205
categories:
- science
tags:
- Ames
- internship
- NASA
---
[![The Butterfly Nebula](hubble-butterfly-nebula.jpg "The Butterfly Nebula"){ width=100% } ](https://en.wikipedia.org/wiki/NGC_6302)

All of us are excited when news outlets report about space missions
and programs, such as sending the [Curiosity rover][curiosity] to
Mars, the [International Space Station][iss] orbiting the Earth, the
[Voyager 1][voy1] probe entering the previously unexplored region of
space known as [interstellar space][stellar], traveling "further than
anyone, or anything, in history", and the [Hubble Space
Telescope][hubble] taking stunningly awesome photos of space and time,
such as the photo of the Butterfly Nebula above. And there is a reason
why we get excited - things not residing on Earth are more and more
not just daydreamed about, but also explored by the human kind. We get
to explore new worlds! For example, thanks to the progress of science,
analyzing soil samples from Mars is not a fancy anymore and we
accurately know the rate of expansion of the universe.


<!-- more -->


An organization that is almost always to blame for such news is
NASA. When someone says "NASA", these images typically come to mind:

![](astronaut-300x300.jpg)

![](nasa-logo.png)

![](us-flag-on-moon.jpg)

![](launch-of-friendship-7-150x150.jpg)


These are powerful images serving as an inspiration to everyone. I am
so happy to say I will be a part of that! No, I won't be going to
space, but I will be a research intern in NASA!


Starting September 22 this year, I will spend 12 weeks at the [NASA
Ames Research
Center](http://www.nasa.gov/centers/ames/home/index.html). The center
is located in Mountain View, California. You might be wondering what I
will do there. The work will be in the field of formal verification,
and in particular I will be testing collision avoidance
algorithms. Hopefully I will make a dent in the research done in NASA.


It is hard for me to describe how excited about and horrified of this
opportunity at the same time I am!


[curiosity]: https://en.wikipedia.org/wiki/Curiosity_%28rover%29
[iss]: https://en.wikipedia.org/wiki/International_Space_Station
[voy1]: https://en.wikipedia.org/wiki/Voyager_1
[stellar]: https://en.wikipedia.org/wiki/Interstellar_space
[hubble]: https://en.wikipedia.org/wiki/Hubble_Space_Telescope
