---
author: Marko Dimjašević
comments: true
date: 2014-08-09 15:11:20+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/08/09/free-software-on-the-final-frontier-gnu-radio-controls-the-isee-3-spacecraft/
slug: free-software-on-the-final-frontier-gnu-radio-controls-the-isee-3-spacecraft
title: "Free Software on the final frontier: GNU Radio controls the ISEE-3 Spacecraft"
wordpress_id: 34166
categories:
- free software
- science
post_format:
- Link
tags:
- GNU Radio
- ISS
- NASA
---

![International Sun\/Earth Explorer 3](ISEE3-ICE.jpg "International Sun\/Earth Explorer 3"){ width=100% }

More and more space technology is free software, which is great to
hear. Not that long ago the International Space Station switched to
Debian GNU/Linux, and now GNU Radio is used by citizen scientists to
bring back to life a spacecraft abandoned by NASA in 1999. Read more
about it at the [Free Software
Foundation](https://www.fsf.org/blogs/community/free-software-in-space-gnu-radio-and-the-isee-3-spacecraft).

Let's hope some day there will be nothing but free software in
science.
