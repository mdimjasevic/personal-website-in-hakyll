---
author: Marko Dimjašević
comments: true
date: 2014-08-09 21:32:22+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/08/09/handcuffed-research/
slug: handcuffed-research
title: "Handcuffed Research"
wordpress_id: 34173
categories:
- science
tags:
- academic publishing
- copyright
- Diego Gomez
- Electronic Frontier Foundation
- open access
- research
---

[![Courtesy of Victor](handcuffs.jpg "Courtesy of Victor"){ width=100% } ](https://secure.flickr.com/photos/v1ctor/7606416730)

Researchers publish their work so as to advance science and exchange
ideas with other researchers. It is a crucial part of research, yet it
is hindered by copyright laws throughout the world. Researchers face
criminal charges and being put to prison for years for sharing
research articles. One of the most recent examples of such act is a
[prison charge against Diego Gomez][Gomez], a Master's student in
Columbia researching biodiversity. Coming from Croatia, I know
accessing research articles is a big issue, especially in countries
and at research institutions that cannot afford a ticket to get past a
publisher's paywall.


<!-- more -->


While Open Access has a noble goal of providing access to research
articles for free for everyone, it is not a solution as the
[Electronic Frontier Foundation][OA] is suggesting. It looks like the
academic publishing could use a complete reboot, and not just a
facelift from the pay-to-read paradigm to the pay-to-publish paradigm,
what Open Access is. If Open Access was to be adopted, tax payers
would still be paying for research twice, and those with scarce
economic resources wouldn't be able to publish their research.

I wish articles like those by the Electronic Frontier Foundation
didn't fall short of identifying the whole problem because fighting
symptoms only is not going to get us far --- we will still have the
handcuffed research as before.

[Gomez]: https://www.eff.org/deeplinks/2014/07/colombian-student-faces-prison-charges-sharing-academic-article-online
[OA]: https://www.eff.org/deeplinks/2014/08/front-lines-open-access-fight-colombian-students-prosecution-highlights-need
