---
author: Marko Dimjašević
comments: true
date: 2014-12-07 02:28:44+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/12/07/mr-grim/
slug: mr-grim
title: "Mr. Grim"
wordpress_id: 34615
post_format:
- Image
---

Last week I was at the California Academy of Sciences --- again, but
this time with a proper camera. It was worth it. One of my favorite
shots I made that day is Mr. Grim.

[![Mr. Grim](mr-grim.jpg "Mr. Grim"){ width=100% }](mr-grim.jpg)
