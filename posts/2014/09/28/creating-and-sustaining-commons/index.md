---
author: Marko Dimjašević
comments: true
date: 2014-09-28 07:01:26+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/09/28/creating-and-sustaining-commons/
slug: creating-and-sustaining-commons
title: "Creating and Sustaining Commons"
wordpress_id: 34296
categories:
- free culture
- free software
tags:
- commons
- creative commons
- donating
- OpenStreetMap
- volunteering
- Wikipedia
---

Ever since I got hooked on ideas of emancipation and solidarity by
learning about free software, I've wanted to give back by writing free
software and in general making other things such as art and writing a
common good. That's what is called social production. When I create, I
draw from pools of creative commons and then give the outcomes back to
the community to be used, studied, modified, and distributed, all with
[the four essential cultural work freedoms][freed]. So I translated
part of the KDE desktop environment from English to Croatian, I've
been writing [some][ikev2] [software][maline], sharing my photos under
free culture licenses, adding and improving [my hometown][kzosm] on
the free as in freedom world map [OpenStreetMap][osmwp]... but I've
never contributed a single word, a pixel, or a sound to Wikipedia.

[![Photography by Lane Hartwell](wikipedia-globe-held-in-hands-300x232.jpg "Photography by Lane Hartwell")](https://en.wikipedia.org/wiki/Wikipedia:Wikipedians#mediaviewer/File:10_sharing_book_cover_background.jpg)

> "Anyone can put Wikipedia in the palms of his or her hands,
> including you. All you need to do is simply edit an article."

<!-- more -->


Therefore, according to the quote associated with the above photo of
the Wikipedia puzzle globe held in hands, I never did that --- I never
held Wikipedia in my hands. That makes me feel uneasy. Just like
pretty much every other netizen, I use Wikipedia daily. I use it 100
times a day. Maybe even more, I can't tell. It's there for me whenever
I need to look up something, and I am looking up something all the
time. Even while writing this blog post, I looked up Wikipedia and its
sister projects like [Wiktionary](https://www.wiktionary.org/) and
[Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) so
many times. All of them form an incredibly rich source of knowledge
and inspiration. Have you heard of a piece of software called
[MediaWiki](https://www.mediawiki.org/wiki/MediaWiki)? That's the
software that serves you not just articles from the above listed
projects into your browser, but from thousands of other web
pages. MediaWiki is free software, which is no wonder. The whole
Wikipedia [is based](https://en.wikipedia.org/wiki/Wikipedia#History)
on free software principles.


So, I've been simply using what millions of volunteers have been
writing and adding together to Wikipedia without making a dent
myself. A few years ago I'd decided to put an end to the unease ---
since then I've been donating money to the [Wikimedia
Foundation](https://wikimediafoundation.org/wiki/Home). The foundation
is the non-profit charitable organization behind Wikipedia and its
sister projects. People in the foundation don't write articles on
Wikipedia; that's what netizens do. Instead, they provide a valuable
service by running the projects and their infrastructure. Wikipedia is
used by millions of people, with 10 -- 20 billion page views every
month, which makes it the #5 website in the world. To keep it running,
the foundation needs both the people and the infrastructure.

It feels good to make a donation to such an amazing cause. Wikipedia
is the knowledge library of the Internet. As Jimmy Wales, Wikipedia
Founder, has put it: "It is like a library or a public park. It is
like a temple for the mind. It is a place we can all go to think, to
learn, to share our knowledge with others." Wikipedia is an
astonishing achievement of the mankind. It is a creative commons good.


I've donated to the Wikimedia Foundation again a few days ago. It's a
small amount, but that is okay because so many other people donate
money too, which makes it possible to meet the foundation's
multimillion donation needs year after year. Have you donated this
year? Even if you do create and edit articles on Wikipedia, [consider
making a
donation](https://donate.wikimedia.org/w/index.php?title=Special:FundraiserLandingPage)
to sustain the truly awesome project.


Should I also finally start editing Wikipedia articles? I definitely
should and I am going to!


[freed]: http://freedomdefined.org/Definition
[ikev2]: https://gitorious.org/ikev2
[maline]: https://github.com/soarlab/maline
[kzosm]: http://www.openstreetmap.org/?mlat=46.0235&mlon=16.5457#map=14/46.0235/16.5457
[osmwp]: https://en.wikipedia.org/wiki/OpenStreetMap
