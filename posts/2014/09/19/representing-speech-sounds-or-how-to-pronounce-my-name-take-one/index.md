---
author: Marko Dimjašević
comments: false
date: 2014-09-19 06:00:18+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/09/19/representing-speech-sounds-or-how-to-pronounce-my-name-take-one/
slug: representing-speech-sounds-or-how-to-pronounce-my-name-take-one
title: "Representing Speech Sounds (or How To Pronounce My Name — Take One)"
wordpress_id: 34252
tags:
- IPA
- LaTeX
- phonetic transcription
- phonetics
- TIPA
---

![Marko Dimjašević](ipa-marko-dimjasevic-300x31.png "Marko Dimjašević")

No, this not a yet another gibberish of mine in the glagolitic
alphabet! That's a notation saying how to pronounce my first and last
name according to:

![IPA](ipa-in-ipa.png "IPA")

That's right, that's IPA. Maybe this IPA doesn't seem as smooth for
your eyes as those IPAs on Saturday evenings do for your throat, but
that's because we're talking about the [International Phonetic
Alphabet](https://en.wikipedia.org/wiki/International_Phonetic_Alphabet)
here.


<!-- more -->


You might have noticed similar unusual symbols on Wikipedia, commonly
next to a name not originating from the same language the article is
written in. For example, the Utah state's name is derived from the Ute
tribe, and at the very beginning of a [Wikipedia article on
Utah](https://en.wikipedia.org/wiki/Utah) you can see a few characters
enclosed in a pair of forward slashes (or even multiple pronunciation
options like here) explaining how "Utah" sounds when spoken out
loud:

![Utah in IPA](utah-in-ipa-300x43.png "Utah in IPA")


I also remember the symbols from English textbooks for foreign
language classes in school.


Given that people from non-Slavic-speaking parts of the world have a
hard time pronouncing my name, I thought I could make it easier for
them by writing my name in a phonetic transcription. Enter IPA. You
might argue not many people know IPA and you are probably right. Hey,
I had to browse multiple pages to figure out how to put together my
name! Nevertheless, I wanted to give at least an approximate
pronunciation of those two words that identify me.


This is my first attempt to write my name, borrowing from an [IPA help
page on
Wikipedia](https://en.wikipedia.org/wiki/Help:IPA_for_English#Key). There
might be something wrong with how I've written the name in IPA, but I
hope to improve the transcription. If you can see what's wrong with
it, please leave a comment below! Once I'm done with this, I'm also
planning to put a sound recording along with it, just like on the
Wikipedia article on Utah. Those two things will then be on the front
page of my web site.


As a side note, here is how I generated the transcription. First, I
wrote the following LaTeX document:

```latex
\documentclass{article}
\usepackage{tipa}
\begin{document}
\textipa{/m2rkO; ""dIm"j2SEvItS/}
\end{document}
```

A [cheat sheet][sheet] might come in handy if you want to try writing
something yourself. Then I used pdflatex to convert the document to
PDF. Once the PDF file was ready, I used a neat command line tool for
converting a PDF file into the SVG format:

    pdf2svg my-name.pdf my-name.svg


After that Inkscape comes into play: adjust document properties
(resize page to content) and export to bitmap (PNG). That's it.


[sheet]: http://www.ling.ohio-state.edu/events/lcc/tutorials/tipachart/tipachart.pdf
