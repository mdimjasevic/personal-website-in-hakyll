---
author: Marko Dimjašević
comments: true
date: 2014-11-02 21:53:51+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/11/02/the-dance-of-merge-sort/
slug: the-dance-of-merge-sort
title: "The Dance of Merge Sort"
wordpress_id: 34461
tags:
- choreography
- folk dance
- merge sort
- NASA
- record tapes
---

[![Courtesy of NASA. Merge sort was used to sort data stored on magnetic tapes.](IBM_729_Tape_Drives.nasa.jpg "Courtesy of NASA. Merge sort was used to sort data stored on magnetic tapes.")](https://en.wikipedia.org/wiki/Merge_sort)

As with any other subject, so it is in computer science that lecturers
work hard to convey ideas in topics that they're covering. Some ways
are more effective than others. From the dance of the record tapes in
NASA depicted above to the dance of performers in Romania, merge sort
gives the beat. In my opinion, this a pretty effective way of
demonstrating how the merge sort algorithm works:

> [Merge-sort with Transylvanian-saxon (German) folk dance](https://www.youtube.com/watch?v=dENca26N6V4)

		
