---
author: Marko Dimjašević
comments: false
date: 2014-11-18 07:29:51+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/11/18/phonetics-on-front-page/
slug: phonetics-on-front-page
title: "Phonetics on Front Page"
wordpress_id: 34555
categories:
- ephemera
post_format:
- Status
---

Just like I'd promised in a post on [phonetics][post] a while back,
I've added a phonetic transcription and a sound recording of my name
to the [front page](/). HTML5 + Audacity for the recording, UTF-8 for
IPA.

[post]: /2014/09/19/representing-speech-sounds-or-how-to-pronounce-my-name-take-one/
