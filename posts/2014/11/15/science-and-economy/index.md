---
author: Marko Dimjašević
comments: true
date: 2014-11-15 07:36:15+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/11/15/science-and-economy/
slug: science-and-economy
title: "Science and Economy"
wordpress_id: 34477
categories:
- science
tags:
- capitalism
- economy
- internship
- militarism
- NASA
- US budget
---

[![Courtesy of NASA/Goddard Space Flight Center/Debbie Mccallum](science-in-nasa-goddard-space-flight-center.jpg "Courtesy of NASA/Goddard Space Flight Center/Debbie Mccallum"){ width=100% } ](https://www.flickr.com/photos/gsfc/6185861183)

For the last two years I've been in the United States studying and
doing research in computer science. While I don't know much about how
science looks like in Europe (where I come from), I can definitely
tell that I brought different expectations from Europe about what
science will look like in the US.


<!-- more -->


As an article series I give you a link to at the bottom of this post
argues, it turns out that the economic system present in a society as
well as the society's aspirations towards militarism play a great role
in shaping the research it carries out and even what kind of
scientific questions can be asked. Capitalism is the economic system
in the United States and that by itself already tells a lot, but in
the US the system has been pushed towards its extremes. For the
upcoming fiscal year 2015, the president of the US has [proposed a
budget][budget] in which 16% would go to military, and 1% to
science. The military part would be $555 billion worth, and some of it
will without any doubt go into research. Such an emphasis the US puts
on military --- compared to science --- tells what the society values.


I've been at a research internship in NASA for the last couple of
months. It is interesting to "watch from inside" how the economic
system plays a role in NASA and how highly positioned employees praise
the system. What if all NASA employees, and especially NASA
Administrator, instead of [praising the economic system][praise],
asked the federal government not to allocate as much money to military
and allocate the money to NASA instead? Maybe then would NASA, almost
half a century after putting men on the Moon, be capable of launching
manned missions into low Earth orbit and beyond again.


For the series on what science is and what it could be in the future
if we were to aspire a different economic system, go to
[teleSUR].

[budget]: http://web.archive.org/web/20141007205710/https://www.nationalpriorities.org/budget-basics/federal-budget-101/spending/
[praise]: http://web.archive.org/web/20141115063610/http://casgc.ucsd.edu/?p=6107
[teleSUR]: http://www.telesurtv.net/english/opinion/Science-and-the-Left-1-What-It-is-20141022-0004.html
