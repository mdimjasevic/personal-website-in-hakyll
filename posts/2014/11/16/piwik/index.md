---
author: Marko Dimjašević
comments: false
date: 2014-11-16 06:27:18+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/11/16/piwik/
slug: piwik
title: "Piwik"
wordpress_id: 34529
categories:
- ephemera
post_format:
- Status
---

Today for this website I've set up Piwik, a free software privacy
respecting web analytics application. [Check it
out](http://piwik.org/)! Along with it now there is a [privacy policy
page](http://dimjasevic.net/marko/privacy/).
