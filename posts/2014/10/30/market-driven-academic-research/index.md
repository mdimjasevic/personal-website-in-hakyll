---
author: Marko Dimjašević
comments: false
date: 2014-10-30 07:21:54+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/10/30/market-driven-academic-research/
slug: market-driven-academic-research
title: "Market-driven Academic Research"
wordpress_id: 34448
categories:
- science
- University of Utah
tags:
- corruption
- free market
- research funding
- Technology &amp; Venture Commercialization office
---

[![Courtesy of 401(K) 2012](money.jpg "Courtesy of 401(K) 2012"){ width=100% }](https://www.flickr.com/photos/68751915@N05/6355388579)

If there is a private entity providing funds for a project that is
carried out by another entity, everyone would find it egregious if the
receiving entity denied access to the project's deliverables to the
funding entity. However, as a society we find it perfectly acceptable
that when the funding entity is the general public funding a research
project through taxes, that the receiving entity does not have to make
research results freely available to the public.


<!-- more -->


With academics having to search for research funding beyond publicly
provided resources, there come interesting changes in a market-driven
academic research world. Llewellyn Hinkes-Jones writes incisively in
an article about such a world, giving an example of corruption at the
University of Utah where I'm a student. Unfortunately, that is not a
matter of the past --- the university has an office promoting ideas of
patenting research and saying that a research idea is the same as a
property, in particular an "intellectual property".


As the office puts it on its [website](https://www.tvc.utah.edu/),
"One of [our] goals is to provide process support services to
companies and universities to help them successfully commercialize
their intellectual property." And here lies the problem --- at a
publicly funded university, such as the University of Utah, research
outcomes do not belong to the research team carrying out the research,
but to the public that has been funding it. Therefore, "their" in the
quoted statement cannot refer to a "private thing" belonging to the
team. The university supporting the team in denying the general public
from accessing the research ought to be banned, and not
encouraged. Taking money from the public and then privatizing the
research is an act of misappropriation.


For the article in which Hinkes-Jones writes about this issue and
related problems that come with research being a yet another traded
item on the market, go to
[Jacobin](https://www.jacobinmag.com/2014/06/bad-science/).
