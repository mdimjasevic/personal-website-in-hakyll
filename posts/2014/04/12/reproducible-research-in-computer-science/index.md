---
author: Marko Dimjašević
comments: true
date: 2014-04-12 05:58:45+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2014/04/12/reproducible-research-in-computer-science/
slug: reproducible-research-in-computer-science
title: "Reproducible Research in Computer Science"
wordpress_id: 34127
categories:
- reproducible research
tags:
- Debian
---

[![Courtesy of Nomadic Lass](research.jpg "Courtesy of Nomadic Lass")](https://secure.flickr.com/photos/nomadic_lass/6820209341)

One of the gists of science is to form a hypothesis of a phenomenon
and then test the hypothesis via predictions through experimental
studies. When a scientist announces a discovery claiming her
hypothesis was confirmed through the experimental studies, she has to
provide documentation with all the data and methodology. This is the
basis of the scientific method, enabling other scientists to
scrutinize the discovery. If there was no expectation to share the
documentation, data, and methodology, anyone could claim discoveries
without being held accountable, and science would be a yet another
joke.


<!-- more -->


In computer science, the data and methodology are stored in form of
computer files, representing raw data and computer programs that
perform computation on the data. That makes it easy, or at least it
sounds easy, to share the documentation, data, and methodology with
other scientists because sharing boils down to copying digital
files. However, due to the complexity and rapid development of
computer systems, it is not always clear if the documentation is
complete and how to make the discovery reproducible 10 years down the
road.


Even though it is not applicable in all forms of computer science
research, computer scientists can share their experimental studies in
the form of virtual machines, a software-based emulation of
computers. A virtual machine encompasses the whole environment of the
experimental studies, which enables other scientists to repeat the
studies. Nonetheless, the availability of the virtual machines doesn't
necessarily make the studies reproducible; the machines themselves
don't document the steps taken to build them.


I would like to make my research easily repeatable and
reproducible. In order to meet that goal, I want to provide the
research community with both virtual machines with my experimental
studies and with ways to rebuild the machines from scratch. I am still
not there, but I am thinking of writing a piece of software that would
pull a clean operating system installation image from the Internet,
install the operating system, configure it, and install my
experimental studies.


The good news is that there is already something like that out there,
so probably the only thing I will need to do is to configure such
software to do what I want it to do. In particular, I am talking about
_libvirt_. It is a collection of tools for managing virtual
machines. In other words, this would enable me to provide other
scientists with a way to rebuild a virtual machine and as a result of
it, I would also have the virtual machine itself ready to be shared.


The operating system I am planning to use for the virtual machines is
[Debian GNU/Linux](https://www.debian.org/). There are several reasons
why Debian, and not some other GNU/Linux distribution or some other
operating system. First of all, Debian is a free (as in freedom)
operating system. The most trivial consequence of Debian being free
software is that it is available free of charge, which means other
scientists will be able to repeat and reproduce my research without
having to pay for software licenses. Free software also means no
strings attached --- no artificial limitations nor user
subjugation. Second of all, because source code availability is a
prerequisite for free software, it means the whole environment of my
experimental studies will be ready for careful scrutiny. Third, no
other GNU/Linux distribution has such a strong and big community that
provides archives for such a long time; all releases, including the
first Debian 1.1 release, have been available since 1996. Fourth,
Debian is another word for stability; there are so many aspects to
Debian stability, including the availability of its archives, making
it a perfect match for reproducible research.


Even if you haven't worried too much so far about others being able to
(easily) reproduce your research, you should still care about
reproducible research if nothing else, then for yourself to be able to
repeat and reproduce your own experimental studies in a month, in 5
years, and in 20 years too. Yes, it takes time to carefully document
everything and often there are deadlines to be met, but this is not
your last experimental study ever. Therefore, it is worthwhile to
spend some time in figuring out how to make your research
reproducible, and reap benefits from it in research projects to come.
