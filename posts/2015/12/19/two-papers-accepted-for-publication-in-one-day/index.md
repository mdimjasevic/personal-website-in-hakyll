---
author: Marko Dimjašević
comments: true
date: 2015-12-19 02:56:57+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/12/19/two-papers-accepted-for-publication-in-one-day/
slug: two-papers-accepted-for-publication-in-one-day
title: "Two Papers Accepted for Publication in One Day"
wordpress_id: 35069
categories:
- free software
- science
tags:
- CMU
- IWSPA
- jDART
- maline
- NASA
- NASA Ames
- TACAS
---

[![Courtesy of purdman1](printing.jpg "Courtesy of purdman1"){ width=100% }](https://www.flickr.com/photos/purdman1/2875431305/)

Friday, December 18 was an amazing day for me when it comes to
research --- I got two papers accepted for publication! One of the
papers was a never-ending nightmare I couldn't get rid of, while the
other paper was a first-time submission that got accepted right away.


<!-- more -->


The first paper is titled "Evaluation of Android Malware Detection
Based on System Calls"\cite{iwspa2016-daur}. It's a line of work that
started two years ago as a class project when Simone Atzeni, my lab
mate, and I were taking a class in machine learning. We decided to do
a project on detecting malicious apps in Android using system call
traces and machine learning to discriminate malicious from benign
apps. The project was promising, so we extended it and got our
(co)advisor and [Ivo Ugrina][ivo] on board. Finally, in its fourth
edition this work got accepted for publication at the [2nd
International Workshop on Security And Privacy Analytics][iwspa]
(IWSPA). I will be traveling to New Orleans in March 2016 to present
the work. If you are interested, the paper's underlying tool
[maline](https://github.com/soarlab/maline) is available online.


The other paper is "JDart: A Dynamic Symbolic Analysis
Framework"\cite{tacas2016-ldghikrr}. It is a tool paper on a symbolic
(concolic) execution tool for Java. I am really happy with this tool
as NASA, where the tool originates from, finally decided to make it
[free software](https://github.com/psycopaths/jdart)! As a matter of
fact, both JDart and all its dependencies are free software now (as
long as you discard optional proprietary SMT solvers it can work
with). As a side note, JDart is a dependency for my tool
[JDoop](/2015/11/16/jpf-doop-is-dead-long-live-jdoop/). This
work is with folks from the NASA Ames Research Center and the Carnegie
Mellon University, Silicon Valley Campus (and with folks that were
there at some point). The paper got accepted for publication at the
22nd [International Conference on Tools and Algorithms for the
Construction and Analysis of
Systems](http://www.etaps.org/index.php/2016/tacas) (TACAS). The
conference will take place in April 2016 in Eindhoven, The
Netherlands. It's unlikely I'll be there.


Now onto the easiest part of publishing papers --- polishing the two
papers to get them camera-ready!


[ivo]: https://web.math.pmf.unizg.hr/~iugrina/en.html
[iwspa]: https://web.archive.org/web/20151220022636/http://capex.cs.uh.edu/?q=content/international-workshop-security-and-privacy-analytics-2016
