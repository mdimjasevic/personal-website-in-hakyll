---
author: Marko Dimjašević
comments: false
date: 2015-04-19 04:37:06+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/04/19/view-over-salt-lake-city/
slug: view-over-salt-lake-city
title: "View Over Salt Lake City"
wordpress_id: 34784
categories:
- photography
post_format:
- Image
tags:
- hiking
- Little Black Mountain
- Oquirrh Mountains
- Salt Lake City
- Utah
---

Recently I was on a hike not that far from the Salt Lake City
downtown. The hike was along the Little Black Mountain trail. The
trail offers amazing views over Salt Lake City, the whole Salt Lake
Valley, and over the Great Salt Lake. Here is a photo, taken at a 1 km
higher altitude than that of the downtown, with a group taking a break
from the hike to enjoy a view over the city. The Oquirrh Mountains are
in the background.

[![A view over the Salt Lake City downtown](view-over-slc.jpg "A view over the Salt Lake City downtown"){ width=100% }](view-over-slc.jpg)

		
