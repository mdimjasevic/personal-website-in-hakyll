---
author: Marko Dimjašević
comments: false
date: 2015-04-19 19:21:40+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/04/19/another-reason-why-ads-are-bad-for-you/
slug: another-reason-why-ads-are-bad-for-you
title: "Another Reason Why Ads Are Bad For You"
wordpress_id: 34782
categories:
- ephemera
post_format:
- Status
---

If you haven't done web ad blocking up to now, now you have [a good
reason to start doing so][ars]. An excellent web browser plugin for
the task is [AdBlock Plus][abp].

[ars]: http://arstechnica.com/security/2015/04/faked-flash-based-ads-on-huffpo-other-sites-downloaded-extortionware/
[abp]: https://en.wikipedia.org/wiki/Adblock_Plus
