---
author: Marko Dimjašević
comments: true
date: 2015-01-24 04:34:46+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/01/24/back-to-utah/
slug: back-to-utah
title: "Back to Utah!"
wordpress_id: 34671
categories:
- ephemera
post_format:
- Status
---

I am back to Utah from my internship at NASA! Last night submitted a
paper to ISSTA based on the internship. Today moved into an apartment
close to the U. Life's good!
