---
author: Marko Dimjašević
comments: true
date: 2015-01-31 07:14:41+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/01/31/once-upon-a-time-in-the-west/
slug: once-upon-a-time-in-the-west
title: "Once Upon A Time In The West"
wordpress_id: 34674
categories:
- science
tags:
- NASA
- NASA internship
- Silicon Valley
---

A few weeks ago I finished my internship in the NASA Ames Research
Center in Mountain View in California. If you're asking how it was and
you want a short answer, I can tell you it was sweet. If you want a
slightly longer answer, I can tell you it was bittersweet. Keep on
reading for an even longer answer.


<!-- more -->


Whom I work with means a lot to me. Even if it was NASA, but my mentor
there was bad at guiding a student and good at making a workday a
nightmare, the internship wouldn't be worth it. However, what I had
was quite the opposite of that! I was lucky to work with a [remarkable
mentor](http://ti.arc.nasa.gov/profile/dimitra/) during the
internship. By the end of it, I was happy with the work we've done and
we've got interesting results. My opinion is this wouldn't be possible
if I didn't have as engaging mentor.


What I was working on is runtime verification of a software system for
air traffic control called AutoResolver, which has been developed at
NASA. There were many things I learned (about) or got better in,
including [air traffic control](http://www.imdb.com/title/tt0120797/),
runtime verification, aspect-oriented programming, a language called
AspectJ, Java and Eclipse, testing, and various good software
engineering practices, just to name a few. We formed and verified
several separation assurance properties and monitored a requirement
through novel techniques we came up with. With the infrastructure we
developed we were able to find several previously unknown problems in
the environment combining our wrapper and AutoResolver and we learned
quite a lot about AutoResolver's operational logic and its
behavior. Just last Friday we wrote this up and submitted a paper to
the International Symposium on Software Testing and Analysis. I'm
planning to put it to arXiv soon too.


On the other hand, every day of the internship was a horror to
me. Just like many bad things that happened to me prior to the
internship and at its very beginning, I ended up living in a
disturbing place during the internship. Real estate is incredibly
expensive in the Silicon Valley, which means that renting a place to
stay at is quite expensive too. For example, I was sharing an
apartment with two more guys and my share of the rent was $1100 a
month. Just to make it clear, the place was nothing special and it was
in the middle of nowhere. What was wrong with the place? Well, I don't
know how you would feel if you lived with a guy that claimed to have
ordered a mass murder and bragged about it by telling you a
story. Furthermore, the streets in the neighborhood were not safe to
walk in the evening. Thugs would be sitting in their cars and just
waiting for someone to get into their street. You would see broken
windows on a parked car on one day coming back from work. Then on
another day you would see three police cars with their lights on and
policemen telling you to take a different route. Or have I maybe told
you that on yet another day police officers broke into a neighbor's
apartment shouting: "Put your weapons down!"?


In spite of all my efforts to look up the neighborhood with regard to
the crime rate during an apartment search, I ended up in one that
wasn't pleasant to live in at all. Should I have moved? Maybe I should
have. I was seriously thinking of moving. But then again, I had a leg
fracture at the time, which I earned while moving out from Salt Lake
City to be able to afford the rent in California during the
internship. Furthermore, by the time I said to myself that this is
just too much, I didn't have a rented car anymore. (For people outside
the US: if you're in the US and you don't have a car, you'll have a
hard time in your day-to-day life.) This was in Santa Clara. I wanted
to stay in Mountain View close to NASA, but with the pitiful salary I
had at the internship, that was not really feasible.


Maybe it is not directly NASA to blame for the pitiful salary because
I was hired through a contracting company. On the other hand, the
company was paying me out of the money they received from
NASA. Allegedly, NASA didn't know how much actually I got out of the
money they allocated for me. The company, at its discretion, held a
share of the money for themselves, and then served me with what was
left. I am done with the internship, but NASA should seriously think
about the deal that they have with the company, otherwise in the long
run --- if not already --- they might end up unhappy with interns
willing to come to NASA.


If it is still not clear where I had this interesting experience, it
was in the heart of the Silicon Valley. That's California, the United
States. During the day I would be at NASA doing research. In the
evening I would put on a hoodie on my way back from work in order to
appear tough, only to come to an apartment shared with a mass
murderer. Oh yeah, once upon a time in the West... dire straits.


[![Courtesy of Kevin D](dark-street.jpg "Courtesy of Kevin D"){ width=100% }](https://www.flickr.com/photos/kdigga/11740460174)
