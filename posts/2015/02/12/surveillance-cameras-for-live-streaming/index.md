---
author: Marko Dimjašević
comments: false
date: 2015-02-12 18:40:37+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/02/12/surveillance-cameras-for-live-streaming/
slug: surveillance-cameras-for-live-streaming
title: "Surveillance Cameras for Live Streaming"
wordpress_id: 34703
categories:
- University of Utah
tags:
- surveillance
---

[![Courtesy of EFF](surveillance-camera.png "Courtesy of EFF"){ width=100% }](https://commons.wikimedia.org/wiki/File:Surveillance-camera.png)

Today a bureaucrat from Carnegie Mellon University is giving a talk at
the University of Utah. Prior to that he will have a round table with
the research group I'm part of. The round table will happen in my
department in the Grad Lounge. Unfortunately, I don't feel so well
today so I'll be staying at home, hence I'll miss the round
table. However, there is a surveillance camera installed in the Grad
Lounge so that egregious graduate students don't misbehave in there.


If only the department decided to live stream the round table with the
surveillance camera. Then I could lay back and watch the round table
from my sofa. I see how this could be a great way to increase turnouts
at such round tables. The department could then finally have a
justification for putting surveillance cameras in lounges, hallways,
classrooms, etc.
