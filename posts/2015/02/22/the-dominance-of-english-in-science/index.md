---
author: Marko Dimjašević
comments: true
date: 2015-02-22 19:30:53+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/02/22/the-dominance-of-english-in-science/
slug: the-dominance-of-english-in-science
title: "The Dominance of English in Science"
wordpress_id: 34742
categories:
- ephemera
- science
post_format:
- Status
---

[æon](http://aeon.co/magazine/science/how-did-science-come-to-speak-only-english/)
brings a nice essay on how did science come to speak English only. It
explores the history of languages used in science.
