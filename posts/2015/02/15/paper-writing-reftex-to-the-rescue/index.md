---
author: Marko Dimjašević
comments: false
date: 2015-02-15 02:22:24+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/02/15/paper-writing-reftex-to-the-rescue/
slug: paper-writing-reftex-to-the-rescue
title: "Paper writing: RefTeX to the rescue!"
wordpress_id: 34711
categories:
- science
tags:
- Emacs
- free software
- LaTeX
- RefTeX
- scientific writing
---

[![Courtesy of Nic McPhee](fountain-pen.jpg "Courtesy of Nic McPhee"){ width=100% }](https://www.flickr.com/photos/nicmcphee/2756494307)

Emacs provides a very powerful LaTeX environment for paper
writing. I've been using it for a while and I've been enjoying it. In
this post I'll give a simple example of how a regular-expression
search-and-replace can be done in a multifile paper.


<!-- more -->


Say you have a paper you've been writing with a few other
researchers. A good practice is to split your paper into several LaTeX
files. For example, each paper section can be one file. This is a
better paper organization than having everything in a single gigantic
LaTeX file. Also, it lets you and your collaborators work on the paper
in parallel --- you writing an introduction, one collaborator writing
about related work, and yet another collaborator adding in results you
got, all at the same time. Then you would have a master LaTeX file
incorporating all the section files like this:

```latex
\input{macros.tex}
\input{abstract.tex}
\input{introduction.tex}
\input{related-work.tex}
\input{background.tex}
\input{approach.tex}
\input{implementation.tex}
\input{evaluation.tex}
\input{conclusion.tex}
\input{acknowledgments.tex}
```


Next, let's say the paper is on [detecting malicious apps in
Android](https://github.com/soarlab/maline). During the writing of the
paper, you've been using both "app" and "application" throughout the
paper to refer to a smartphone application. Then you realize it'd be
better if you used only one of the terms consistently. You go with
"application".


You have several obstacles in making a consistent update to the
paper. First, "app" is a prefix of "application", meaning you have to
be careful not to do a dummy search-and-replace and turn "application"
into "applicationlication". Second, you have to take care of
capitalization, i.e. you need to replace "App" at the beginning of a
sentence with "Application". Third, you have a multifile
document. Will you manually walk through all the files sentence by
sentence and watch not to make a mistake?


RefTeX to the rescue!
[It](https://www.gnu.org/software/auctex/reftex.html) is an Emacs
minor-mode helping you with references, labels, indices, operating
multifile documents, etc. All you have to do to properly replace all
"app" occurrences with "application" is to run a RefTeX
regular-expression command anywhere in the document:

	M-x reftex-query-replace-document


Provide **\<app\>** for the first argument and **application** for the
second argument. You still might want to walk through all the matches
and for example make sure you're not messing up a figure label. Once
you are done, save all the modified paper files by running:

	M-x reftex-save-all-document-buffers

If you keep your paper in a version control system repository like
Git, you can easily see which files have been affected by the update:

	$ git status --short
	M acknowledgments.tex
	M approach.tex
	M background.tex
	M evaluation.tex
	M implementation.tex
