---
author: Marko Dimjašević
comments: false
date: 2015-09-15 04:50:57+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/09/15/the-smartphone-society/
slug: the-smartphone-society
title: "The Smartphone Society"
wordpress_id: 35008
categories:
- ephemera
post_format:
- Status
tags:
- capital
- smartphone
- social networks
---

I recommend a fascinating read on [the smartphone
society](https://www.jacobinmag.com/2015/03/smartphone-usage-technology-aschoff/).
