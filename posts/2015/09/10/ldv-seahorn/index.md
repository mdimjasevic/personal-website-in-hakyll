---
author: Marko Dimjašević
comments: false
date: 2015-09-10 15:07:08+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/09/10/ldv-seahorn/
slug: ldv-seahorn
title: "LDV + SeaHorn"
wordpress_id: 35004
categories:
- ephemera
- software engineering
post_format:
- Status
tags:
- CAVA
- Docker
- LDV
- Linux
- SeaHorn
- software verification
---

As part of our CAVA project, one student looked at using the [Linux
Driver Verification](http://linuxtesting.org/ldv) tool in Docker in
combination with [SeaHorn](https://github.com/seahorn/seahorn) ---
[LDV with Docker](https://www.youtube.com/watch?v=YxKnY9ewzHo).
