---
author: Marko Dimjašević
comments: false
date: 2015-11-09 23:37:19+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/11/09/accessing-research-publications-off-campus/
slug: accessing-research-publications-off-campus
title: "Accessing Research Publications Off-campus"
wordpress_id: 35044
categories:
- ephemera
- science
post_format:
- Status
tags:
- Firefox
- FoxyProxy
- IP address-restricted access
---

The following combination of software tools is useful when you need to
access research publications off-campus in an IP address-restricted
access setting: GNU screen, keychain, autossh, and FoxyProxy +
Firefox.
