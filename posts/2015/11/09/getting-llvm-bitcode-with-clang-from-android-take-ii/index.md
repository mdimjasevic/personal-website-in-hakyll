---
author: Marko Dimjašević
comments: true
date: 2015-11-09 23:59:24+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/11/09/getting-llvm-bitcode-with-clang-from-android-take-ii/
slug: getting-llvm-bitcode-with-clang-from-android-take-ii
title: "Getting LLVM Bitcode with Clang from Android — Take II"
wordpress_id: 35031
categories:
- free software
- software engineering
tags:
- Android
- CAVA
- Clang
- GNU make
- LLVM
- whole program LLVM
---

A couple of months ago [I thought I got it][take-i] --- that I can
successfully get LLVM bitcode for a Bluetooth module from the Android
Open Source Project (AOSP). As it turns out, that was not the case
assuming the approach described in that blog post. The problem is that
the output of that compilation process looked like a wasteland: Clang
exited with lots of errors, only some bitcode files were generated,
but they were definitely not linked together, hence not usable.


[![Courtesy of daniel.stark](wasteland.jpg "Courtesy of daniel.stark"){ width=100% }][flickr]


Here is why and how to actually get a linked LLVM bitcode file for the
Bluetooth module.


<!-- more -->


The reason you might want a bitcode version of a code base is because
you want to do program analysis on it. Until recently I wasn't aware
of a general approach that would take an arbitrary program or library
project written in C/C++ and compile it to LLVM bitcode. One would
usually have to tweak the build system of the project in order to make
it LLVM bitcode emitting-friendly. Enter [Whole Program
LLVM](https://github.com/travitch/whole-program-llvm). The Whole
Program LLVM tool takes the arbitrary project's build system, runs it,
but at the same time except getting native code, in a smart way gets
bitcode as well. In essence, it first invokes a compiler as usual, but
then invokes it again on the same target file with the same options,
but this time with flags for outputting LLVM bitcode. A file system
path to the generated bitcode file is stored in the object file
generated in the first step, and in the end the Whole Program LLVM
tool links together all such bitcode files. Woot!


Unfortunately, following directions from WLLVM's readme didn't do when
I tried to get LLVM bitcode for the Bluetooth module in Android. The
reason is that the GNU make-based build system in Android is quite
complex, and simply changing values of CC and CXX environment
variables on the command line didn't do the trick. Here is what I had
to do to get it working, i.e. to get bitcode for the Bluetooth module:
	
1. Download the Whole Program LLVM tool (`wllvm`) from
   [https://github.com/travitch/whole-program-llvm][wllvm].

2. Add `wllvm`'s directory to the `PATH_`variable:

```bash
	export PATH=$PATH:/path/to/wllvm
```

3. Set the compiler to be used by `wllvm` to Clang:

```bash
	export LLVM_COMPILER=clang
```

4. For `wllvm` set a path to LLVM tools to the local prebuilt version
   distributed with AOSP:

```bash
	export LLVM_COMPILER_PATH=/path/to/android-repo/
		prebuilts/clang/linux-x86/host/3.6/bin
```

5. The trick is in not following instructions from the `wllvm`
   website, but dirty-hacking AOSP's build system. In particular,
   modify its `build/core/clang/config.mk` such that `CLANG` and
   `CLANG_CXX` have the following values:

```bash
	CLANG := /path/to/wllvm
	CLANG_CXX := /path/to/wllvm++
```

6. Set `LOCAL_CLANG:=true` in `build/core/clear_vars.mk`.

7. Run the build process (Replace `aosp_x86-eng` with your
   target). The `wllvm` tool will likely give bunch of warnings on
   unknown parameters, but it's safe to ignore the warnings:

```bash
	source build/envsetup.sh
	lunch aosp_x86-eng
	make -j32 Bluetooth
```

8. Now all object files have a section called `.llvm_bc` created by
   `wllvm`. The section contains a path to the respective `.bc` file
   for the native code object file. This is also true for the shared
   library of interest (i.e. the Bluetooth module with a JNI
   interface), namely `libbluetooth_jni.so`. You can make sure this is
   the case by executing:

```bash
	objdump -h out/target/product/generic_x86
	  /obj/lib/libbluetooth_jni.so
```

9. Link all of the bitcode into a single whole-library bitcode
   file. The `extract-bc` tool is part of the Whole Program LLVM
   project:

```bash
	extract-bc out/target/product/generic_x86/
	  obj/lib/libbluetooth_jni.so
```

10. The resulting file is:

```bash
	out/target/product/generic_x86/obj/
	  lib/libbluetooth_jni.so.bc
```


Out of curiosity, I tried to do the same for the whole AOSP code base
(i.e. by running `make` instead of `make Bluetooth`), but that
resulted in errors. I informed authors of the Whole Program LLVM tool
about it, so let's see if they can fix that.


[take-i]: /2015/09/07/getting-llvm-bitcode-with-clang-from-android/
[flickr]: https://www.flickr.com/photos/web-stark/16692192345/in/photolist-rr2ReK-4Hi6jF-5AJdxS-r9trpL-7KRDV1-8Rv9U7-ucYFMj-cqMMUo-nMbzfM-8WMjJt-tXGUub-bjgoWi-r9trfC-4cy4j5-wQn5GE-bhpn28-qPGVUV-dqmhkk-tXJdCV-5UA8h5-dqmhdJ-dqmm4A-dZyqk5-5KhViU-ngtrVa-e7X2bA-a9EMUN-6YdrzU-6avbYm-hoRQMX-8aW77E-7M3xej-n8oNAe-iZgjzy-pAnd2n-ne8nNK-vzHgg-49HpzE-7YHEZM-6jJko4-58d5Pk-uPuajk-uNLrMA-58hckC-4GaFZH-rjurPz-DaTCC-4bQZqE-6jNwgu-vzHiZ
[wllvm]: https://github.com/travitch/whole-program-llvm
