---
author: Marko Dimjašević
comments: false
date: 2015-11-17 01:42:07+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/11/16/jpf-doop-is-dead-long-live-jdoop/
slug: jpf-doop-is-dead-long-live-jdoop
title: "JPF-Doop is dead, long live JDoop!"
wordpress_id: 35050
categories:
- free software
- science
tags:
- CAVA
- concolic execution
- dynamic symbolic execution
- feedback-directed random testing
- Java PathFinder
- jDART
- JDoop
- jpf-doop
- Randoop
- software testing
---

[![JDoop Source Code Snippet](jdoop-code-screenshot.png "JDoop Source Code Snippet"){ width=100% }](jdoop-code-screenshot.png)


In my first year of a PhD program, I worked on a software testing
technique that combines concolic (or dynamic symbolic) execution and
feedback-directed random testing. An implementation followed and it
was dubbed JPF-Doop. The name was trying to honor two tools that
formed the foundation of JPF-Doop --- on one hand, it was Java
Pathfinder and to be more precise, it was JDart (back then JPF-JDart)
that performs concolic execution, and on the other hand there was
Randoop, which performs feedback-directed random testing. The name was
a bit clumsy.

<!-- more -->

Nevertheless, for JPF-Doop my advisor and I got a poster presentation
at a conference\cite{fmcad2013-d} and an extended abstract at a
workshop\cite{jpf2013-dr}, which was a good acknowledgment for the
technique. Until last week JPF-Doop was collecting dust.


Since JPF-Doop's incarnation several things changed. Most important,
all JPF-Doop dependencies are free software now! Most of the
dependencies used to be proprietary:
[JPF](http://babelfish.arc.nasa.gov/trac/jpf) used to be NOSA
1.3-licensed (now is Apache 2.0), I think
[JDart](https://github.com/psycopaths/jdart) had no license at all and
wasn't available to the general public (now is Apache 2.0), and
[Z3](https://github.com/Z3Prover/z3) used to be licensed under MSR-LA
(now is Expat). Given all of that, I thought it made sense to release
JPF-Doop and make it free software too.


We've been working on the [CAVA project](/tag/cava/), which includes
analyzing code written in Java. Given that JPF-Doop analyzes Java
code, we decided to revamp JPF-Doop. As of last week, JPF-Doop is no
more, and instead it is known as
[JDoop](https://github.com/psycopaths/jdoop). The new name is a bit
easier to remember as it follows a convention like many other tools
that are written and/or work on Java code, having a name J
something. JDoop is finally free software and its [1.1
release](https://github.com/psycopaths/jdoop/releases/tag/1.1) is
available.


If you have a Java project and you want to automatically generate unit
tests that cover a (hopefully) large portion of your code base, feel
free to give the tool a try. [Let me know](/contact/) [how it
goes](https://github.com/psycopaths/jdoop/issues).
