---
author: Marko Dimjašević
comments: false
date: 2015-03-05 03:26:00+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/03/05/vmware-sued-for-failure-to-comply-with-gpl/
slug: vmware-sued-for-failure-to-comply-with-gpl
title: "VMware Sued for Failure to Comply with GPL"
wordpress_id: 34755
categories:
- ephemera
- free software
post_format:
- Status
tags:
- GPL
- VMware
---

[VMware is sued][vm] in Germany for failure to comply with the GPL on
the Linux kernel.

[vm]: http://sfconservancy.org/news/2015/mar/05/vmware-lawsuit/
