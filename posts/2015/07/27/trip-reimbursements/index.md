---
author: Marko Dimjašević
comments: false
date: 2015-07-27 22:04:10+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/07/27/trip-reimbursements/
slug: trip-reimbursements
title: "Trip reimbursements"
wordpress_id: 34954
categories:
- ephemera
- University of Utah
post_format:
- Status
---

I wish it was a lot easier to have a conference trip covered through
the university. This way I'm dealing with 4 fund sources, with
expenditures breaking down in a number of crazy ways.
