---
author: Marko Dimjašević
comments: false
date: 2015-07-27 21:44:45+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/07/27/back-from-issta-and-cav-2015/
slug: back-from-issta-and-cav-2015
title: "Back from ISSTA and CAV 2015"
wordpress_id: 34952
categories:
- ephemera
- science
post_format:
- Status
---

Got back from this year's [ISSTA][issta] (Baltimore) and [CAV][cav]
(San Francisco). Learned a lot, met old and new people, discussed
research, presented a paper, and had a great time!  When is the next
research event? :-)

[issta]: https://web.archive.org/web/20150727214115/http://issta2015.cs.uoregon.edu/
[cav]: https://web.archive.org/web/20150727214310/http://i-cav.org/2015/
