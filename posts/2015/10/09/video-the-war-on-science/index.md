---
author: Marko Dimjašević
comments: false
date: 2015-10-09 16:35:25+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/10/09/video-the-war-on-science/
slug: video-the-war-on-science
title: "Video: The War on Science"
wordpress_id: 35011
categories:
- science
---

I stumbled upon this nice video that talks about the society being at
odds with science... it's nice until it starts preaching about voting
in inherently broken societal systems we have today.

Therefore, I recommend to watch the 5-minute video The War on Science
minus its last 30 seconds.

> [The War on Science](https://www.youtube.com/watch?v=8e1XX-ngJcc)
