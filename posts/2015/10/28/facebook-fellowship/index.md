---
author: Marko Dimjašević
comments: false
date: 2015-10-28 20:17:36+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/10/28/facebook-fellowship/
slug: facebook-fellowship
title: "Facebook Fellowship"
wordpress_id: 35027
categories:
- ephemera
post_format:
- Status
---

In order to apply for Facebook's fellowship, one has to create a
profile on Facebook first.
