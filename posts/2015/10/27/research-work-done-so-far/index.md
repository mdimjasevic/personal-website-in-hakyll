---
author: Marko Dimjašević
comments: false
date: 2015-10-27 03:57:03+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/10/27/research-work-done-so-far/
slug: research-work-done-so-far
title: "Research work done so far"
wordpress_id: 35022
categories:
- free software
- science
tags:
- html
- html2text
- LaTeX
- latex2html
- plain text
- research summary
---

As a preparation for a written qualifier I am soon to take as part of
my PhD program, I had to write a research summary outlining what I
have done so far. The point of the summary is to show your thesis
committee what you have done.


Given that I had to embed the summary as plain text in an email to my
thesis committee, I was thinking what to write it in. $latex \LaTeX$
seemed like an obvious choice because I write research works in it,
but the outcome had to be plain text instead of the usual PDF
format. Nevertheless, there is a way to get plain text out of a $latex
\LaTeX$ document: convert the $latex \LaTeX$ source to HTML and then
convert the HTML intermediate to plain text. There are readily
available free software tools for both conversions: _latex2html_ and
_html2text_. If you are curious how to do it, [check it
out](https://gitlab.com/mdimjasevic/written-qual-summary).


In case you are interested in the summary itself, go ahead and [read
it](./research-summary.html).
