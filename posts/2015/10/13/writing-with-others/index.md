---
author: Marko Dimjašević
comments: true
date: 2015-10-13 04:27:47+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/10/13/writing-with-others/
slug: writing-with-others
title: "Writing with Others"
wordpress_id: 35015
categories:
- ephemera
post_format:
- Status
tags:
- auctex
- Emacs
- ignorance
- labels
- LaTeX
- RefTeX
- writing
---

It's hard when you use RefTeX and others you write with don't, but
they still deliberately make changes with regard to labels in a
multifile document how pleases them, in spite of breaking context
boundaries.
