---
author: Marko Dimjašević
comments: false
date: 2015-05-13 06:44:06+00:00
excerpt: ""
layout: post
link: https://dimjasevic.net/marko/2015/05/13/going-bould/
slug: going-bould
title: "Going Bould"
wordpress_id: 34839
categories:
- University of Utah
tags:
- bouldering
- carpal tunnel syndrome
- rock climbing
---

[![Courtesy of Ram Sripracha](rock-climbing.jpg "Courtesy of Ram Sripracha"){ width=70% }](https://en.wikipedia.org/wiki/File:Bouldering.jpg)


About a year and a half ago I visited a doctor due to a wrist
pain. That day one of the greatest fears of every office worker came
true --- it was the [Carpal tunnel
syndrome](https://en.wikipedia.org/wiki/Carpal_tunnel_syndrome). The
syndrome involves the median nerve that delivers sensation to wrists
and forearms and in the syndrome's case, it's with tingling, pain, and
numbness included. Luckily, the doctor said it was a mild degree. It's
kind of ironic to have the syndrome when your job description revolves
around using a computer all day long. That got me quite worried and
since then I took some measures to fight it, such as getting an
ergonomic keyboard at home and at work and doing lightweight exercises
for the wrists the doctor had prescribed, as well as trying to work
out more in general. That helped me to some extent, though I'd
occasionally feel numbness in my wrists and forearms. I also informed
my research group about this and shared the exercises with
them. Ganesh Gopalakrishnan, the group's professor, replied by saying
all of us students should do rock climbing and then there would be no
such wrist pain.


<!-- more -->


Several months ago I got back to Utah from a few months spent in
California. I was without an apartment so a friend of mine offered me
to stay at his place until I find something. Soon I found an apartment
and decided to buy some beer to thank him and his roommates for
hosting me. Carrying two sizable beer packs on the way back from a
store, my wrists turned numb and my forearms sore. I had to stop and
take a break several times. This would never happen in the old days so
it was _the_ wake-up call --- what kind of man am I if I can't take
beer?


It is that evening, in spite of no host having a beer, that I decided
I am taking Ganesh's advice --- I want to do bouldering. Having the
syndrome is the very reason to do it. So I made up my mind --- I will
workout and then several months later, in May, I will test if I can
boulder.


[![Courtesy of Blockhelden](bouldering-1.jpg "Courtesy of Blockhelden"){ width=70% }](https://www.flickr.com/photos/blockhelden/8053680621/)


May is here and I decided today would be the test day. So today after
work I went to [a new fancy recreational facility we have at the
U](http://campusrec.utah.edu/facilities/thesummit/) and checked out
some of artificial climbing walls they have there. For the last few
months I would get to the facility for swimming and basketball and
casually observe the walls and folks climbing. Today I was there to
climb. A couple of guys working there gave me a brief intro as I never
climbed in this way. Basically I can climb however I want, but there
are marked paths of a varying difficulty and they are color-coded. I
looked at a color legend and then found an orange path, as orange ones
are the easiest. To my surprise, I completed the climb and I thought
to myself: "Wow, this is rather easy!" Hanging off the top of the wall
(about 5 meters in height) I jumped onto bouldering mats and was ready
for the next challenge.


Then I browsed around and found another orange path. This one wasn't
as smooth, but I finished it too. After that I took a break as I
realized that my forearms got pumped. Forearms are, along with
fingers, the most stressed parts of your body while climbing. My
forearms looked like that time when I was carrying the beer packs. It
is flexors in particular that got pumped, which are mainly supplied by
the median nerve, the same median nerve of the Carpal tunnel syndrome
fame. A few minutes later I decided to give a shot to a path on the
next difficulty level. That one was unsuccessful as it was harder and
the grip along the path was not so good. I called it a day there.


All in all, I would say the test was successful! My long-term goal is
to finish all the paths on the walls in the facility and then do rock
climbing with a harness at the same place. I already found advices
online on how to delay pumping. Another thing that doesn't hurt is to
have stronger forearms so I will pay more attention to working them
out. In other words, on a way to turn the
Carpal-tunnel-syndrome-diagnosed wrists and forearms into boulders
through bouldering...


[![Courtesy of Martin Morissette](bouldering-2.jpg "Courtesy of Martin Morissette"){ width=70% }](https://www.flickr.com/photos/9201430@N06/4326518791/)


Hopefully the next post on bouldering will have a crazy photo
featuring me hanging off a climbing wall!
