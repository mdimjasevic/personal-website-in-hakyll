---
title: Publications
---

## Peer-reviewed Publications

* M. Dimjašević, F. Howar, K. Luckow, and Z. Rakamarić, “Study of integrating random and symbolic testing for object-oriented software,” in Proceedings of the International Conference on integrated Formal Methods (iFM), 2018.
* K. Luckow, M. Dimjašević, D. Giannakopoulou, F. Howar, M. Isberner, T. Kahsai, Z. Rakamarić, and V. Raman, “JDart: a dynamic symbolic analysis framework,” in Proceedings of the International Conference on Tools and Algorithms for the Construction and Analysis of Systems (TACAS), 2016. doi:10.1007/978-3-662-49674-9_26
* M. Dimjašević, S. Atzeni, I. Ugrina, and Z. Rakamarić, “Evaluation of android malware detection based on system calls,” in Proceedings of the International Workshop on Security and Privacy Analytics (IWSPA), 2016. doi:10.1145/2875475.2875487
* M. Dimjašević and D. Giannakopoulou, “Test-case generation for runtime analysis and vice versa: verification of aircraft separation assurance,” in Proceedings of the International Symposium on Software Testing and Analysis (ISSTA), 2015. doi:10.1145/2771783.2771804
* M. Dimjašević, D. Giannakopoulou, F. Howar, M. Isberner, Z. Rakamarić, and V. Raman, “The Dart, the Psyco, and the Doop: concolic execution in Java Pathfinder and its applications,” in Proceedings of the java pathfinder workshop (jpf), 2014. doi:10.1145/2693208.2693248
* M. Dimjašević and Z. Rakamarić, “JPF-Doop: combining concolic and random testing for java,” in Java pathfinder workshop (jpf), 2013.
* M. Dimjašević, “Automatic testing of software libraries,” in Formal methods in computer-aided design (FMCAD), 2013.


## Technical Reports

* M. Dimjašević, S. Atzeni, I. Ugrina, and Z. Rakamarić, “Android malware detection based on system calls,” University of Utah, School of Computing, UUCS-15-003, 2015.


## Data

* Dimjašević, Marko, Atzeni, Simone, Ugrina, Ivo, & Rakamarić, Zvonimir. (2016). Evaluation of Android Malware Detection Based on System Calls --- Dataset [Data set]. Zenodo. [http://doi.org/10.5281/zenodo.154737](http://doi.org/10.5281/zenodo.154737)
