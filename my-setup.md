---
title: My setup
---

## Laptop
* Lenovo ThinkPad X1 Carbon Gen 6
* Operating system: <a href="https://www.debian.org/">Debian</a> GNU/Linux (Buster)
* <a href="https://www.gnu.org/software/emacs/">GNU Emacs</a>
* <a href="http://orgmode.org/">Org-mode</a>

## Server

* VPS
* Debian GNU/Linux (Jessie)
* Web server: nginx
* This website: <a href="http://jaspervdj.be/hakyll">Hakyll</a>
* Mail server: Postfix + Dovecot + SpamAssassin
<!-- <li>Media gallery: <a href="http://mediagoblin.org">GNU MediaGoblin</a> -->
<!-- <li>Cloud (files, calendar, address book): <a href="http://owncloud.org/">ownCloud</a> -->
<!-- <li>URL shortener: <a title="YOURLS" href="http://yourls.org/">Your Own URL Shortener</a> -->

## Mobile phone
* Sony Experia Z1 Compact
* CyanogenMod 12.1
* <a href="https://f-droid.org/">F-Droid</a>, an Android app repository with free software only
* K-9 Mail
