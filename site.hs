--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}

import           Control.Monad (mplus)
import           Data.Char (toLower)
import           Data.List (intersperse, sortBy)
import           Data.Maybe (fromMaybe)
import           Data.List.Split (splitOn)
import           Data.Monoid (mappend)
import           Hakyll
import           Hakyll.Web.Tags (Tags, caseInsensitiveTags, sortTagsBy)
import           Text.Blaze.Html             (toHtml, toValue, (!))
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A

--------------------------------------------------------------------------------
main :: IO ()
main = hakyllWith config $ do
    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "media/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "resume.pdf" $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match (fromList ["about.md", "contact.md", "my-setup.md", "publications.md"]) $ do
        route   $ customRoute (mainPageRoute . toFilePath) `composeRoutes` setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls

    match "publications/bib/publications.bib" $ compile biblioCompiler

    match "publications/csl/association-for-computational-linguistics.csl" $ compile cslCompiler

    -- Collect tags and categories. This unifies tags and categories
    -- from WordPress-age posts where I used both tags and categories.
    tags <- fmap (sortTagsBy caseInsensitiveTags) $
              buildTagsWith getTagsCategories postsMdPattern (fromCapture tagPattern)

    -- Generate tag pages
    tagsRules tags $ \tag pattern -> do
      let title = "Posts tagged with \"" ++ tag ++ "\""
      route   urlFriendlyRoute
      compile $ do
        posts <- recentFirst =<< loadAll pattern
        let ctx = constField "title" title
                    `mappend` listField "posts" postCtx (return posts)
                    `mappend` defaultContext

        makeItem ""
          >>= loadAndApplyTemplate "templates/tag.html" ctx
          >>= loadAndApplyTemplate "templates/default.html" ctx
          >>= relativizeUrls

    -- Render blog posts
    match "posts/*/*/*/*/*.md" $ do
        route   $ postsRoute `composeRoutes` setExtension "html"
        compile $ pandocBiblioCompiler cslFileName bibFileName
            >>= loadAndApplyTemplate "templates/post.html"    (postCtxWithTags tags)
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate "templates/default.html" (postCtxWithTags tags)
            >>= relativizeUrls

    -- Copy resource files for blog posts.
    match "posts/*/*/*/*/*" $ do
      route   postsRoute
      compile copyFileCompiler

    create ["archive.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll postsMdPattern
            let archiveCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    constField "title" "Archives"            `mappend`
                    defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls


    match "index.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll postsMdPattern
            let indexCtx =
                  listField
                    "posts"
                    postCtx
                    (return . take 5 $ posts) `mappend`
                  constField "title" "Home"   `mappend`
                  defaultContext

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler

    create [atomName, rssName] $ do
      route   idRoute
      compile $ do
        xmlName <- getUnderlying
        let renderer =
              if xmlName == atomName
                then renderAtom
                else renderRss
        feedCompiler renderer postsMdPattern

  where
    atomName :: Identifier
    atomName = "atom.xml"

    rssName :: Identifier
    rssName = "feed/"

    postsRoute :: Routes
    postsRoute = gsubRoute "posts/" (const "")

    postsMdPattern = "posts/*/*/*/*/*.md"

    bibFileName = "publications/bib/publications.bib"
    cslFileName = "publications/csl/association-for-computational-linguistics.csl"

    mainPageRoute :: String -> String
    mainPageRoute s =
      (head . splitOn "." $ s) ++ "/index"

    tagPattern :: Pattern
    tagPattern = "tag/*/index.html"

    -- | Replaces spaces with a dash "-" and turns upper case letters
    -- into lower case letters
    urlFriendlyRoute :: Routes
    urlFriendlyRoute = customRoute $ fmap toLowerRemSpace . toFilePath where
      toLowerRemSpace :: Char -> Char
      toLowerRemSpace ' ' = '-'
      toLowerRemSpace c   = toLower c

--------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext

postCtxWithTags :: Tags -> Context String
postCtxWithTags tags =
  tagsField' "tags" tags `mappend` postCtx

-- | Render tags with links
tagsField'
  :: String
  -> Tags
  -> Context a
tagsField' =
  tagsFieldWith getTagsCategories simpleRenderLink (mconcat . intersperse ", ")

-- | Obtain tags and categories from a page: parse them from the
-- @tags@ and @categories@ metadata field. This can either be a list
-- or a comma-separated string.
getTagsCategories :: MonadMetadata m => Identifier -> m [String]
getTagsCategories identifier = do
  metadata <- getMetadata identifier
  tags <- getTags identifier
  return $ tags ++ (fromMaybe [] $
    (lookupStringList "categories" metadata) `mplus`
    (map trim . splitAll "," <$> lookupString "categories" metadata))


-- TODO: Ask upstream to export function simpleRenderLink from Hakyll.Web.Tags

-- | Render one tag link
simpleRenderLink :: String -> (Maybe FilePath) -> Maybe H.Html
simpleRenderLink _   Nothing         = Nothing
simpleRenderLink tag (Just filePath) =
  Just $ H.a ! A.href (toValue $ toUrl filePath) $ toHtml tag

config :: Configuration
config = defaultConfiguration { previewPort = 8080 }

myFeedConfiguration :: FeedConfiguration
myFeedConfiguration = FeedConfiguration
  { feedTitle       = "Marko Dimjašević"
  , feedDescription = "Computer scientist / Fellow engaged in social production of culture"
  , feedAuthorName  = "Marko Dimjašević"
  , feedAuthorEmail = "marko@dimjasevic.net"
  , feedRoot        = "https://dimjasevic.net/marko"
  }

feedCompiler
  :: (  FeedConfiguration
     -> Context String
     -> [Item String]
     -> Compiler (Item String)
     )
  -> Pattern
  -> Compiler (Item String)
feedCompiler f postsMdPattern = do
  let feedCtx = postCtx `mappend` bodyField "description"
  posts <- fmap (take 10) . recentFirst =<<
           loadAllSnapshots postsMdPattern "content"
  f myFeedConfiguration feedCtx posts
